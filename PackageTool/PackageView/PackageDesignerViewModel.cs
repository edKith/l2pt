﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using PackageTool.PackageView.ViewModels;
using Unreal.Core.Objects;
using Unreal.Core.Package.Parts;

namespace PackageTool.PackageView
{
    public class PackageDesignerViewModel
    {
        public string PackageName => "Core.u";
        public string PackagePath => "d:\\Games\\Lineage2\\CT25\\System\\Core.u";
        public int SummaryVersion => 123;
        public int SummaryLicenseeVersion => 37;
        public int FileInfoSize => 72950;

        private NameEntry[] stubNameTable => new List<NameEntry>()
        {
            new NameEntry(){ Flags = EObjectFlags.RF_TagExp | EObjectFlags.RF_HighlightedName | EObjectFlags.RF_LoadContextFlags | EObjectFlags.RF_Native, Name = "None"},
            new NameEntry(){ Flags = EObjectFlags.RF_TagExp | EObjectFlags.RF_LoadContextFlags | EObjectFlags.RF_Native, Name = "Core"}
        }.ToArray();

        public ObservableCollection<NameEntryViewModel> NameTable => new ObservableCollection<NameEntryViewModel>()
        {
            new NameEntryViewModel(stubNameTable[0]),
            new NameEntryViewModel(stubNameTable[1]),
        };
        public ObservableCollection<ImportEntryViewModel> ImportTable => new ObservableCollection<ImportEntryViewModel>()
        {
            //new ImportEntryViewModel(0, new ImportEntry(){NameIndex = 0, ObjectClass = 0, ObjectSubPackage = 0, Package = 0}, stubNameTable),
        };
        public ObservableCollection<ExportEntryViewModel> ExportTable => new ObservableCollection<ExportEntryViewModel>()
        {
            //new ImportEntryViewModel(0, new ImportEntry(){NameIndex = 0, ObjectClass = 0, ObjectSubPackage = 0, Package = 0}, stubNameTable),
        };
    }
}