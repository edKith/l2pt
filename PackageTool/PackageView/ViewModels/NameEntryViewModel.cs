﻿using UI.Core.Mvvm;
using Unreal.Core.Objects;
using Unreal.Core.Package.Parts;

namespace PackageTool.PackageView.ViewModels
{
    public class NameEntryViewModel : BindableBase
    {
        private NameEntry model;

        public string Name
        {
            get => this.model.Name;
            set => SetProperty(ref this.model.Name, value);
        }
        public EObjectFlags Flags
        {
            get => this.model.Flags;
            set => SetProperty(ref this.model.Flags, value);
        }
       
        public NameEntryViewModel(NameEntry model)
        {
            this.model = model;
        }
    }
}