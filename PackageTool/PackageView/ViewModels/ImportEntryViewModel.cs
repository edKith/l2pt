﻿using UI.Core.Mvvm;
using Unreal.Core.Objects;
using Unreal.Core.Package;
using Unreal.Core.Package.Parts;

namespace PackageTool.PackageView.ViewModels
{
    public class ImportEntryViewModel : BindableBase
    {
        private readonly PackageBase package;
        public readonly ImportEntry Model;
        
        public string Name
        {
            get => this.package.GetName(this.Model.NameIndex);
            //set => SetProperty(ref this.model.Name, value);
        }
        public string Package
        {
            get => this.package.GetName(this.Model.Package);
            //set => SetProperty(ref this.model.Name, value);
        }
        public string SubPackage
        {
            get => this.package.GetObjectName(this.Model.ObjectSubPackage);
            //set => SetProperty(ref this.model.Name, value);
        }
        public string Class
        {
            get => this.package.GetName(this.Model.ObjectClass);
            //set => SetProperty(ref this.model.Name, value);
        }
       

        public ImportEntryViewModel(ImportEntry model, PackageBase package)
        {
            this.Model = model;
            this.package = package;
        }

    }
}