﻿using UI.Core.Mvvm;
using Unreal.Core.Objects;
using Unreal.Core.Package;
using Unreal.Core.Package.Parts;

namespace PackageTool.PackageView.ViewModels
{
    public class ExportEntryViewModel : BindableBase
    {
        private PackageBase package;
        private ExportEntry model;

        public string Name
        {
            get => this.package.GetName(this.model.NameIndex);
            //set => SetProperty(ref this.model.Name, value);
        }
        public string SuperClass
        {
            get => this.package.GetObjectName(this.model.SuperClass);
            //set => SetProperty(ref this.model.Name, value);
        }
        public string SubPackage
        {
            get => this.package.GetObjectName(this.model.ObjectSubPackage);
            //set => SetProperty(ref this.model.Name, value);
        }
        public string Class
        {
            get => this.package.GetObjectName(this.model.ObjectClass);
            //set => SetProperty(ref this.model.Name, value);
        }

        public int Size
        {
            get => this.model.DataSize;
            //set => SetProperty(ref this.model.Name, value);
        }
        public int Offset
        {
            get => this.model.DataOffset;
            //set => SetProperty(ref this.model.Name, value);
        }

        public EObjectFlags Flags
        {
            get => this.model.Flags;
            set => SetProperty(ref this.model.Flags, value);
        }


        public ExportEntryViewModel(ExportEntry model, PackageBase package)
        {
            this.model = model;
            this.package = package;
        }

    }
}