﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using UI.Core.Mvvm;
using Unreal.Core.Package;
using Unreal.Core.Package.Parts;

namespace PackageTool.PackageView.ViewModels
{
    public class ImportTreeViewModel : BindableBase
    {
        private readonly PackageBase owner;
        public readonly int Number;
        public readonly ImportEntry Entry;
        private ObservableCollection<ImportTreeViewModel> children;

        public string Name
        {
            get => this.owner == null ? "???" : this.owner.GetName(this.Entry.NameIndex);
            //set => SetProperty(ref this.model.Name, value);
        }
        public string Package
        {
            get => this.owner == null ? "???" : this.owner.GetName(this.Entry.Package);
            //set => SetProperty(ref this.model.Name, value);
        }
        public string SubPackage
        {
            get => this.owner == null ? "???" : this.owner.GetObjectName(this.Entry.ObjectSubPackage);
            //set => SetProperty(ref this.model.Name, value);
        }
        public string Class
        {
            get => this.owner == null ? "???" : this.owner.GetName(this.Entry.ObjectClass);
            //set => SetProperty(ref this.model.Name, value);
        }
        public ObservableCollection<ImportTreeViewModel> Children
        {
            get => this.children;
            private set => this.SetProperty(ref this.children, value);
        }


        public ImportTreeViewModel(PackageBase owner)
        {
            this.owner = owner;
            this.Number = -1;

            var nonRootElements = new List<ImportTreeViewModel>();
            this.Children = new ObservableCollection<ImportTreeViewModel>();

            for (var index = 0; index < owner.ImportTable.Length; index++)
            {
                ImportEntry importEntry = owner.ImportTable[index];
                if (importEntry.ObjectSubPackage == 0)
                {
                    this.Children.Add(new ImportTreeViewModel(owner, importEntry, index));
                }
                else
                {
                    nonRootElements.Add(new ImportTreeViewModel(owner, importEntry, index));
                }
            }

            foreach (ImportTreeViewModel importTree in this.Children)
            {
                importTree.FillTree(nonRootElements);
            }
            this.RaisePropertyChanged(nameof(Children));
        }

        public ImportTreeViewModel(PackageBase owner, ImportEntry entry, int number)
        {
            this.owner = owner;
            this.Entry = entry;
            this.Number = number;
            this.RaisePropertyChanged(nameof(Name));
        }

        private void FillTree(List<ImportTreeViewModel> importTrees)
        {
            var count = importTrees.Count;
            if (count < 1) return;
            var tmpClassList = new ObservableCollection<ImportTreeViewModel>();
            var classList = new List<ImportTreeViewModel>();
            foreach (var ucInfo in importTrees)
            {
                if (this.owner[ucInfo.Entry.ObjectSubPackage] == this.Entry)
                {
                    tmpClassList.Add(ucInfo);
                    //Progress.Message = $"Add {ucInfo.FileSystemEntry.Name} to {FileSystemEntry.Name}";
                    classList.Add(ucInfo);
                }
                else
                {
                    //Progress.Message = $"Filling children of {Name}...";
                }
            }

            foreach (var removeInfo in tmpClassList)
            {
                importTrees.Remove(removeInfo);
            }
            tmpClassList = new ObservableCollection<ImportTreeViewModel>();
            foreach (var _class in classList)
            {
                _class.FillTree(importTrees);
                tmpClassList.Add(_class);
            }
            this.Children = tmpClassList;
        }

        public string PrintOut(string appender)
        {
            var stringBuilder = new StringBuilder();
            if (this.Entry != null)
                stringBuilder.AppendLine($"({this.Number}) {this.owner.NameTable[this.Entry.NameIndex].Name}");
            if(this.Children != null)
            {
                foreach (ImportTreeViewModel importTree in this.Children)
                {
                    stringBuilder.Append(appender);
                    stringBuilder.AppendLine(importTree.PrintOut(appender+' '));
                }
            }
            return stringBuilder.ToString();
        }
    }
}