﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using UI.Core.Mvvm;
using Unreal.Core.Package;
using Unreal.Core.Package.Parts;

namespace PackageTool.PackageView.ViewModels
{
    public class ExportTreeViewModel : BindableBase
    {
        private readonly PackageBase owner;
        public readonly int Number;
        public readonly ExportEntry Entry;
        private ObservableCollection<ExportTreeViewModel> children;

        public string Name
        {
            get => this.owner == null ? "???" : this.owner.GetName(this.Entry.NameIndex);
            //set => SetProperty(ref this.model.Name, value);
        }
        public string SuperClass
        {
            get => this.owner == null ? "???" : this.owner.GetObjectName(this.Entry.SuperClass);
            //set => SetProperty(ref this.model.Name, value);
        }
        public string SubPackage
        {
            get => this.owner == null ? "???" : this.owner.GetObjectName(this.Entry.ObjectSubPackage);
            //set => SetProperty(ref this.model.Name, value);
        }
        public string Class
        {
            get => this.owner == null ? "???" : this.owner.GetObjectName(this.Entry.ObjectClass);
            //set => SetProperty(ref this.model.Name, value);
        }
        public ObservableCollection<ExportTreeViewModel> Children
        {
            get => this.children;
            private set => this.SetProperty(ref this.children, value);
        }


        public ExportTreeViewModel(PackageBase owner)
        {
            this.owner = owner;
            this.Number = -1;

            var nonRootElements = new List<ExportTreeViewModel>();
            this.Children = new ObservableCollection<ExportTreeViewModel>();

            for (var index = 0; index < owner.ExportTable.Length; index++)
            {
                ExportEntry exportEntry = owner.ExportTable[index];
                if (exportEntry.ObjectSubPackage == 0)
                {
                    this.Children.Add(new ExportTreeViewModel(owner, exportEntry, index));
                }
                else
                {
                    nonRootElements.Add(new ExportTreeViewModel(owner, exportEntry, index));
                }
            }

            foreach (ExportTreeViewModel exportTree in this.Children)
            {
                exportTree.FillTree(nonRootElements);
            }
            this.RaisePropertyChanged(nameof(Children));
        }

        private ExportTreeViewModel(PackageBase owner, ExportEntry entry, int number)
        {
            this.owner = owner;
            this.Entry = entry;
            this.Number = number;
            this.RaisePropertyChanged(nameof(Name));
        }

        private void FillTree(List<ExportTreeViewModel> exportTrees)
        {
            var count = exportTrees.Count;
            if (count < 1) return;
            var tmpClassList = new ObservableCollection<ExportTreeViewModel>();
            var classList = new List<ExportTreeViewModel>();
            foreach (var ucInfo in exportTrees)
            {
                if (this.owner[ucInfo.Entry.ObjectSubPackage] == this.Entry)
                {
                    tmpClassList.Add(ucInfo);
                    //Progress.Message = $"Add {ucInfo.FileSystemEntry.Name} to {FileSystemEntry.Name}";
                    classList.Add(ucInfo);
                }
                else
                {
                    //Progress.Message = $"Filling children of {Name}...";
                }
            }

            foreach (var removeInfo in tmpClassList)
            {
                exportTrees.Remove(removeInfo);
            }
            tmpClassList = new ObservableCollection<ExportTreeViewModel>();
            foreach (var _class in classList)
            {
                _class.FillTree(exportTrees);
                tmpClassList.Add(_class);
            }
            this.Children = tmpClassList;
        }

        public string PrintOut(string appender)
        {
            var stringBuilder = new StringBuilder();
            if (this.Entry != null)
                stringBuilder.AppendLine($"({this.Number}) {this.owner.NameTable[this.Entry.NameIndex].Name}");
            if(this.Children != null)
            {
                foreach (ExportTreeViewModel exportTree in this.Children)
                {
                    stringBuilder.Append(appender);
                    stringBuilder.AppendLine(exportTree.PrintOut(appender+' '));
                }
            }
            return stringBuilder.ToString();
        }
    }
}