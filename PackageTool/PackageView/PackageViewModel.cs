﻿using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Microsoft.Win32;
using PackageTool.PackageView.ViewModels;
using UI.Core.Mvvm;
using UI.Core.Wpf.Mvvm;
using Unreal.Core.IO;
using Unreal.Core.Objects;
using Unreal.Core.Package;
using Unreal.Core.Package.Parts;

namespace PackageTool.PackageView
{
    public class PackageViewModel : BindableBase
    {
        private PackageBase model;
        private ImportTreeViewModel importTree;
        private ExportTreeViewModel exportTree;
        public string PackageName => this.model == null ? "???" : this.model.PackageFileEntry.Name;
        public string PackagePath => this.model == null ? "???" : this.model.PackageFileEntry.FullName;
        public int SummaryVersion => this.model == null ? -1 : this.model.Summary.Version;
        public int SummaryLicenseeVersion => this.model == null ? -1 : this.model.Summary.LicenseeVersion;
        public long FileInfoSize => this.model == null ? 0 : this.model.PackageFileEntry.Length;
        public EPackageFlags Flags => this.model == null ? (EPackageFlags)0 : this.model.Summary.PackageFlags;

        public ObservableCollection<NameEntryViewModel> NameTable => this.model == null
            ? new ObservableCollection<NameEntryViewModel>()
            : new ObservableCollection<NameEntryViewModel>(this.model.NameTable.ToList()
                .ConvertAll((entry) => new NameEntryViewModel(entry)));
        public ObservableCollection<ImportEntryViewModel> ImportTable => this.model == null
            ? new ObservableCollection<ImportEntryViewModel>()
            : new ObservableCollection<ImportEntryViewModel>(this.model.ImportTable.ToList()
                .ConvertAll((entry) => new ImportEntryViewModel( entry, this.model)));
        public ObservableCollection<ExportEntryViewModel> ExportTable => this.model == null
            ? new ObservableCollection<ExportEntryViewModel>()
            : new ObservableCollection<ExportEntryViewModel>(this.model.ExportTable.ToList()
                .ConvertAll((entry) => new ExportEntryViewModel( entry, this.model)));

        public ImportTreeViewModel ImportTree
        {
            get => this.importTree;
            set => SetProperty(ref this.importTree, value);
        }
        public ExportTreeViewModel ExportTree
        {
            get => this.exportTree;
            set => SetProperty(ref this.exportTree, value);
        }

        public ICommand LoadModelCommand { get; }
        //public ICommand PrintImportTreeCommand { get; }

        public PackageViewModel()
        {
            LoadModelCommand = new DelegateCommand(LoadModel);
            //PrintImportTreeCommand = new DelegateCommand(PrintImportTree);
        }

        private void LoadModel()
        {
            var openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                var packageFileInfo = new FileInfo(openFileDialog.FileName);
                if (packageFileInfo.Exists)
                {
                    this.model = new PackageBase(packageFileInfo);
                    using (var packageReader = new FileReader(this.model.PackageFileEntry))
                    {
                        this.model.Read(packageReader);
                    }
                    this.RaiseModelChanged();
                }
                ImportTree = new ImportTreeViewModel(this.model);
                ExportTree = new ExportTreeViewModel(this.model);
            }
        }
        //private void PrintImportTree()
        //{
        //    if (this.model != null)
        //    {
        //        var tree = new ImportTreeViewModel(this.model);
        //        File.WriteAllText("ImportTree.txt", tree.PrintOut(string.Empty));
        //    }
        //}

        private void RaiseModelChanged()
        {
            this.RaisePropertyChanged(nameof(PackageName));
            this.RaisePropertyChanged(nameof(PackagePath));
            this.RaisePropertyChanged(nameof(SummaryVersion));
            this.RaisePropertyChanged(nameof(SummaryLicenseeVersion));
            this.RaisePropertyChanged(nameof(FileInfoSize));
            this.RaisePropertyChanged(nameof(NameTable));
            this.RaisePropertyChanged(nameof(ImportTable));
            this.RaisePropertyChanged(nameof(Flags));
            this.RaisePropertyChanged(nameof(ExportTable));
        }
    }
}