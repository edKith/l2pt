﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using Unreal.Core.Codecs;
using Unreal.Core.Extensions;
using Unreal.Core.IO;
using Unreal.Core.Package;

namespace PackageTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MiscWindow : Window
    {
        public MiscWindow()
        {
            InitializeComponent();
        }

        private const string TestFileName = "SkillName-e.dat";
        private void TestEncodeButtonClick(object sender, RoutedEventArgs e)
        {
            byte[] bytes;
            using (BufferedReader reader = new BufferedReader(new FileInfo($"dec-{TestFileName}")))
            {
                bytes = reader.Read();
            }

            using (BufferedWriter writer = new BufferedWriter(new FileInfo($"enc-{TestFileName}"), CodecFactory.PeekFromNumber(413)))
            {
                writer.Write(bytes);
            }
        }

        private void TestDecodeButtonClick(object sender, RoutedEventArgs e)
        {
            byte[] bytes;
            using (BufferedReader reader = new BufferedReader(new FileInfo(TestFileName)))
            {
                bytes = reader.Read();
            }

            using (BufferedWriter writer = new BufferedWriter(new FileInfo($"dec-{TestFileName}"), CodecFactory.PeekFromNumber(-1)))
            {
                writer.Write(bytes);
            }
        }

        private void Deflate(object sender, RoutedEventArgs e)
        {
            var bytes = File.ReadAllBytes("dec-l2.ini");
            var compressedBytes = new MemoryStream(bytes).CompressStream();

                using (var compressedFileStream = File.Create("dec-l2.compressed.ini"))
                {
                    compressedFileStream.Write(compressedBytes, 0, compressedBytes.Length);
                }

        }

        private void GetButtonName(object sender, RoutedEventArgs e)
        {
            if (sender is Control ui)
            {
                MessageBox.Show(ui.Name);
            }
        }

        private void testDump(object sender, RoutedEventArgs e)
        {
            var bytes = File.ReadAllBytes("dump.bin");
            try
            {
                File.WriteAllBytes("dump.unp.bin", new MemoryStream(bytes).DecompressStream()); 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message +"\n" +ex.StackTrace);
            }
        }

        private void makeDump(object sender, RoutedEventArgs e)
        {
            var testBytes = File.ReadAllBytes($"dec-{TestFileName}");

            var compressedBytes = new MemoryStream(testBytes).CompressStream();
            File.WriteAllBytes("dump.bin", compressedBytes);
        }

        private void TestPackage(object sender, RoutedEventArgs e)
        {
            var coreFileInfo = new FileInfo("core.u");
            if(coreFileInfo.Exists)
            {
                var corePackage = new PackageBase(coreFileInfo);
                using (var coreReader = new FileReader(coreFileInfo))
                {
                    corePackage.Read(coreReader);
                }

                using (var coreWriter = new FileWriter(CodecFactory.PeekFromNumber(111), new FileInfo("enc-core.u")))
                {
                    coreWriter.Write(corePackage);
                }
            }
        }
    }
}
