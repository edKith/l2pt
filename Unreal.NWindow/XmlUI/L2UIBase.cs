﻿using Unreal.Core.IO;
using Unreal.Core.Objects;
using Unreal.Core.Version;
using Unreal.Engine.LineageData;

namespace Unreal.NWindow.XmlUI
{
    public abstract class L2UIBase : IUnrealSerializable, IVersionControl
    {
        public L2Version Version { get; set; }
        public void Read(FileReader reader)
        {
            throw new System.NotImplementedException();
        }

        public void Write(FileWriter writer)
        {
            throw new System.NotImplementedException();
        }
    }
}