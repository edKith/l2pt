﻿namespace Unreal.NWindow.XmlUI.Data
{
    public enum ChatType
    {
        Normal,
        Shout,      // '!'
        Tell,       // '\'
        Party,      // '#'
        Clan,       // '@'
        System,     // ''
        UserPetition,   // '&'
        GmPetition,     // '*'
        Market,     // '+'
        Alliance,   // '%'	
        Announce,   // ''
        Custom,     // ''
        L2Friend,   // ''
        MsnChat,    // ''
        PartyRoomChat,  // ''		14
        CommanderChat,              // 15
        InterPartymasterChat,
        Hero,
        CriticalAnnounce,
        ScreenAnnounce,
        Dominionwar,                    // 20
        MpccRoom,
        NpcNormal,      // NPC 대사 필터링 - 2010.9.8 winkey
        NpcShout,
        FriendAnnounce,     // NPC 대사 필터링 - 2010.9.8 winkey
        World
    };
}