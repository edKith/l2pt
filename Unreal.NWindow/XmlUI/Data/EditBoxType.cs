﻿namespace Unreal.NWindow.XmlUI.Data
{
    public enum EditBoxType
    {
        Default,
        Chat,
        Password,
        Number,
        Unk4,
        Date,
        Time,
        ID
    }
}