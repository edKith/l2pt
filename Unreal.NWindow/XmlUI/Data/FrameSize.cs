﻿namespace Unreal.NWindow.XmlUI.Data
{
    public enum FrameSize
    {
        Undefined = -1,
        Big = 0,
        Small = 1,
        Unk2 = 2
    }
}