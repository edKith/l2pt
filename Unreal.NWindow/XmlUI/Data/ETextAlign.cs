﻿namespace Unreal.NWindow.XmlUI.Data
{
    public enum ETextAlign
    {
        Undefined,
        Left,
        Center,
        Right,
        MacroIcon,
    }
}