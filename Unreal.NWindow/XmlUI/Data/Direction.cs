﻿namespace Unreal.NWindow.XmlUI.Data
{
    public enum Direction
    {
        undefined2 = -9999,
        undefined = -1,
        None = 0,
        Left = 1,
        Right = 2,
        Top = 3,
        Bottom = 4,
        Free = 5
    }
}