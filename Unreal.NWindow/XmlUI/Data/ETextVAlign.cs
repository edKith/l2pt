﻿namespace Unreal.NWindow.XmlUI.Data
{
    public enum ETextVAlign
    {
        Undefined,
        Top,
        Middle,
        Bottom,
    }
}