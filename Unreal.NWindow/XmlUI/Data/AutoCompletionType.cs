﻿namespace Unreal.NWindow.XmlUI.Data
{
    public enum AutoCompletionType
    {
        Disabled = -1,
        NotUsed = 0,
        Normal = 1,
        PostBox = 2
    }
}