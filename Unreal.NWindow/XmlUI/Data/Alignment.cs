﻿namespace Unreal.NWindow.XmlUI.Data
{
    public enum Alignment
    {
        None,
        TopLeft,
        TopCenter,
        TopRight,
        CenterLeft,
        CenterCenter,
        CenterRight,
        BottomLeft,
        BottomCenter,
        BottomRight
    };
}