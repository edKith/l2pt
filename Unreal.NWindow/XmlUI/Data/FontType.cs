﻿namespace Unreal.NWindow.XmlUI.Data
{
    public enum FontType
    {
        NotUsed = -9999,
        Normal = -1,
        SpecialDigitSmall = 0,
        SpecialDigitLarge = 1,
        SpecialDigitXLarge = 2
    }
}