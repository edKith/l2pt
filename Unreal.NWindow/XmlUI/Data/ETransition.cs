﻿namespace Unreal.NWindow.XmlUI.Data
{
    public enum ETransition
    {
        None = 0,
        AlphaTransition = 3,
        PositionTransition = 4
    }
}