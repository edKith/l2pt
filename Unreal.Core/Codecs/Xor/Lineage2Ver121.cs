﻿namespace Unreal.Core.Codecs.Xor
{
    public class Lineage2Ver121 : Lineage2Ver1x1
    {
        private static ICodec _instance;
        public static ICodec Instance => _instance ?? (_instance = new Lineage2Ver121());

        public override string Header => nameof(Lineage2Ver121);

        public override void GenerateKey(string keyBase)
        {
            keyBase = keyBase.ToLower();
            var ind = 0;
            for (var i = 0; i < keyBase.Length; i++)
            {
                ind += keyBase[i];
            }

            this.XorKey = ind & 0xff;
        }
    }
}