﻿namespace Unreal.Core.Codecs.Xor
{
    public class Lineage2Ver111 : Lineage2Ver1x1
    {
        private static ICodec _instance;
        public static  ICodec Instance => _instance ?? (_instance = new Lineage2Ver111());
        public override string Header => nameof(Lineage2Ver111);
        public override void GenerateKey(string keyBase)
        {
            this.XorKey = 0xAC;
        }
    }
}