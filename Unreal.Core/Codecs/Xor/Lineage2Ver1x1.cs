﻿using System;
using Unreal.Core.Output;

namespace Unreal.Core.Codecs.Xor
{
    // ReSharper disable once InconsistentNaming
    public abstract class Lineage2Ver1x1 : ICodec
    {
        public string Title { get; set; }
        public Progresser Progress { get; set; }
        protected int XorKey;
        public long DataOffset => 28;
        public abstract void GenerateKey(string keyBase);

        protected Lineage2Ver1x1()
        {
            Progress = new Progresser
            {
                Message = "XOR codec in progress...",
                State = ProgressState.Indeterminate
            };
        }
        public byte Decode(byte data)
        {
            return this.ApplyKey(data);
        }

        public byte Encode(byte data)
        {
            return this.ApplyKey(data);
        }

        public virtual byte[] Decode(byte[] data)
        {
            Console.WriteLine("XOR decrypt...");
            Progress.Message = "XOR decrypt...";
            Progress.Begin(ProgressState.Forward, 0, data.Length);
            return this.ApplyKey(data);
        }

        public virtual byte[] Encode(byte[] data)
        {
            Console.WriteLine("XOR encrypt...");
            Progress.Message = "XOR encrypt...";
            Progress.Begin(ProgressState.Forward, 0, data.Length);
            return this.ApplyKey(data);
        }

        public abstract string Header { get; }

        private byte[] ApplyKey(byte[] data)
        {

            for (var i = 0; i < data.Length; i++)
            {
                data[i] = (byte) (data[i] ^ this.XorKey);
                if (i % 1024 == 0 || i == data.Length - 1)
                {
                    Progress.Current = i;
                }
            }
            Console.WriteLine("XOR done");
            Progress.Message = "XOR done";
            return data;
        }

        private byte ApplyKey(byte data)
        {
            return (byte) (data ^ this.XorKey);
        }
    }
}