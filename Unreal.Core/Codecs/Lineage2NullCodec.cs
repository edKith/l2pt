﻿using Unreal.Core.Output;

namespace Unreal.Core.Codecs
{
    public class Lineage2NullCodec : ICodec
    {
        private static ICodec _instance;
        public static ICodec Instance => _instance ?? (_instance = new Lineage2NullCodec());
        public string Title { get; set; }
        public Progresser Progress { get; set; } = new Progresser();
        public long DataOffset => 0;
        public void GenerateKey(string keyBase) { }
        public byte Decode(byte data) => data;
        public byte Encode(byte data) => data;

        public byte[] Decode(byte[] data) => data;
        public byte[] Encode(byte[] data) => data;
        public string Header => string.Empty;
    }
}