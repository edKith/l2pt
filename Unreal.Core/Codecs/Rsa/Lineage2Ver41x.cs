﻿using System;
using System.Collections.Generic;
using System.IO;
using Unreal.Core.Extensions;
using Unreal.Core.Output;
using Unreal.Core.RSA;
using Unreal.Core.RSA.Exceptions;
using Unreal.Core.RSA.Math;
using Unreal.Core.RSA.Parameters;

namespace Unreal.Core.Codecs.Rsa
{
    public abstract class Lineage2Ver41x : ICodec
    {
        #region RSA_constants
        public static BigInteger MODULUS_411 = new BigInteger(
            "8c9d5da87b30f5d7cd9dc88c746eaac5" +
            "bb180267fa11737358c4c95d9adf59dd" +
            "37689f9befb251508759555d6fe0eca8" +
            "7bebe0a10712cf0ec245af84cd22eb4c" +
            "b675e98eaf5799fca62a20a2baa4801d" +
            "5d70718dcd43283b8428f1387aec6600" +
            "f937bfc7bb72404d187d3a9c438f1ffc" +
            "e9ce365dccf754232ff6def038a41385", 16
        );

        public static BigInteger PRIVATE_EXPONENT_411 = new BigInteger("1d", 16);

        public static BigInteger MODULUS_412 = new BigInteger(
            "a465134799cf2c45087093e7d0f0f144" +
            "e6d528110c08f674730d436e40827330" +
            "eccea46e70acf10cdda7d8f710e3b44d" +
            "cca931812d76cd7494289bca8b73823f" +
            "57efc0515b97e4a2a02612ccfa719cf7" +
            "885104b06f2e7e2cc967b62e3d3b1aad" +
            "b925db94cbc8cd3070a4bb13f7e202c7" +
            "733a67b1b94c1ebc0afcbe1a63b448cf", 16
        );

        public static BigInteger PRIVATE_EXPONENT_412 = new BigInteger("25", 16);

        public static string Modulus413 =
            "97df398472ddf737ef0a0cd17e8d172f" +
            "0fef1661a38a8ae1d6e829bc1c6e4c3c" +
            "fc19292dda9ef90175e46e7394a18850" +
            "b6417d03be6eea274d3ed1dde5b5d7bd" +
            "e72cc0a0b71d03608655633881793a02" +
            "c9a67d9ef2b45eb7c08d4be329083ce4" +
            "50e68f7867b6749314d40511d09bc574" +
            "4551baa86a89dc38123dc1668fd72d83";

        public static BigInteger MODULUS_413 = new BigInteger(Modulus413, 16);
        public static string PrivateExponent413 = "00000035";
        public static BigInteger PRIVATE_EXPONENT_413 = new BigInteger("35", 16);

        public static BigInteger MODULUS_414 = new BigInteger(
            "ad70257b2316ce09dfaf2ebc3f63b3d6" +
            "73b0c98a403950e26bb87379b11e17ae" +
            "d0e45af23e7171e5ec1fbc8d1ae32ffb" +
            "7801b31266eef9c334b53469d4b7cbe8" +
            "3284273d35a9aab49b453e7012f37449" +
            "6c65f8089f5d134b0eb3d1e3b22051ed" +
            "5977a6dd68c4f85785dfcc9f4412c816" +
            "81944fc4b8ce27caf0242deaa5762e8d", 16
        );

        public static BigInteger PRIVATE_EXPONENT_414 = new BigInteger("25", 16);

        public static string ModulusL2Encdec =
            "75b4d6de5c016544068a1acf125869f4" +
            "3d2e09fc55b8b1e289556daf9b875763" +
            "5593446288b3653da1ce91c87bb1a5c1" +
            "8f16323495c55d7d72c0890a83f69bfd" +
            "1fd9434eb1c02f3e4679edfa43309319" +
            "070129c267c85604d87bb65bae205de3" +
            "707af1d2108881abb567c3b3d069ae67" +
            "c3a4c6a3aa93d26413d4c66094ae2039";

        public static BigInteger MODULUS_L2ENCDEC = new BigInteger(ModulusL2Encdec, 16
        );

        public static BigInteger PUBLIC_EXPONENT_L2ENCDEC = new BigInteger(
            "30b4c2d798d47086145c75063c8e841e" +
            "719776e400291d7838d3e6c4405b504c" +
            "6a07f8fca27f32b86643d2649d1d5f12" +
            "4cdd0bf272f0909dd7352fe10a77b34d" +
            "831043d9ae541f8263c6fe3d1c14c2f0" +
            "4e43a7253a6dda9a8c1562cbd493c1b6" +
            "31a1957618ad5dfe5ca28553f746e2fc" +
            "6f2db816c7db223ec91e955081c1de65", 16
        );

        public static string PrivateExponentL2Encdec = "0000001d";
        public static BigInteger PRIVATE_EXPONENT_L2ENCDEC = new BigInteger("1d", 16);
        #endregion
        protected RSACipher _cipherEncode;
        protected RSACipher _cipherDecode;
        

        public abstract string Header { get; }
        public string Title { get; set; }
        public Progresser Progress { get; set; }
        public long DataOffset => 28;
        public virtual void GenerateKey(string keyBase)
        {
        }

        protected Lineage2Ver41x()
        {
            Progress = new Progresser()
            {
                State = ProgressState.Indeterminate,
                Message = "RSA codec in progress..."
            };
        }
        protected void InitDecodeCipher(BigInteger modulus, BigInteger exponent)
        {
            this._cipherDecode = new RSACipher();
            RsaKeyParameters privateKey = new RsaKeyParameters(true, modulus, exponent);
            this._cipherDecode.Init(false, privateKey);
        }
        protected void InitEncodeCipher(BigInteger modulus, BigInteger exponent)
        {
            this._cipherEncode = new RSACipher();
            RsaKeyParameters publicKey = new RsaKeyParameters(false, modulus, exponent);
            this._cipherEncode.Init(true, publicKey);
        }
        public virtual byte Decode(byte data)
        {
            throw new NotImplementedException();
        }

        public virtual byte Encode(byte data)
        {
            throw new NotImplementedException();
        }

        public virtual byte[] Decode(byte[] data)
        {
            var decoded = new List<byte>();
            Console.WriteLine("RSA dec call.");
            int rsaBlocks = data.Length / 124;
            Console.WriteLine($"Blocks: {rsaBlocks}");
            Progress.Message = "RSA blocks decryption";
            Progress.Begin(ProgressState.Forward, 0, rsaBlocks);
            for (var i = 0; i < data.Length / 128; i++)
            {
                var buffer = new byte[128];
                Array.Copy(data, 128 * i, buffer, 0, 128);
                buffer = this._cipherDecode.DoFinal(buffer);
                int size = buffer[0];
                if (size > 124)
                {
                    Progress.State = ProgressState.Indeterminate;
                    Progress.Message = "Codec error!";
                    throw new InvalidKeyException("Codec error!");

                }

                var finalBuffer = new byte[size];
                int targetIndex = 125 - size - (124 - size) % 4;
                Array.Copy(buffer, targetIndex, finalBuffer, 0, size);
                decoded.AddRange(finalBuffer);
                Progress.Current = i;
                if (i % 128 == 0)
                {
                    Console.WriteLine($"Block: {i + 1}/{rsaBlocks}...");
                }
            }

            var decodedArray = new byte[decoded.Count - 4];
            for (var i = 0; i < decodedArray.Length; i++)
            {
                decodedArray[i] = decoded[i + 4];
            }
#if DEBUG
            using (BinaryWriter writer = new BinaryWriter(File.Create("dump.bin")))
            {
                writer.Write(decoded.ToArray());
            }
#endif

            Console.WriteLine("RSA dec call completed.");
            Console.WriteLine();
            Progress.Message = "Decompression...";
            Progress.Begin(ProgressState.Indeterminate, 0, 1);
            var decompressed = new MemoryStream(decodedArray).DecompressStream();
            Progress.Message = "RSA decrypted";
            Progress.Begin(ProgressState.Forward, 1, 1, 1);
            return decompressed;
        }


        public virtual byte[] Encode(byte[] data)
        {
            if(this._cipherEncode == null) throw new InvalidKeyException($"There no key to encode using {Header}");
            var compressedData = new List<byte>();

            Progress.Message = "Compression...";
            Progress.Begin(ProgressState.Indeterminate, 0, 1);
            Console.WriteLine($"Beginning encryption and compression - be patient, it may take a while.");
            Console.WriteLine();
            compressedData.AddRange(BitConverter.GetBytes(data.Length));
            compressedData.AddRange(new MemoryStream(data).CompressStream());

            byte[] compressedDataArray = compressedData.ToArray();
#if DEBUG
            using (BinaryWriter writer = new BinaryWriter(File.Create("enc-dump.bin")))
            {
                writer.Write(compressedDataArray);
            }
#endif

            var encryptedData = new List<byte>();
            Console.WriteLine("RSA enc call.");
            int rsaBlocks = compressedDataArray.Length / 124;
            Console.WriteLine($"Blocks: {rsaBlocks}");
            Progress.Message = "RSA blocks encryption";
            int tailSize = compressedDataArray.Length % 124;
            Progress.Begin(ProgressState.Forward, 0, tailSize == 0 ? rsaBlocks : rsaBlocks+1);
            for (var i = 0; i < rsaBlocks; i++)
            {
                var buffer = new byte[125];
                byte size = 124;
                buffer[0] = size;
                Array.Copy(compressedDataArray, 124 * i, buffer, 1, 124);
                buffer = this._cipherEncode.DoFinal(buffer);
                encryptedData.AddRange(buffer);
                Progress.Current = i;
                if (i % 124 == 0)
                {
                    Console.WriteLine($"Block: {i + 1}/{rsaBlocks}...");
                }
            }
            
            if (tailSize != 0)
            {
                Progress.Current++;
                Console.WriteLine($"Tail, size: {tailSize}");
                var bufferFinal = new byte[125];
                bufferFinal[0] = (byte) tailSize;
                int targetIndex = 125 - tailSize - (124 - tailSize) % 4;
                Array.Copy(compressedDataArray, compressedDataArray.Length - tailSize, bufferFinal, targetIndex, tailSize);
                bufferFinal = this._cipherEncode.DoFinal(bufferFinal);
                encryptedData.AddRange(bufferFinal);
            }

            Console.WriteLine("RSA enc call completed.");
            Progress.Message = "RSA encrypted";
            Console.WriteLine();
            encryptedData.AddRange(new byte[20]);

            return encryptedData.ToArray();
        }
    }
}