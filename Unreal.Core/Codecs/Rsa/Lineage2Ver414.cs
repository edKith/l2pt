﻿using System;

namespace Unreal.Core.Codecs.Rsa
{
    public class Lineage2Ver414 : Lineage2Ver41x
    {
        private static ICodec _instance;
        public static ICodec Instance => _instance ?? (_instance = new Lineage2Ver414());
        public override string Header => nameof(Lineage2Ver414);

        public Lineage2Ver414()
        {
            try
            {
                InitDecodeCipher(MODULUS_414, PRIVATE_EXPONENT_414);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        
    }
}