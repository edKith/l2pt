﻿using System;

namespace Unreal.Core.Codecs.Rsa
{
    public class Lineage2Ver413ReadOnly : Lineage2Ver41x
    {
        private static ICodec _instance;
        public static ICodec Instance => _instance ?? (_instance = new Lineage2Ver413ReadOnly());
        public override string Header => nameof(Lineage2Ver413);

        public Lineage2Ver413ReadOnly()
        {
            try
            {
                InitDecodeCipher(MODULUS_413, PRIVATE_EXPONENT_413);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}