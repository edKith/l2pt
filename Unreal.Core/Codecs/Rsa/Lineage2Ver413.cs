﻿using System;

namespace Unreal.Core.Codecs.Rsa
{
    public class Lineage2Ver413 : Lineage2Ver41x
    {
        private static ICodec _instance;
        public static ICodec Instance => _instance ?? (_instance = new Lineage2Ver413());
        public override string Header => nameof(Lineage2Ver413);

        public Lineage2Ver413()
        {
            try
            {
                InitDecodeCipher(MODULUS_L2ENCDEC, PRIVATE_EXPONENT_L2ENCDEC);
                InitEncodeCipher(MODULUS_L2ENCDEC, PUBLIC_EXPONENT_L2ENCDEC);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}