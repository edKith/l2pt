﻿using System;

namespace Unreal.Core.Codecs.Rsa
{
    public class Lineage2Ver411 : Lineage2Ver41x
    {
        private static ICodec _instance;
        public static ICodec Instance => _instance ?? (_instance = new Lineage2Ver411());
        public override string Header => nameof(Lineage2Ver411);

        public Lineage2Ver411()
        {
            try
            {
                InitDecodeCipher(MODULUS_411, PRIVATE_EXPONENT_411);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}