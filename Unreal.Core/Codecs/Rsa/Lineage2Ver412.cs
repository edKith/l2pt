﻿using System;

namespace Unreal.Core.Codecs.Rsa
{
    public class Lineage2Ver412 : Lineage2Ver41x
    {
        private static ICodec _instance;
        public static ICodec Instance => _instance ?? (_instance = new Lineage2Ver412());
        public override string Header => nameof(Lineage2Ver412);

        public Lineage2Ver412()
        {
            try
            {
                InitDecodeCipher(MODULUS_412, PRIVATE_EXPONENT_412);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}