﻿using System.IO;
using Unreal.Core.Codecs.Rsa;
using Unreal.Core.Codecs.Xor;
using Unreal.Core.IO;
using Unreal.Core.IO.Interfaces;

namespace Unreal.Core.Codecs
{
    public static class CodecFactory
    {
        public static int Lineage2VerxxxLength = 28;

        public static void PeekCodec(IEncodedReader reader)
        {
            string header = reader.PeekHeader();
            ICodec codec = PeekFromNumber(PeekNumber(header));
            codec.GenerateKey(reader.EncodeKeyBase);

            reader.Codec = codec;
        }

        public static int PeekNumber(string header)
        {
            if (!header.StartsWith("Lineage2Ver"))
            {
                return -1;
            }

            return int.Parse(header.Substring(11));
        }

        public static void SetCodec(IEncodedStream writer, int version)
        {
            ICodec codec = PeekFromNumber(version);
            codec.GenerateKey(writer.EncodeKeyBase);
            writer.Codec = codec;
        }

        public static ICodec PeekFromNumber(int version)
        {
            ICodec codec = new Lineage2NullCodec();
            switch (version)
            {
                case 111:
                    codec = Lineage2Ver111.Instance;
                    break;
                case 121:
                    codec = Lineage2Ver121.Instance;
                    break;
                case 411:
                    codec = Lineage2Ver411.Instance;
                    break;
                case 412:
                    codec = Lineage2Ver412.Instance;
                    break;
                case 413:
                    codec = Lineage2Ver413.Instance;
                    break;
                case 414:
                    codec = Lineage2Ver413.Instance;
                    break;
                default:
                    break;
            }

            return codec;
        }

        public static ICodec PeekFromExtension(FileInfo info)
        {
            string upperExt = info.Extension.ToUpperInvariant();
            ICodec codec = Lineage2NullCodec.Instance;
            switch (upperExt)
            {
                case ".UTX":
                    codec = Lineage2Ver121.Instance;
                    break;
                case ".DAT":
                    codec = Lineage2Ver413.Instance;
                    break;
                default:
                    codec = Lineage2Ver111.Instance;
                    break;
            }

            switch (info.Name.ToUpperInvariant())
            {
                case "L2.INI":
                case "USER.INI":
                    codec = Lineage2Ver413.Instance;
                    break;
            }

            return codec;
        }
    }
}