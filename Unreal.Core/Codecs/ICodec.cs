﻿using Unreal.Core.Output;

namespace Unreal.Core.Codecs
{
    public interface ICodec : IProgressable
    {
        long DataOffset { get; }
        void GenerateKey(string keyBase);
        byte Decode(byte data);
        byte Encode(byte data);
        byte[] Decode(byte[] data);
        byte[] Encode(byte[] data);

        string Header { get; }
    }
}