﻿using System;

namespace Unreal.Core.Output
{
    public class Progresser
    {
        private ProgressState _state;
        private string _message = "";
        private double _minimum;
        private double _current;
        private double _maximum;

        public delegate void UpdateStateHandler(ProgressState state);
        public delegate void UpdateMessageHandler(string message);
        public delegate void UpdateMinimumHandler(double minimum);
        public delegate void UpdateCurrentHandler(double current);
        public delegate void UpdateMaximumHandler(double maximum);


        public event UpdateStateHandler StateUpdated;
        public event UpdateMessageHandler MessageUpdated;
        public event UpdateMinimumHandler MinimumUpdated;
        public event UpdateCurrentHandler CurrentUpdated;
        public event UpdateMaximumHandler MaximumUpdated;

        public ProgressState State
        {
            get => _state;
            set
            {
                if (_state == value) return;
                _state = value;
                StateUpdated?.Invoke(_state);
            }
        }

        public string Message
        {
            get => _message;
            set
            {
                if (_message.Equals(value, StringComparison.InvariantCulture)) return;
                _message = value;
                MessageUpdated?.Invoke(_message);
            }
        }

        public double Minimum
        {
            get => _minimum;
            set
            {
                if (_minimum == value) return;
                _minimum = value;
                MinimumUpdated?.Invoke(_minimum);
            }
        }

        public double Current
        {
            get => _current;
            set
            {
                if (_current == value) return;
                _current = value;
                CurrentUpdated?.Invoke(_current);
            }
        }

        public double Maximum
        {
            get => _maximum;
            set
            {
                if (_maximum == value) return;
                _maximum = value;
                MaximumUpdated?.Invoke(_maximum);
            }
        }

        public void Begin(ProgressState state, double minimum, double maximum)
        {
            Begin(state, minimum, maximum, 0);
        }
        public void Begin(ProgressState state, double minimum, double maximum, double current)
        {
            State = state;
            Minimum = minimum;
            Maximum = maximum;
            Current = current;
        }

        public void Reset()
        {
            Reset(ProgressState.Indeterminate, 0, 0, 0);
        }
        public void Reset(ProgressState state)
        {
            Reset(state, 0, 0, 0);
        }
        public void Reset(ProgressState state, double minimum, double maximum, double current)
        {
            State = state;
            Minimum = minimum;
            Maximum = maximum;
            Current = current;
        }
    }
}