﻿namespace Unreal.Core.Output
{
    public interface IProgressable
    {
        string Title { get; set; }
        Progresser Progress { get; set; }
    }
}