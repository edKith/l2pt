﻿using System.Dynamic;

namespace Unreal.Core.Output
{
    public class NullProgressable : IProgressable
    {
        private static readonly NullProgressable instance = new NullProgressable();

        public static NullProgressable Instance => instance;

        public string Title { get; set; }
        public Progresser Progress { get; set; }

        public NullProgressable()
        {
            Title = "Null progress process";
            Progress = new Progresser
            {
                Message = "nothing",
                State = ProgressState.Indeterminate
            };
        }
    }
}