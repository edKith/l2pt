﻿namespace Unreal.Core.Output
{
    public enum ProgressState
    {
        None,
        Indeterminate,
        Forward,
        Backward
    }
}