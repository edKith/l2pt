﻿using Unreal.Core.IO;

namespace Unreal.Core.Objects
{
    public interface IUnrealSerializable
    {
        void Read(FileReader reader);
        void Write(FileWriter writer);
    }
}