﻿using Unreal.Core.IO;

namespace Unreal.Core.Objects.Misc
{
    public class GUID : IUnrealSerializable
    {
        public uint[] Data = new uint[4];
        public void Read(FileReader reader)
        {
            var i = 0;
            this.Data[i++] = reader.ReadUInt32();
            this.Data[i++] = reader.ReadUInt32();
            this.Data[i++] = reader.ReadUInt32();
            this.Data[i++] = reader.ReadUInt32();
        }

        public void Write(FileWriter writer)
        {
            var i = 0;
            writer.Write(this.Data[i++]);
            writer.Write(this.Data[i++]);
            writer.Write(this.Data[i++]);
            writer.Write(this.Data[i++]);
        }
    }
}