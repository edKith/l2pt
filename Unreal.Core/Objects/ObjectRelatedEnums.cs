﻿using System;

namespace Unreal.Core.Objects
{
    [Flags]
    public enum EObjectFlags : uint
    {
        RF_Transactional = 0x00000001, // Object is transactional.
        RF_Unreachable = 0x00000002, // Object is not reachable on the object graph.
        RF_Public = 0x00000004, // Object is visible outside its package.
        RF_TagImp = 0x00000008, // Temporary import tag in load/save.
        RF_TagExp = 0x00000010, // Temporary export tag in load/save.
        RF_SourceModified = 0x00000020, // Modified relative to source files.
        RF_TagGarbage = 0x00000040, // Check during garbage collection.
        RF_Final = 0x00000080, // Object is not visible outside of class.
        RF_PerObjectLocalized = 0x00000100, // Object is localized by instance name, not by class.
        RF_NeedLoad = 0x00000200, // During load, indicates object needs loading.
        RF_HighlightedName = 0x00000400, // A hardcoded name which should be syntax-highlighted.
        RF_EliminateObject = 0x00000400, // NULL out references to this during garbage collecion.
        RF_InSingularFunc = 0x00000800, // In a singular function.
        RF_RemappedName = 0x00000800, // Name is remapped.
        RF_Suppress = 0x00001000, //warning: Mirrored in UnName.h. Suppressed log name.
        RF_StateChanged = 0x00001000, // Object did a state change.
        RF_InEndState = 0x00002000, // Within an EndState call.
        RF_Transient = 0x00004000, // Don't save object.
        RF_Preloading = 0x00008000, // Data is being preloaded from file.
        RF_LoadForClient = 0x00010000, // In-file load for client.
        RF_LoadForServer = 0x00020000, // In-file load for client.
        RF_LoadForEdit = 0x00040000, // In-file load for client.
        RF_Standalone = 0x00080000, // Keep object around for editing even if unreferenced.
        RF_NotForClient = 0x00100000, // Don't load this object for the game client.
        RF_NotForServer = 0x00200000, // Don't load this object for the game server.
        RF_NotForEdit = 0x00400000, // Don't load this object for the editor.
        RF_Destroyed = 0x00800000, // Object Destroy has already been called.
        RF_NeedPostLoad = 0x01000000, // Object needs to be postloaded.
        RF_HasStack = 0x02000000, // Has execution stack.
        RF_Native = 0x04000000, // Native (UClass only).
        RF_Marked = 0x08000000, // Marked (for debugging).
        RF_ErrorShutdown = 0x10000000, // ShutdownAfterError called.
        RF_DebugPostLoad = 0x20000000, // For debugging Serialize calls.
        RF_DebugSerialize = 0x40000000, // For debugging Serialize calls.
        RF_DebugDestroy = 0x80000000, // For debugging Destroy calls.
        RF_ContextFlags = RF_NotForClient | RF_NotForServer | RF_NotForEdit, // All context flags.
        RF_LoadContextFlags = RF_LoadForClient | RF_LoadForServer | RF_LoadForEdit, // Flags affecting loading.

        RF_Load = RF_ContextFlags | RF_LoadContextFlags | RF_Public | RF_Final | RF_Standalone | RF_Native |
            RF_SourceModified | RF_Transactional | RF_HasStack |
            RF_PerObjectLocalized, // Flags to load from Unrealfiles.
        RF_Keep = RF_Native | RF_Marked | RF_PerObjectLocalized, // Flags to persist across loads.

        RF_ScriptMask =
            RF_Transactional | RF_Public | RF_Final | RF_Transient | RF_NotForClient | RF_NotForServer | RF_NotForEdit |
            RF_Standalone // Script-accessible flags.
    }

    [Flags]
    public enum EPropertyFlags
    {
        // Regular flags.
        CPF_Edit = 0x00000001, // Property is user-settable in the editor.
        CPF_Const = 0x00000002, // Actor's property always matches class's default actor property.
        CPF_Input = 0x00000004, // Variable is writable by the input system.
        CPF_ExportObject = 0x00000008, // Object can be exported with actor.
        CPF_OptionalParm = 0x00000010, // Optional parameter (if CPF_Param is set).
        CPF_Net = 0x00000020, // Property is relevant to network replication.
        CPF_EditConstArray = 0x00000040, // Prevent adding/removing of items from dynamic a array in the editor.
        CPF_Parm = 0x00000080, // Function/When call parameter.
        CPF_OutParm = 0x00000100, // Value is copied out after function call.
        CPF_SkipParm = 0x00000200, // Property is a short-circuitable evaluation function parm.
        CPF_ReturnParm = 0x00000400, // Return value.
        CPF_CoerceParm = 0x00000800, // Coerce args into this function parameter.
        CPF_Native = 0x00001000, // Property is native: C++ code is responsible for serializing it.
        CPF_Transient = 0x00002000, // Property is transient: shouldn't be saved, zero-filled at load time.
        CPF_Config = 0x00004000, // Property should be loaded/saved as permanent profile.
        CPF_Localized = 0x00008000, // Property should be loaded as localizable text.
        CPF_Travel = 0x00010000, // Property travels across levels/servers.
        CPF_EditConst = 0x00020000, // Property is uneditable in the editor.
        CPF_GlobalConfig = 0x00040000, // Load config from base class, not subclass.
        CPF_OnDemand = 0x00100000, // Object or dynamic array loaded on demand only.
        CPF_New = 0x00200000, // Automatically create inner object.
        CPF_NeedCtorLink = 0x00400000, // Fields need construction/destruction.
        CPF_NoExport = 0x00800000, // Property should not be exported to the native class header file.
        CPF_Button = 0x01000000, // String that has "Go" button which allows it to call functions from UEd.
        CPF_CommentString = 0x02000000, // Property has a comment string visible via the property browser
        CPF_EditInline = 0x04000000, // Edit this object reference inline.
        CPF_EdFindable = 0x08000000, // References are set by clicking on actors in the editor viewports.
        CPF_EditInlineUse = 0x10000000, // EditInline with Use button.
        CPF_Deprecated = 0x20000000, // Property is deprecated.  Read it from an archive, but don't save it.
        CPF_EditInlineNotify = 0x40000000, // EditInline, notify outer object on editor change.


        // Combinations of flags.
        CPF_ParmFlags = CPF_OptionalParm | CPF_Parm | CPF_OutParm | CPF_SkipParm | CPF_ReturnParm | CPF_CoerceParm,
        CPF_PropagateFromStruct = CPF_Const | CPF_Native | CPF_Transient,

        CPF_PropagateToArrayInner =
            CPF_ExportObject | CPF_EditInline | CPF_EditInlineUse | CPF_EditInlineNotify | CPF_Localized // gam
    }
    [Flags]
    public enum EPackageFlags
    {
        PKG_AllowDownload = 0x0001, // Allow downloading package.
        PKG_ClientOptional = 0x0002,    // Purely optional for clients.
        PKG_ServerSideOnly = 0x0004,   // Only needed on the server side.
        PKG_BrokenLinks = 0x0008,   // Loaded from linker with broken import links.
        PKG_Unsecure = 0x0010,   // Not trusted.
        PKG_Need = 0x8000,  // Client needs to download this package.
    };
}