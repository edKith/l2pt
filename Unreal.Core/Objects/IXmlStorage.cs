﻿using System.IO;
using System.Xml.Linq;

namespace Unreal.Core.Objects
{
    public interface IXmlStorage
    {
        XElement ToXml();
        bool FromXml(XElement rootElement);
    }
}