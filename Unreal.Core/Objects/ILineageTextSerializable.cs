﻿namespace Unreal.Core.Objects
{
    public interface ILineageTextSerializable
    {
        //TODO: Plain text data reader-writer
        string ToText();
        bool FromText();
    }
}