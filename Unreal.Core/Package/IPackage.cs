﻿using System.IO;
using Unreal.Core.IO;
using Unreal.Core.Objects;

namespace Unreal.Core.Package
{
    public interface IPackage : IUnrealSerializable
    {
        FileInfo PackageFileEntry { get; set; }
    }
}