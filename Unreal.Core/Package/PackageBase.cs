﻿using System.IO;
using Unreal.Core.IO;
using Unreal.Core.Objects;
using Unreal.Core.Package.Parts;

namespace Unreal.Core.Package
{
    public class PackageBase : IPackage
    {
        public Summary Summary;
        public NameEntry[] NameTable;
        public ExportEntry[] ExportTable;
        public ImportEntry[] ImportTable;
        public RawDataEntry[] RawDatas;
        public FileInfo PackageFileEntry { get; set; }

        public ObjectEntry this[int number]
        {
            get
            {
                if (number > 0)
                {
                    return this.ExportTable[number - 1];
                }
                if (number < 0)
                {
                    return this.ImportTable[-number - 1];
                }

                return ObjectEntry.None;
            }
        }

        public string GetName(int index)
        {
            return this.NameTable[index].Name;
        }
        public string GetObjectName(int index)
        {
            return GetName(this[index].NameIndex);
        }

        public PackageBase(FileInfo packageFileEntry)
        {
            PackageFileEntry = packageFileEntry;
        }
        
        public void Read(FileReader reader)
        {
            this.Summary = reader.Read<Summary>();
            this.NameTable = reader.ReadArray<NameEntry>(this.Summary.NameTableOffset, this.Summary.NameTableSize);
            this.ImportTable = reader.ReadArray<ImportEntry>(this.Summary.ImportTableOffset, this.Summary.ImportTableSize);
            this.ExportTable = reader.ReadArray<ExportEntry>(this.Summary.ExportTableOffset, this.Summary.ExportTableSize);

            this.RawDatas = new RawDataEntry[this.Summary.ExportTableSize];
            for (int i = 0; i < this.Summary.ExportTableSize; i++)
            {
                this.RawDatas[i] = new RawDataEntry(){Offset = this.ExportTable[i].DataOffset, Size = this.ExportTable[i].DataSize};
                this.RawDatas[i].Read(reader);
            }
        }

        public void Write(FileWriter writer)
        {
            writer.Write(this.Summary);
            writer.WriteArray(this.NameTable, this.Summary.NameTableOffset, this.Summary.NameTableSize);
            writer.WriteArray(this.ImportTable, this.Summary.ImportTableOffset, this.Summary.ImportTableSize);
            writer.WriteArray(this.ExportTable, this.Summary.ExportTableOffset, this.Summary.ExportTableSize);
            for (int i = 0; i < this.Summary.ExportTableSize; i++)
            {
                this.RawDatas[i].Write(writer);
            }
        }
    }
}