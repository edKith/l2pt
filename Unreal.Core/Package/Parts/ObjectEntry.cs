﻿using Unreal.Core.IO;
using Unreal.Core.Objects;

namespace Unreal.Core.Package.Parts
{
    public class ObjectEntry : IUnrealSerializable
    {
        public static ObjectEntry None = new ObjectEntry(){NameIndex=0, ObjectClass = 0, ObjectSubPackage = 0};
        public int NameIndex;
        public int ObjectClass;
        public int ObjectSubPackage;

        public virtual void Read(FileReader reader)
        {
            ObjectClass = reader.ReadCompactInt();
            ObjectSubPackage = reader.ReadInt32();
            NameIndex = reader.ReadCompactInt();
        }

        public virtual void Write(FileWriter writer)
        {
            writer.WriteCompactInt(ObjectClass);
            writer.Write(ObjectSubPackage);
            writer.WriteCompactInt(NameIndex);
        }
    }
}