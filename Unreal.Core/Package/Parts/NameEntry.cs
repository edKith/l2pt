﻿using Unreal.Core.IO;
using Unreal.Core.Objects;

namespace Unreal.Core.Package.Parts
{
    public class NameEntry : IUnrealSerializable
    {
        public NameEntry()
        {
            
        }
        public string Name;
        public EObjectFlags Flags;
        public void Read(FileReader reader)
        {
            Name = reader.ReadString();
            Flags = (EObjectFlags)reader.ReadUInt32();
        }

        public void Write(FileWriter writer)
        {
            writer.Write(this.Name);
            writer.Write((uint)Flags);
        }
    }
}