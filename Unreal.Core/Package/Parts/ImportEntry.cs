﻿using Unreal.Core.IO;

namespace Unreal.Core.Package.Parts
{
    public class ImportEntry : ObjectEntry
    {
        public int Package;

        public ImportEntry()
        {
            
        }
        public override void Read(FileReader reader)
        {
            Package = reader.ReadCompactInt();
            base.Read(reader);
        }

        public override void Write(FileWriter writer)
        {
            writer.WriteCompactInt(this.Package);
            base.Write(writer);
        }
    }
}