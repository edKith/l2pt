﻿using Unreal.Core.IO;
using Unreal.Core.IO.Interfaces;
using Unreal.Core.Objects;

namespace Unreal.Core.Package.Parts
{
    public class GenerationInfo : IUnrealSerializable
    {

        public int ExportCount;
        public int NameCount;
        
        public void Read(FileReader reader)
        {
            ExportCount = reader.ReadInt32();
            NameCount = reader.ReadInt32();
        }
        public void Write(FileWriter writer)
        {
            writer.Write(this.ExportCount);
            writer.Write(this.NameCount);
        }
    }
}