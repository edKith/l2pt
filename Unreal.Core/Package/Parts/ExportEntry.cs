﻿using Unreal.Core.IO;
using Unreal.Core.Objects;

namespace Unreal.Core.Package.Parts
{
    public class ExportEntry : ObjectEntry
    {
        public int SuperClass;

        public EObjectFlags Flags;
        
        public int DataSize;

        public int DataOffset;

        public ExportEntry()
        {
            
        }

        public override void Read(FileReader reader)
        {
            ObjectClass = reader.ReadCompactInt();
            SuperClass = reader.ReadCompactInt();
            ObjectSubPackage = reader.ReadInt32();
            NameIndex = reader.ReadCompactInt();
            Flags = (EObjectFlags) reader.ReadUInt32();
            DataSize = reader.ReadCompactInt();
            DataOffset = reader.ReadCompactInt();
        }
        public override void Write(FileWriter writer)
        {
            writer.WriteCompactInt(ObjectClass);
            writer.WriteCompactInt(SuperClass);
            writer.Write(ObjectSubPackage);
            writer.WriteCompactInt(NameIndex);
            writer.Write((uint)Flags);
            writer.WriteCompactInt(DataSize);
            writer.WriteCompactInt(DataOffset);
        }
    }
}