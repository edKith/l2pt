﻿using Unreal.Core.Exceptions;
using Unreal.Core.IO;
using Unreal.Core.IO.Interfaces;
using Unreal.Core.Objects;
using Unreal.Core.Objects.Misc;

namespace Unreal.Core.Package.Parts
{
    public class Summary : IUnrealSerializable
    {

        public const uint UnrealFileTag = 0x9E2a83c1;

        public ushort Version;

        public ushort LicenseeVersion;

        public EPackageFlags PackageFlags;

        public int NameTableSize;

        public int NameTableOffset;

        public int ExportTableSize;

        public int ExportTableOffset;

        public int ImportTableSize;

        public int ImportTableOffset;

        public GUID GUID = new GUID();
        

        public GenerationInfo[] Generations;
        public void Read(FileReader reader)
        {
            if (reader.ReadUInt32() == UnrealFileTag)
            {
                Version = reader.ReadUInt16();
                LicenseeVersion = reader.ReadUInt16();

                this.PackageFlags = (EPackageFlags)reader.ReadInt32();
                NameTableSize = reader.ReadInt32();
                NameTableOffset = reader.ReadInt32();
                ExportTableSize = reader.ReadInt32();
                ExportTableOffset = reader.ReadInt32();
                ImportTableSize = reader.ReadInt32();
                ImportTableOffset = reader.ReadInt32();

                this.GUID = reader.Read<GUID>();
                this.Generations = reader.ReadArray<GenerationInfo>();
            }
            else
            {
                throw new UnrealInvalidPackageHeader();
            }
        }

        public void Write(FileWriter writer)
        {
            writer.Write(UnrealFileTag);

            writer.Write(this.Version);
            writer.Write(this.LicenseeVersion);

            writer.Write((int)this.PackageFlags);
            writer.Write(this.NameTableSize);
            writer.Write(this.NameTableOffset);
            writer.Write(this.ExportTableSize);
            writer.Write(this.ExportTableOffset);
            writer.Write(this.ImportTableSize);
            writer.Write(this.ImportTableOffset);

            writer.Write(this.GUID);

            writer.WriteArray(this.Generations);
        }

    }
}