﻿using Unreal.Core.IO;
using Unreal.Core.Objects;

namespace Unreal.Core.Package.Parts
{
    public class RawDataEntry : IUnrealSerializable
    {
        public int Size;
        public int Offset;
        public byte[] Data;
        public void Read(FileReader reader)
        {
            reader.StreamPosition = this.Offset;
            this.Data = reader.ReadBytes(this.Size);
        }

        public void Write(FileWriter writer)
        {
            writer.StreamPosition = this.Offset;
            writer.Write(this.Data);
        }
    }
}