using System;
using System.Collections.Generic;
using System.Text;
using Unreal.Core.IniFileParser.Model.Configuration;

namespace Unreal.Core.IniFileParser.Model.Formatting
{
    
    public class DefaultIniDataFormatter : IIniDataFormatter
    {
        IniParserConfiguration _configuration;
        
        #region Initialization
        public DefaultIniDataFormatter():this(new IniParserConfiguration()) {}
        
        public DefaultIniDataFormatter(IniParserConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException("configuration");
            this.Configuration = configuration;
            this.Configuration.AssigmentSpacer = string.Empty;
        }
        #endregion
        
        public virtual string IniDataToString(IniData iniData)
        {
            var sb = new StringBuilder();

            if (this.Configuration.AllowKeysWithoutSection)
            {
                // Write global key/value data
                this.WriteKeyValueData(iniData.Global, sb);
            }

            //Write sections
            foreach (SectionData section in iniData.Sections)
            {
                //Write current section
                this.WriteSection(section, sb);
            }

            return sb.ToString();
        }
        
        /// <summary>
        ///     Configuration used to write an ini file with the proper
        ///     delimiter characters and data.
        /// </summary>
        /// <remarks>
        ///     If the <see cref="IniData"/> instance was created by a parser,
        ///     this instance is a copy of the <see cref="IniParserConfiguration"/> used
        ///     by the parser (i.e. different objects instances)
        ///     If this instance is created programatically without using a parser, this
        ///     property returns an instance of <see cref=" IniParserConfiguration"/>
        /// </remarks>
        public IniParserConfiguration Configuration
        {
            get { return this._configuration; }
            set { this._configuration = value.Clone(); }
        }

        #region Helpers

        private void WriteSection(SectionData section, StringBuilder sb)
        {
            // Write blank line before section, but not if it is the first line
            if (sb.Length > 0) sb.Append(this.Configuration.NewLineStr);

            // Leading comments
            this.WriteComments(section.LeadingComments, sb);

            //Write section name
            sb.Append(string.Format("{0}{1}{2}{3}", 
                this.Configuration.SectionStartChar, 
                section.SectionName, 
                this.Configuration.SectionEndChar, 
                this.Configuration.NewLineStr));

            this.WriteKeyValueData(section.Keys, sb);

            // Trailing comments
            this.WriteComments(section.TrailingComments, sb);
        }

        private void WriteKeyValueData(KeyDataCollection keyDataCollection, StringBuilder sb)
        {

            foreach (KeyData keyData in keyDataCollection)
            {
                // Add a blank line if the key value pair has comments
                if (keyData.Comments.Count > 0) sb.Append(this.Configuration.NewLineStr);

                // Write key comments
                this.WriteComments(keyData.Comments, sb);
                if (Configuration.ConcatenateDuplicateKeys &&
                    keyData.Value.Contains(Configuration.ConcatenateSeparator))
                {
                    var multipleValues = keyData.Value.Split(new[] { Configuration.ConcatenateSeparator },
                        StringSplitOptions.RemoveEmptyEntries);
                    foreach (string multipleValue in multipleValues)
                    {

                        sb.Append(string.Format("{0}{3}{1}{3}{2}{4}",
                            keyData.KeyName,
                            this.Configuration.KeyValueAssigmentChar,
                            multipleValue,
                            this.Configuration.AssigmentSpacer,
                            this.Configuration.NewLineStr));
                    }
                }
                else
                {
                    //Write key and value
                    sb.Append(string.Format("{0}{3}{1}{3}{2}{4}",
                        keyData.KeyName,
                        this.Configuration.KeyValueAssigmentChar,
                        keyData.Value,
                        this.Configuration.AssigmentSpacer,
                        this.Configuration.NewLineStr));
                }
            }
        }

        private void WriteComments(List<string> comments, StringBuilder sb)
        {
            foreach (string comment in comments)
                sb.Append(string.Format("{0}{1}{2}", this.Configuration.CommentString, comment, this.Configuration.NewLineStr));
        }
        #endregion
        
    }
    
} 