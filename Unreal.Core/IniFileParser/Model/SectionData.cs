using System;
using System.Collections.Generic;

namespace Unreal.Core.IniFileParser.Model
{
    /// <summary>
    ///     Information associated to a section in a INI File
    ///     Includes both the value and the comments associated to the key.
    /// </summary>
    public class SectionData : ICloneable
    {
        IEqualityComparer<string> _searchComparer;
        #region Initialization

        public SectionData(string sectionName)
            :this(sectionName, EqualityComparer<string>.Default)
        {
            
        }
        /// <summary>
        ///     Initializes a new instance of the <see cref="SectionData"/> class.
        /// </summary>
        public SectionData(string sectionName, IEqualityComparer<string> searchComparer)
        {
            this._searchComparer = searchComparer;

            if (string.IsNullOrEmpty(sectionName))
                throw new ArgumentException("section name can not be empty");

            this._leadingComments = new List<string>();
            this._keyDataCollection = new KeyDataCollection(this._searchComparer);
            this.SectionName = sectionName;
        }


        /// <summary>
        ///     Initializes a new instance of the <see cref="SectionData"/> class
        ///     from a previous instance of <see cref="SectionData"/>.
        /// </summary>
        /// <remarks>
        ///     Data is deeply copied
        /// </remarks>
        /// <param name="ori">
        ///     The instance of the <see cref="SectionData"/> class 
        ///     used to create the new instance.
        /// </param>
        /// <param name="searchComparer">
        ///     Search comparer.
        /// </param>
        public SectionData(SectionData ori, IEqualityComparer<string> searchComparer = null)
        {
            this.SectionName = ori.SectionName;

            this._searchComparer = searchComparer;
            this._leadingComments = new List<string>(ori._leadingComments);
            this._keyDataCollection = new KeyDataCollection(ori._keyDataCollection, searchComparer ?? ori._searchComparer);
        }

        #endregion

		#region Operations

        /// <summary>
        ///     Deletes all comments in this section and key/value pairs
        /// </summary>
        public void ClearComments()
        {
            this.LeadingComments.Clear();
            this.TrailingComments.Clear();
            this.Keys.ClearComments();
        }

        /// <summary>
        /// Deletes all the key-value pairs in this section.
        /// </summary>
		public void ClearKeyData()
		{
			this.Keys.RemoveAllKeys();
		}

        /// <summary>
        ///     Merges otherSection into this, adding new keys if they don't exists
        ///     or overwriting values if the key already exists.
        /// Comments get appended.
        /// </summary>
        /// <remarks>
        ///     Comments are also merged but they are always added, not overwritten.
        /// </remarks>
        /// <param name="toMergeSection"></param>
        public void Merge(SectionData toMergeSection)
        {
            foreach (var comment in toMergeSection.LeadingComments) 
                this.LeadingComments.Add(comment);
                
            this.Keys.Merge(toMergeSection.Keys);

            foreach(var comment in toMergeSection.TrailingComments) 
                this.TrailingComments.Add(comment);
        }

		#endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the name of the section.
        /// </summary>
        /// <value>
        ///     The name of the section
        /// </value>
        public string SectionName
        {
            get
            {
                return this._sectionName;
            }

            set
            {
                if (!string.IsNullOrEmpty(value))
                    this._sectionName = value;
            }
        }


		[Obsolete("Do not use this property, use property Comments instead")]
        public List<string> LeadingComments
        {
            get
            {
                return this._leadingComments;
            }

            internal set
            {
                this._leadingComments = new List<string>(value);
            }
        }

        /// <summary>
        ///     Gets or sets the comment list associated to this section.
        /// </summary>
        /// <value>
        ///     A list of strings.
        /// </value>
        public List<string> Comments
        {
            get
            {
				return this._leadingComments;
            }


        }

		[Obsolete("Do not use this property, use property Comments instead")]
        public List<string> TrailingComments
        {
            get
            {
                return this._trailingComments;
            }

            internal set
            {
                this._trailingComments = new List<string>(value);
            }
        }
        /// <summary>
        ///     Gets or sets the keys associated to this section.
        /// </summary>
        /// <value>
        ///     A collection of KeyData objects.
        /// </value>
        public KeyDataCollection Keys
        {
            get
            {
                return this._keyDataCollection;
            }

            set
            {
                this._keyDataCollection = value;
            }
        }

        #endregion

        #region ICloneable Members

        /// <summary>
        ///     Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        ///     A new object that is a copy of this instance.
        /// </returns>
        public object Clone()
        {
            return new SectionData(this);
        }

        #endregion

        #region Non-public members

        // Comments associated to this section
        private List<string> _leadingComments;
        private List<string> _trailingComments = new List<string>();

        // Keys associated to this section
        private KeyDataCollection _keyDataCollection;

        private string _sectionName;
        #endregion



    }
}