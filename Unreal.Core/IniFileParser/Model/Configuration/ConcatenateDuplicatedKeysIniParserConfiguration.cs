namespace Unreal.Core.IniFileParser.Model.Configuration
{
    public class ConcatenateDuplicatedKeysIniParserConfiguration : IniParserConfiguration
    {
        public new bool AllowDuplicateKeys { get {return true; }}
        public ConcatenateDuplicatedKeysIniParserConfiguration()
            :base()
        {
            this.ConcatenateSeparator = "\u007f";
        }

        public ConcatenateDuplicatedKeysIniParserConfiguration(ConcatenateDuplicatedKeysIniParserConfiguration ori)
            :base(ori)
        {
            this.ConcatenateSeparator = ori.ConcatenateSeparator;
        }

    }

}