﻿using System;
using System.IO;
using System.Text;
using Unreal.Core.Codecs;
using Unreal.Core.IniFileParser.Exceptions;
using Unreal.Core.IniFileParser.Model;
using Unreal.Core.IniFileParser.Parser;
using Unreal.Core.IO;

namespace Unreal.Core.IniFileParser
{
    public class BufferedIniParser : StreamIniDataParser
    {
        /// <summary>
        ///     Ctor
        /// </summary>
        public BufferedIniParser() { }

        /// <summary>
        ///     Ctor
        /// </summary>
        /// <param name="parser"></param>
        public BufferedIniParser(IniDataParser parser) : base(parser)
        {
            this.Parser = parser;
        }

        /// <summary>
        ///     Implements reading ini data from a file.
        /// </summary>
        /// <remarks>
        ///     Uses <see cref="Encoding.Default"/> codification for the file.
        /// </remarks>
        /// <param name="fileInfo">
        ///     Ini file FileInfo
        /// </param>
        public IniData ReadFile(FileInfo fileInfo)
        {
            if (!fileInfo.Exists)
                throw new FileNotFoundException($"{fileInfo.FullName} is not exists!");

            try
            {
                // (FileAccess.Read) we want to open the ini only for reading 
                // (FileShare.ReadWrite) any other process should still have access to the ini file 
                using (var bufferedStream = new BufferedReader(fileInfo))
                {
                    var allBytes = bufferedStream.Read();

                    using (StreamReader sr = new StreamReader(new MemoryStream(allBytes)))
                    {
                        return this.ReadData(sr);
                    }
                }
            }
            catch (IOException ex)
            {
                throw new ParsingException(String.Format("Could not parse file {0}", fileInfo.FullName), ex);
            }
        }


        /// <summary>
        ///     Writes INI data to a text file.
        /// </summary>
        /// <param name="fileInfo">
        ///     Target file FileInfo
        /// </param>
        /// <param name="parsedData">
        ///     IniData to be saved as an INI file.
        /// </param>
        /// <param name="codec">
        ///     Lineage 2 file codec
        /// </param>
        public void WriteFile(FileInfo fileInfo, IniData parsedData, ICodec codec)
        {
            if (parsedData == null)
                throw new ArgumentNullException("parsedData");

            var memoryBuffer = new MemoryStream();
            using (StreamWriter sr = new StreamWriter(memoryBuffer, Encoding.UTF8))
            {
                this.WriteData(sr, parsedData);
            }
            using (BufferedWriter bufferedStream = new BufferedWriter(fileInfo, codec))
            {
                bufferedStream.Write(memoryBuffer.ToArray());
            }
        }
    }
}