﻿using System;
using System.IO;
using System.Threading.Tasks;
using Unreal.Core.Codecs;
using Unreal.Core.Codecs.Rsa;
using Unreal.Core.IO;

namespace Unreal.Core.Submodules
{
    public static class FileCodecProcessing
    {
        public static void CodecApply(params string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Please select a file to process");
                return;
            }

            if (string.IsNullOrEmpty(args[0]))
            {
                Console.WriteLine("File name is null of empty!");
                return;
            }

            string fileName = args[0];
            FileInfo fileInfo = new FileInfo(fileName);
            CodecApplyToFile(fileInfo);
        }

        public static void CodecApplyToFile(FileInfo fileInfo)
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine($"Input file: \"{fileInfo.Name}\"");
            Console.WriteLine($"Output file (given or generated): \"{fileInfo.Name}\"");
            Console.WriteLine();
            bool wasNotEncoded;
            byte[] buffer;
            if (!fileInfo.Exists)
            {
                return;
            }

            using (BufferedReader reader = new BufferedReader(fileInfo))
            {
                buffer = reader.Read();
                wasNotEncoded = reader.Codec is Lineage2NullCodec;
            }

            Console.WriteLine();
            using (BufferedWriter writer = new BufferedWriter(fileInfo,
                wasNotEncoded ? CodecFactory.PeekFromExtension(fileInfo) : new Lineage2NullCodec()))
            {
                writer.Write(buffer);
            }
        }

        public static void ChangeCodecToCompatible(FileInfo fileInfo)
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine($"Input file: \"{fileInfo.Name}\"");
            Console.WriteLine($"Output file (given or generated): \"{fileInfo.Name}\"");
            Console.WriteLine();
            byte[] buffer;
            if (!fileInfo.Exists)
            {
                return;
            }

            using (BufferedReader reader = new BufferedReader(fileInfo, new Lineage2Ver413ReadOnly()))
            {
                buffer = reader.Read();
            }

            Console.WriteLine();
            using (BufferedWriter writer = new BufferedWriter(fileInfo, CodecFactory.PeekFromExtension(fileInfo)))
            {
                writer.Write(buffer);
            }
        }

        public static void SystemPatcher(string folderPath, bool patchExe)
        {
            ShowHeader();
            DirectoryInfo currentDir = new DirectoryInfo(folderPath);
            if (!PatcherProcedure(currentDir, patchExe))
            {
                Console.WriteLine("Hmmm, looks like there have been some problems with patcher operation.");
            }
        }

        public static void SystemPatcher(bool patchExe)
        {
            ShowHeader();
            DirectoryInfo currentDir = new DirectoryInfo(Directory.GetCurrentDirectory());
            if (!PatcherProcedure(currentDir, patchExe))
            {
                Console.WriteLine("Hmmm, looks like there have been some problems with patcher operation.");
            }
        }

        public static bool PatcherProcedure(DirectoryInfo currentDir, bool patchExe)
        {
            DirectoryInfo backUpDir = new DirectoryInfo(Path.Combine(currentDir.FullName, "backup"));
            if (backUpDir.Exists)
            {
                Console.WriteLine("Cannot create backup dir (or it already exists).");
                return false;
            }

            bool datProcessed = ProcessDats(currentDir, backUpDir);
            bool iniProcessed = ProcessInis(currentDir, backUpDir);
            var exeProcessed = false;
            if (patchExe)
            {
                exeProcessed = PatchL2Exe(currentDir, backUpDir);
            }

            Console.WriteLine();
            Console.WriteLine($" *.dat patched {datProcessed}");
            Console.WriteLine($" *.ini patched {iniProcessed}");
            Console.WriteLine($"l2.exe patched {exeProcessed}");
            Console.WriteLine();
            ShowBanner();
            return datProcessed && iniProcessed && (!patchExe || exeProcessed);
        }

        private static bool PatchL2Exe(DirectoryInfo currentDir, DirectoryInfo backUpDir)
        {
            try
            {
                FileInfo l2Exe = new FileInfo(Path.Combine(currentDir.FullName, "l2.exe"));
                if (l2Exe.Exists)
                {
                    l2Exe.CopyTo(Path.Combine(backUpDir.FullName, l2Exe.Name), true);
                    byte[] l2Exebytes = File.ReadAllBytes(l2Exe.FullName);
                    bool modulusPatched = BinaryPatches.Replace(l2Exebytes, Lineage2Ver41x.Modulus413, Lineage2Ver41x.ModulusL2Encdec);
                    bool privatePatched = BinaryPatches.Replace(l2Exebytes, Lineage2Ver41x.PrivateExponent413, Lineage2Ver41x.PrivateExponentL2Encdec);
                    if (modulusPatched || privatePatched)
                    {
                        File.WriteAllBytes(l2Exe.FullName, l2Exebytes);
                    }

                    return modulusPatched && privatePatched;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        private static bool ProcessInis(DirectoryInfo currentDir, DirectoryInfo backUpDir)
        {
            try
            {
                FileInfo userIni = new FileInfo(Path.Combine(currentDir.FullName, "User.ini"));
                FileInfo l2Ini = new FileInfo(Path.Combine(currentDir.FullName, "L2.ini"));
                userIni.CopyTo(Path.Combine(backUpDir.FullName, userIni.Name), true);
                ChangeCodecToCompatible(userIni);
                l2Ini.CopyTo(Path.Combine(backUpDir.FullName, l2Ini.Name), true);
                ChangeCodecToCompatible(l2Ini);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static bool ProcessDats(DirectoryInfo currentDir, DirectoryInfo backUpDir)
        {
            try
            {
                FileInfo[] datFiles = currentDir.GetFiles("*.dat");
                if (!backUpDir.Exists)
                {
                    backUpDir.Create();
                }

                //Parallel.ForEach(datFiles, fileInfo =>
                //{
                //    fileInfo.CopyTo(Path.Combine(backUpDir.FullName, fileInfo.Name), true);
                //    ChangeCodecToCompatible(fileInfo);
                //});
                //foreach (var fileInfo in datFiles)
                //{
                //    fileInfo.CopyTo(Path.Combine(backUpDir.FullName, fileInfo.Name), true);
                //    ChangeCodecToCompatible(fileInfo);
                //}

                return true;
            }
            catch
            {
                return false;
            }
        }

        private static void ShowHeader()
        {
            Console.WriteLine(@"L2 Patcher.NET by edKith.");
        }

        private static void ShowBanner()
        {
            Console.WriteLine(@"----------------------------- NOTICE ------------------------------");
            Console.WriteLine(@"---------------- l2.exe is NOT patched by default! ----------------");
            Console.WriteLine(@"--------------- Use -n key to force patching l2.exe ---------------");
            Console.WriteLine();
            Console.WriteLine(@"Press any key to exit.");
            Console.ReadKey();
        }
    }
}