﻿using System.Text;

namespace Unreal.Core.Submodules
{
    public static class BinaryPatches
    {
        private static bool IsEmptyLocate(byte[] array, byte[] candidate)
        {
            return array == null
                || candidate == null
                || array.Length == 0
                || candidate.Length == 0
                || candidate.Length > array.Length;
        }

        public static int Locate(byte[] self, byte[] candidate)
        {
            if (IsEmptyLocate(self, candidate))
            {
                return -1;
            }

            for (var i = 0; i < self.Length; i++)
            {
                if (IsMatch(self, i, candidate))
                {
                    return i;
                }
            }

            return -1;
        }

        public static bool Replace(byte[] self, string candidateString, string replaceString)
        {
            byte[] candidate = Encoding.ASCII.GetBytes(candidateString);
            byte[] replace = Encoding.ASCII.GetBytes(replaceString);
            int start = Locate(self, candidate);
            if (start > 0)
            {
                for (var i = 0; i < replace.Length; i++)
                {
                    self[i + start] = replace[i];
                }

                return true;
            }

            return false;
        }

        public static bool Replace(byte[] self, byte[] candidate, byte[] replace)
        {
            int start = Locate(self, candidate);
            if (start > 0)
            {
                for (var i = 0; i < replace.Length; i++)
                {
                    self[i + start] = replace[i];
                }

                return true;
            }

            return false;
        }

        private static bool IsMatch(byte[] array, int position, byte[] candidate)
        {
            if (candidate.Length > array.Length - position)
            {
                return false;
            }

            for (var i = 0; i < candidate.Length; i++)
            {
                if (array[position + i] != candidate[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}