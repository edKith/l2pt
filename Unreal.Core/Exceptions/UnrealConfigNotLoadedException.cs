﻿using System;
using System.Runtime.Serialization;

namespace Unreal.Core.Exceptions
{
    public class UnrealConfigNotLoadedException : UnrealCoreException
    {
        public UnrealConfigNotLoadedException()
        {
        }

        public UnrealConfigNotLoadedException(string message) : base($"Configuration file isn't loaded: {message}")
        {
        }

        public UnrealConfigNotLoadedException(string message, Exception innerException) : base($"Configuration file isn't loaded: {message}", innerException)
        {
        }

        protected UnrealConfigNotLoadedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}