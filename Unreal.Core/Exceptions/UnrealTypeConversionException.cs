﻿using System;
using System.Runtime.Serialization;

namespace Unreal.Core.Exceptions
{
    public class UnrealTypeConversionException : UnrealCoreException
    {

        public Type TargetType { get; set; }

        public UnrealTypeConversionException(Type targetType)
        {
            TargetType = targetType;
        }

        public UnrealTypeConversionException(Type targetType, string message)
            : base($"Error while converting to {targetType} : {message}")
        {
            TargetType = targetType;
        }

        public UnrealTypeConversionException(Type targetType, string message, Exception innerException)
            : base($"Error while converting to {targetType} : {message}", innerException)
        {
            TargetType = targetType;
        }

        protected UnrealTypeConversionException(Type targetType, SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            TargetType = targetType;
        }
    }
}