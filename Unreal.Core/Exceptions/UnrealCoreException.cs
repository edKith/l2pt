﻿using System;
using System.Runtime.Serialization;

namespace Unreal.Core.Exceptions
{
    public class UnrealCoreException : Exception
    {
        public UnrealCoreException() : base()
        {
        }

        public UnrealCoreException(string message) : base(message)
        {
        }

        public UnrealCoreException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnrealCoreException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}