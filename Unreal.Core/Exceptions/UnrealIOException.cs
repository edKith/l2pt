﻿using System;
using System.Runtime.Serialization;

namespace Unreal.Core.Exceptions
{
    public class UnrealIOException : UnrealCoreException
    {
        public UnrealIOException() : base()
        {
        }

        public UnrealIOException(string message) : base(message)
        {
        }

        public UnrealIOException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnrealIOException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}