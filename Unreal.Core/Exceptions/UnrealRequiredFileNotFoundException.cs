﻿using System;
using System.Runtime.Serialization;

namespace Unreal.Core.Exceptions
{
    public class UnrealRequiredFileNotFoundException : UnrealCoreException
    {
        public UnrealRequiredFileNotFoundException()
        {
        }

        public UnrealRequiredFileNotFoundException(string message) : base($"Required file not found: {message}")
        {
        }

        public UnrealRequiredFileNotFoundException(string message, Exception innerException) : base($"Required file not found: {message}", innerException)
        {
        }

        protected UnrealRequiredFileNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}