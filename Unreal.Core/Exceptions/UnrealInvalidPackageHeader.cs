﻿using System;
using System.Runtime.Serialization;

namespace Unreal.Core.Exceptions
{
    public class UnrealInvalidPackageHeader : UnrealIOException
    {
        public UnrealInvalidPackageHeader() : base()
        {
        }

        public UnrealInvalidPackageHeader(string message) : base(message)
        {
        }

        public UnrealInvalidPackageHeader(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnrealInvalidPackageHeader(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}