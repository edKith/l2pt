﻿using System.Diagnostics;
using System.IO;
using Unreal.Core.Exceptions;
using Unreal.Core.Templates;

namespace Unreal.Core.IO
{
    public class UnrealFileSystem : Singleton<UnrealFileSystem>
    {
        public DirectoryInfo Directory;
        public FileInfo Executable;
        public FileInfo Config;
        public FileInfo UserConfig;

        public static void Initialize(string directoryPath)
        {
            Initialize(directoryPath, "L2", "exe");
        }

        public void Run()
        {
            if (this.Executable.Exists)
            {
                var startInfo = new ProcessStartInfo(this.Executable.FullName)
                {
                    FileName = this.Executable.FullName
                };
                try
                {
                    // Start the process with the info we specified.
                    // Call WaitForExit and then the using-statement will close.
                    using (Process exeProcess = Process.Start(startInfo))
                    {
                        exeProcess.WaitForExit();
                    }
                }
                catch
                {
                    // Log error.
                }
            }
        }

        public static void Initialize(string directoryPath, string executableName, string executableExt)
        {
            Instance.Directory = new DirectoryInfo(directoryPath);

            Instance.Executable = new FileInfo(Path.Combine(Instance.Directory.FullName, $"{executableName}.{executableExt}"));
            Instance.Config = new FileInfo(Path.Combine(Instance.Directory.FullName, $"{executableName}.ini"));
            Instance.UserConfig = new FileInfo(Path.Combine(Instance.Directory.FullName, "User.ini"));
            GuardExists(Instance.Directory, Instance.Executable, Instance.Config, Instance.UserConfig);
        }

        public static void GuardExists(params FileSystemInfo[] required)
        {
            foreach (FileSystemInfo fileSystemInfo in required)
            {
                if (!fileSystemInfo.Exists)
                {
                    throw new UnrealRequiredFileNotFoundException(fileSystemInfo.FullName);
                }
            }
        }
    }
}