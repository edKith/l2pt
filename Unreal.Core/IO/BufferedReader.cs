﻿using System;
using System.IO;
using System.Text;
using Unreal.Core.Codecs;
using Unreal.Core.Codecs.Rsa;
using Unreal.Core.IO.Interfaces;
using Unreal.Core.RSA.Exceptions;

namespace Unreal.Core.IO
{
    public class BufferedReader : IEncodedStream, IDisposable
    {
        private BinaryReader _innerStream;
        private ICodec codec;

        public FileInfo FileEntry { get; set; }

        public ICodec Codec
        {
            get => this.codec;
            set
            {
                this.codec = value;
                if (!string.IsNullOrEmpty(FileEntry?.Name))
                {
                    this.codec.Title = FileEntry.Name;
                }
            }

        }

        public string EncodeKeyBase => this.FileEntry.Name;

        public BufferedReader(FileInfo targetFile)
        {
            this.FileEntry = targetFile;
            this._innerStream = new BinaryReader(File.OpenRead(this.FileEntry.FullName), Encoding.UTF8);
            this.Codec = Lineage2NullCodec.Instance;
            PeekCodec(this._innerStream);
        }

        public BufferedReader(FileInfo targetFile, ICodec codec)
        {
            this.FileEntry = targetFile;
            this._innerStream = new BinaryReader(File.OpenRead(this.FileEntry.FullName), Encoding.UTF8);
            this.Codec = codec;
        }

        public byte[] Read()
        {
            long dataSize = this._innerStream.BaseStream.Length - this.Codec.DataOffset - (this.DetectTail() ? 20 : 0);
            this._innerStream.BaseStream.Position = this.Codec.DataOffset;
            byte[] data = this._innerStream.ReadBytes((int) dataSize);
            if (!(this.Codec is Lineage2NullCodec))
            {
                Console.WriteLine($"Header: {this.Codec.Header}");
                if (this.Codec is Lineage2Ver413)
                {
                    var testData = new byte[data.Length];
                    Array.Copy(data, testData, data.Length);
                    try
                    {
                        Console.WriteLine($"Action chosen: decode {CodecFactory.PeekNumber(this.Codec.Header)}");
                        Console.WriteLine($"Packed data size: {data.Length}");
                        Console.WriteLine();
                        byte[] decodedData = this.Codec.Decode(testData);
                        Console.WriteLine("Stream read");
                        return decodedData;
                    }
                    catch (RSAException ex)
                    {
                        Console.WriteLine();
                        Console.WriteLine("Original 413 detected, switch to original RSA key pair");
                        Console.WriteLine();
                        this.Codec = new Lineage2Ver413ReadOnly();
                        Console.WriteLine(
                            $"Action chosen: decode original {CodecFactory.PeekNumber(this.Codec.Header)}");
                        Console.WriteLine($"Packed data size: {data.Length}");
                        Console.WriteLine();
                        byte[] secondData = this.Codec.Decode(data);
                        Console.WriteLine("Stream read");
                        return secondData;
                    }
                }

                Console.WriteLine($"Action chosen: decode {CodecFactory.PeekNumber(this.Codec.Header)}");
                Console.WriteLine($"Packed data size: {data.Length}");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine($"Action chosen: pure reading");
                Console.WriteLine($"Pure data size: {data.Length}");
            }

            byte[] decodedsData = this.Codec.Decode(data);
            Console.WriteLine("Stream read");
            return decodedsData;

        }

        public Stream ReadFileToStream()
        {
            return new MemoryStream(this.Read());
        }

        private void PeekCodec(BinaryReader innerStream)
        {
            var headerBytes = new sbyte[28];
            for (var i = 0; i < 28; i++)
            {
                headerBytes[i] = innerStream.ReadSByte();
            }

            string header;
            unsafe
            {
                fixed (sbyte* pbArr = &headerBytes[0])
                {
                    header = new string(pbArr, 0, headerBytes.Length,
                        Encoding.Unicode);
                }
            }

            if (!header.Equals(Codec.Header))
            {
                Console.WriteLine($"Warning: Codec defined by user is different with file codec base!");
            }

            if (header.StartsWith("Lineage2Ver"))
            {
                int version = int.Parse(header.Substring(11));
                Codec = CodecFactory.PeekFromNumber(version);
                Codec.GenerateKey(this.EncodeKeyBase);
            }

            if (Codec is Lineage2NullCodec)
            {
                this._innerStream.BaseStream.Position = 0;
            }
        }

        private bool DetectTail()
        {
            var bytebuffer = new byte[20];
            var result = true;
            long position = this._innerStream.BaseStream.Position;
            this._innerStream.BaseStream.Position = (int) this._innerStream.BaseStream.Length - 20;
            this._innerStream.BaseStream.Read(bytebuffer, 0, 20);
            this._innerStream.BaseStream.Position = position;
            for (var i = 0; i < bytebuffer.Length; i++)
            {
                if (i < 4 || i > 4 && i < 8 || i > 9 && i < 12)
                {
                    result = result && bytebuffer[i] == 0x0;
                }

                if (i > 15)
                {
                    result = result && bytebuffer[i] == 0x0;
                }
            }

            return result;
        }

        public void Dispose()
        {
            this._innerStream?.Dispose();
        }
    }
}