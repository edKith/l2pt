using System;

namespace Unreal.Core.IO.Compression
{

    internal sealed class InfBlocks{
        private const int MANY=1440;

        // And'ing with mask[n] masks the lower n bits
        private static readonly int[] inflate_mask = {
                                                0x00000000, 0x00000001, 0x00000003, 0x00000007, 0x0000000f,
                                                0x0000001f, 0x0000003f, 0x0000007f, 0x000000ff, 0x000001ff,
                                                0x000003ff, 0x000007ff, 0x00000fff, 0x00001fff, 0x00003fff,
                                                0x00007fff, 0x0000ffff
                                            };

        // Table for deflate from PKZIP's appnote.txt.
        static readonly int[] border = { // Order of the bit length code lengths
                                  16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15
                              };

        private const int Z_OK=0;
        private const int Z_STREAM_END=1;
        private const int Z_NEED_DICT=2;
        private const int Z_ERRNO=-1;
        private const int Z_STREAM_ERROR=-2;
        private const int Z_DATA_ERROR=-3;
        private const int Z_MEM_ERROR=-4;
        private const int Z_BUF_ERROR=-5;
        private const int Z_VERSION_ERROR=-6;

        private const int TYPE=0;  // get type bits (3, including end bit)
        private const int LENS=1;  // get lengths for stored
        private const int STORED=2;// processing stored block
        private const int TABLE=3; // get table lengths
        private const int BTREE=4; // get bit lengths tree for a dynamic block
        private const int DTREE=5; // get length, distance trees for a dynamic block
        private const int CODES=6; // processing fixed or dynamic block
        private const int DRY=7;   // output remaining window bytes
        private const int DONE=8;  // finished last block, done
        private const int BAD=9;   // ot a data error--stuck here

        internal int mode;            // current inflate_block mode 

        internal int left;            // if STORED, bytes left to copy 

        internal int table;           // table lengths (14 bits) 
        internal int index;           // index into blens (or border) 
        internal int[] blens;         // bit lengths of codes 
        internal int[] bb=new int[1]; // bit length tree depth 
        internal int[] tb=new int[1]; // bit length decoding tree 

        internal InfCodes codes=new InfCodes();      // if CODES, current state 

        int last;            // true if this block is the last block 

        // mode independent information 
        internal int bitk;            // bits in bit buffer 
        internal int bitb;            // bit buffer 
        internal int[] hufts;         // single malloc for tree space 
        internal byte[] window;       // sliding window 
        internal int end;             // one byte after sliding window 
        internal int read;            // window read pointer 
        internal int write;           // window write pointer 
        internal Object checkfn;      // check function 
        internal long check;          // check on output 

        internal InfTree inftree=new InfTree();

        internal InfBlocks(ZStream z, Object checkfn, int w){
            this.hufts=new int[MANY*3];
            this.window=new byte[w];
            this.end=w;
            this.checkfn = checkfn;
            this.mode = TYPE;
            this.reset(z, null);
        }

        internal void reset(ZStream z, long[] c){
            if(c!=null) c[0]=this.check;
            if(this.mode==BTREE || this.mode==DTREE){
            }
            if(this.mode==CODES){
                this.codes.free(z);
            }
            this.mode=TYPE;
            this.bitk=0;
            this.bitb=0;
            this.read=this.write=0;

            if(this.checkfn != null)
                z.adler=this.check=z._adler.adler32(0L, null, 0, 0);
        }

        internal int proc(ZStream z, int r){
            int t;              // temporary storage
            int b;              // bit buffer
            int k;              // bits in bit buffer
            int p;              // input data pointer
            int n;              // bytes available there
            int q;              // output window write pointer
            int m; {              // bytes to end of window or read pointer

            // copy input/output information to locals (UPDATE macro restores)
     p=z.next_in_index;n=z.avail_in;b=this.bitb;k=this.bitk;} {
     q=this.write;m=(int)(q<this.read?this.read-q-1:this.end-q);}

            // process input based on current state
            while(true){
                switch (this.mode){
                    case TYPE:

                        while(k<(3)){
                            if(n!=0){
                                r=Z_OK;
                            }
                            else{
                                this.bitb=b; this.bitk=k; 
                                z.avail_in=n;
                                z.total_in+=p-z.next_in_index;z.next_in_index=p;
                                this.write=q;
                                return this.inflate_flush(z,r);
                            };
                            n--;
                            b|=(z.next_in[p++]&0xff)<<k;
                            k+=8;
                        }
                        t = (int)(b & 7);
                        this.last = t & 1;

                    switch (t >> 1){
                        case 0: {                         // stored 
           b>>=(3);k-=(3);}
                            t = k & 7; {                    // go to byte boundary

           b>>=(t);k-=(t);}
                            this.mode = LENS;                  // get length of stored block
                            break;
                        case 1: {                         // fixed
                            int[] bl=new int[1];
                            int[] bd=new int[1];
                            int[][] tl=new int[1][];
                            int[][] td=new int[1][];

                            InfTree.inflate_trees_fixed(bl, bd, tl, td, z);
                            this.codes.init(bl[0], bd[0], tl[0], 0, td[0], 0, z);
                        } {

           b>>=(3);k-=(3);}

                            this.mode = CODES;
                            break;
                        case 2: {                         // dynamic

           b>>=(3);k-=(3);}

                            this.mode = TABLE;
                            break;
                        case 3: {                         // illegal

           b>>=(3);k-=(3);}
                            this.mode = BAD;
                            z.msg = "invalid block type";
                            r = Z_DATA_ERROR;

                            this.bitb=b; this.bitk=k; 
                            z.avail_in=n;z.total_in+=p-z.next_in_index;z.next_in_index=p;
                            this.write=q;
                            return this.inflate_flush(z,r);
                    }
                        break;
                    case LENS:

                        while(k<(32)){
                            if(n!=0){
                                r=Z_OK;
                            }
                            else{
                                this.bitb=b; this.bitk=k; 
                                z.avail_in=n;
                                z.total_in+=p-z.next_in_index;z.next_in_index=p;
                                this.write=q;
                                return this.inflate_flush(z,r);
                            };
                            n--;
                            b|=(z.next_in[p++]&0xff)<<k;
                            k+=8;
                        }

                        if ((((~b) >> 16) & 0xffff) != (b & 0xffff)){
                            this.mode = BAD;
                            z.msg = "invalid stored block lengths";
                            r = Z_DATA_ERROR;

                            this.bitb=b; this.bitk=k; 
                            z.avail_in=n;z.total_in+=p-z.next_in_index;z.next_in_index=p;
                            this.write=q;
                            return this.inflate_flush(z,r);
                        }
                        this.left = (b & 0xffff);
                        b = k = 0;                       // dump bits
                        this.mode = this.left!=0 ? STORED : (this.last!=0 ? DRY : TYPE);
                        break;
                    case STORED:
                        if (n == 0){
                            this.bitb=b; this.bitk=k; 
                            z.avail_in=n;z.total_in+=p-z.next_in_index;z.next_in_index=p;
                            this.write=q;
                            return this.inflate_flush(z,r);
                        }

                        if(m==0){
                            if(q==this.end&&this.read!=0){
                                q=0; m=(int)(q<this.read?this.read-q-1:this.end-q);
                            }
                            if(m==0){
                                this.write=q; 
                                r=this.inflate_flush(z,r);
                                q=this.write;m=(int)(q<this.read?this.read-q-1:this.end-q);
                                if(q==this.end&&this.read!=0){
                                    q=0; m=(int)(q<this.read?this.read-q-1:this.end-q);
                                }
                                if(m==0){
                                    this.bitb=b; this.bitk=k; 
                                    z.avail_in=n;z.total_in+=p-z.next_in_index;z.next_in_index=p;
                                    this.write=q;
                                    return this.inflate_flush(z,r);
                                }
                            }
                        }
                        r=Z_OK;

                        t = this.left;
                        if(t>n) t = n;
                        if(t>m) t = m;
                        System.Array.Copy(z.next_in, p, this.window, q, t);
                        p += t;  n -= t;
                        q += t;  m -= t;
                        if ((this.left -= t) != 0)
                            break;
                        this.mode = this.last!=0 ? DRY : TYPE;
                        break;
                    case TABLE:

                        while(k<(14)){
                            if(n!=0){
                                r=Z_OK;
                            }
                            else{
                                this.bitb=b; this.bitk=k; 
                                z.avail_in=n;
                                z.total_in+=p-z.next_in_index;z.next_in_index=p;
                                this.write=q;
                                return this.inflate_flush(z,r);
                            };
                            n--;
                            b|=(z.next_in[p++]&0xff)<<k;
                            k+=8;
                        }

                        this.table = t = (b & 0x3fff);
                        if ((t & 0x1f) > 29 || ((t >> 5) & 0x1f) > 29) {
                            this.mode = BAD;
                            z.msg = "too many length or distance symbols";
                            r = Z_DATA_ERROR;

                            this.bitb=b; this.bitk=k; 
                            z.avail_in=n;z.total_in+=p-z.next_in_index;z.next_in_index=p;
                            this.write=q;
                            return this.inflate_flush(z,r);
                        }
                        t = 258 + (t & 0x1f) + ((t >> 5) & 0x1f);
                        if(this.blens==null || this.blens.Length<t){
                            this.blens=new int[t];
                        }
                        else{
                            for(int i=0; i<t; i++){this.blens[i]=0;}
                        } {

	 b>>=(14);k-=(14);}

                        this.index = 0;
                        this.mode = BTREE;
                        goto case BTREE;
                    case BTREE:
                        while (this.index < 4 + (this.table >> 10)){
                            while(k<(3)){
                                if(n!=0){
                                    r=Z_OK;
                                }
                                else{
                                    this.bitb=b; this.bitk=k; 
                                    z.avail_in=n;
                                    z.total_in+=p-z.next_in_index;z.next_in_index=p;
                                    this.write=q;
                                    return this.inflate_flush(z,r);
                                };
                                n--;
                                b|=(z.next_in[p++]&0xff)<<k;
                                k+=8;
                            }

                            this.blens[border[this.index++]] = b&7; {

	   b>>=(3);k-=(3);}
                        }

                        while(this.index < 19){
                            this.blens[border[this.index++]] = 0;
                        }

                        this.bb[0] = 7;
                        t = this.inftree.inflate_trees_bits(this.blens, this.bb, this.tb, this.hufts, z);
                        if (t != Z_OK){
                            r = t;
                            if (r == Z_DATA_ERROR){
                                this.blens=null;
                                this.mode = BAD;
                            }

                            this.bitb=b; this.bitk=k; 
                            z.avail_in=n;z.total_in+=p-z.next_in_index;z.next_in_index=p;
                            this.write=q;
                            return this.inflate_flush(z,r);
                        }

                        this.index = 0;
                        this.mode = DTREE;
                        goto case DTREE;
                    case DTREE:
                        while (true){
                            t = this.table;
                            if(!(this.index < 258 + (t & 0x1f) + ((t >> 5) & 0x1f))){
                                break;
                            }

                            int i, j, c;

                            t = this.bb[0];

                            while(k<(t)){
                                if(n!=0){
                                    r=Z_OK;
                                }
                                else{
                                    this.bitb=b; this.bitk=k; 
                                    z.avail_in=n;
                                    z.total_in+=p-z.next_in_index;z.next_in_index=p;
                                    this.write=q;
                                    return this.inflate_flush(z,r);
                                };
                                n--;
                                b|=(z.next_in[p++]&0xff)<<k;
                                k+=8;
                            }

                            if(this.tb[0]==-1){
                                //System.err.println("null...");
                            }

                            t=this.hufts[(this.tb[0]+(b&inflate_mask[t]))*3+1];
                            c=this.hufts[(this.tb[0]+(b&inflate_mask[t]))*3+2];

                            if (c < 16){
                                b>>=(t);k-=(t);
                                this.blens[this.index++] = c;
                            }
                            else { // c == 16..18
                                i = c == 18 ? 7 : c - 14;
                                j = c == 18 ? 11 : 3;

                                while(k<(t+i)){
                                    if(n!=0){
                                        r=Z_OK;
                                    }
                                    else{
                                        this.bitb=b; this.bitk=k; 
                                        z.avail_in=n;
                                        z.total_in+=p-z.next_in_index;z.next_in_index=p;
                                        this.write=q;
                                        return this.inflate_flush(z,r);
                                    };
                                    n--;
                                    b|=(z.next_in[p++]&0xff)<<k;
                                    k+=8;
                                }

                                b>>=(t);k-=(t);

                                j += (b & inflate_mask[i]);

                                b>>=(i);k-=(i);

                                i = this.index;
                                t = this.table;
                                if (i + j > 258 + (t & 0x1f) + ((t >> 5) & 0x1f) ||
                                    (c == 16 && i < 1)){
                                    this.blens=null;
                                    this.mode = BAD;
                                    z.msg = "invalid bit length repeat";
                                    r = Z_DATA_ERROR;

                                    this.bitb=b; this.bitk=k; 
                                    z.avail_in=n;z.total_in+=p-z.next_in_index;z.next_in_index=p;
                                    this.write=q;
                                    return this.inflate_flush(z,r);
                                }

                                c = c == 16 ? this.blens[i-1] : 0;
                                do{
                                    this.blens[i++] = c;
                                }
                                while (--j!=0);
                                this.index = i;
                            }
                        }

                        this.tb[0]=-1; {
                        int[] bl=new int[1];
                        int[] bd=new int[1];
                        int[] tl=new int[1];
                        int[] td=new int[1];
                        bl[0] = 9;         // must be <= 9 for lookahead assumptions
                        bd[0] = 6;         // must be <= 9 for lookahead assumptions

                        t = this.table;
                        t = this.inftree.inflate_trees_dynamic(257 + (t & 0x1f), 
                            1 + ((t >> 5) & 0x1f),
                            this.blens, bl, bd, tl, td, this.hufts, z);

                        if (t != Z_OK){
                            if (t == Z_DATA_ERROR){
                                this.blens=null;
                                this.mode = BAD;
                            }
                            r = t;

                            this.bitb=b; this.bitk=k; 
                            z.avail_in=n;z.total_in+=p-z.next_in_index;z.next_in_index=p;
                            this.write=q;
                            return this.inflate_flush(z,r);
                        }
                        this.codes.init(bl[0], bd[0], this.hufts, tl[0], this.hufts, td[0], z);
                    }
                        this.mode = CODES;
                        goto case CODES;
                    case CODES:
                        this.bitb=b; this.bitk=k;
                        z.avail_in=n; z.total_in+=p-z.next_in_index;z.next_in_index=p;
                        this.write=q;

                        if ((r = this.codes.proc(this, z, r)) != Z_STREAM_END){
                            return this.inflate_flush(z, r);
                        }
                        r = Z_OK;
                        this.codes.free(z);

                        p=z.next_in_index; n=z.avail_in;b=this.bitb;k=this.bitk;
                        q=this.write;m=(int)(q<this.read?this.read-q-1:this.end-q);

                        if (this.last==0){
                            this.mode = TYPE;
                            break;
                        }
                        this.mode = DRY;
                        goto case DRY;
                    case DRY:
                        this.write=q; 
                        r=this.inflate_flush(z, r); 
                        q=this.write; m=(int)(q<this.read?this.read-q-1:this.end-q);
                        if (this.read != this.write){
                            this.bitb=b; this.bitk=k; 
                            z.avail_in=n;z.total_in+=p-z.next_in_index;z.next_in_index=p;
                            this.write=q;
                            return this.inflate_flush(z, r);
                        }
                        this.mode = DONE;
                        goto case DONE;
                    case DONE:
                        r = Z_STREAM_END;

                        this.bitb=b; this.bitk=k; 
                        z.avail_in=n;z.total_in+=p-z.next_in_index;z.next_in_index=p;
                        this.write=q;
                        return this.inflate_flush(z, r);
                    case BAD:
                        r = Z_DATA_ERROR;

                        this.bitb=b; this.bitk=k; 
                        z.avail_in=n;z.total_in+=p-z.next_in_index;z.next_in_index=p;
                        this.write=q;
                        return this.inflate_flush(z, r);

                    default:
                        r = Z_STREAM_ERROR;

                        this.bitb=b; this.bitk=k; 
                        z.avail_in=n;z.total_in+=p-z.next_in_index;z.next_in_index=p;
                        this.write=q;
                        return this.inflate_flush(z, r);
                }
            }
        }

        internal void free(ZStream z){
            this.reset(z, null);
            this.window=null;
            this.hufts=null;
            //ZFREE(z, s);
        }

        internal void set_dictionary(byte[] d, int start, int n){
            System.Array.Copy(d, start, this.window, 0, n);
            this.read = this.write = n;
        }

        // Returns true if inflate is currently at the end of a block generated
        // by Z_SYNC_FLUSH or Z_FULL_FLUSH. 
        internal int sync_point(){
            return this.mode == LENS ? 1 : 0;
        }

        // copy as much as possible from the sliding window to the output area
        internal int inflate_flush(ZStream z, int r){
            int n;
            int p;
            int q;

            // local copies of source and destination pointers
            p = z.next_out_index;
            q = this.read;

            // compute number of bytes to copy as far as end of window
            n = (int)((q <= this.write ? this.write : this.end) - q);
            if (n > z.avail_out) n = z.avail_out;
            if (n!=0 && r == Z_BUF_ERROR) r = Z_OK;

            // update counters
            z.avail_out -= n;
            z.total_out += n;

            // update check information
            if(this.checkfn != null)
                z.adler=this.check=z._adler.adler32(this.check, this.window, q, n);

            // copy as far as end of window
            System.Array.Copy(this.window, q, z.next_out, p, n);
            p += n;
            q += n;

            // see if more to copy at beginning of window
            if (q == this.end){
                // wrap pointers
                q = 0;
                if (this.write == this.end)
                    this.write = 0;

                // compute bytes to copy
                n = this.write - q;
                if (n > z.avail_out) n = z.avail_out;
                if (n!=0 && r == Z_BUF_ERROR) r = Z_OK;

                // update counters
                z.avail_out -= n;
                z.total_out += n;

                // update check information
                if(this.checkfn != null)
                    z.adler=this.check=z._adler.adler32(this.check, this.window, q, n);

                // copy
                System.Array.Copy(this.window, q, z.next_out, p, n);
                p += n;
                q += n;
            }

            // update pointers
            z.next_out_index = p;
            this.read = q;

            // done
            return r;
        }
    }
}