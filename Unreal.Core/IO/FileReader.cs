﻿using System;
using System.IO;
using System.Security;
using System.Text;
using Unreal.Core.Codecs;
using Unreal.Core.Codecs.Rsa;
using Unreal.Core.Exceptions;
using Unreal.Core.Extensions;
using Unreal.Core.IO.Interfaces;
using Unreal.Core.Objects;

namespace Unreal.Core.IO
{
    public class FileReader : IEncodedReader, IDisposable
    {
        public FileInfo FileEntry { get; set; }

        private const int MaxCharBytesSize = 128;
        private Stream m_stream;
        private byte[] m_buffer;
        private Decoder m_decoder;
        private byte[] m_charBytes;
        private char[] m_singleChar;
        private char[] m_charBuffer;
        private int m_maxCharsSize;
        private readonly bool m_2BytesPerChar;
        private readonly bool m_leaveOpen;
        public ICodec Codec { get; set; } = new Lineage2NullCodec();

        public string EncodeKeyBase => this.FileEntry.Name;

        public FileReader(FileInfo fileSystemInfo)
            : this(fileSystemInfo, new UTF8Encoding(), false)
        {
        }

        public FileReader(FileInfo fileSystemInfo, Encoding encoding)
            : this(fileSystemInfo, encoding, false)
        {
        }

        public FileReader(FileInfo fileSystemInfo, Encoding encoding, bool leaveOpen)
        {
            if (fileSystemInfo == null)
            {
                throw new ArgumentNullException(nameof(fileSystemInfo));
            }

            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }

            if (!fileSystemInfo.Exists)
            {
                throw new ArgumentException("Argument_FileNotExist");
            }

            this.FileEntry = fileSystemInfo;
            FileStream packageStream = fileSystemInfo.OpenRead();
            if (!packageStream.CanRead)
            {
                throw new ArgumentException("Argument_StreamNotReadable");
            }

            this.m_stream = packageStream;
            CodecFactory.PeekCodec(this);
            if (Codec is Lineage2Ver41x)
            {
                packageStream.Dispose();
                this.m_stream = new BufferedReader(fileSystemInfo, Codec).ReadFileToStream();
                Codec = Lineage2NullCodec.Instance;
            }
            this.m_decoder = encoding.GetDecoder();
            this.m_maxCharsSize = encoding.GetMaxCharCount(128);
            int length = encoding.GetMaxByteCount(1);
            if (length < 16)
            {
                length = 16;
            }

            this.m_buffer = new byte[length];
            this.m_2BytesPerChar = encoding is UnicodeEncoding;
            this.m_leaveOpen = leaveOpen;
            this.InitPosition();
        }
        
        private void InitPosition()
        {
            this.StreamPosition = 0;
        }

        /// <summary>Exposes access to the underlying stream position of the <see cref="T:Unreal.Core.FileIO" />.</summary>
        /// <returns>The underlying stream position associated with the <see langword="FileIO" />.</returns>
        public virtual long StreamPosition
        {
            get => this.m_stream.Position;

            set => this.m_stream.Position = value + this.Codec.DataOffset;
        }


        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Stream stream = this.m_stream;
                this.m_stream = null;
                if (stream != null && !this.m_leaveOpen)
                {
                    stream.Close();
                }
            }

            this.m_stream = null;
            this.m_buffer = null;
            this.m_decoder = null;
            this.m_charBytes = null;
            this.m_singleChar = null;
            this.m_charBuffer = null;
        }

        /// <summary>Releases all resources used by the current instance of the <see cref="T:System.IO.BinaryReader" /> class.</summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>Closes the current reader and the underlying stream.</summary>
        public virtual void Close()
        {
            this.Dispose(true);
        }

        /// <summary>Returns the next available character and does not advance the byte or character position.</summary>
        /// <returns>
        ///     The next available character, or -1 if no more characters are available or the stream does not support
        ///     seeking.
        /// </returns>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ArgumentException">
        ///     The current character cannot be decoded into the internal character buffer
        ///     by using the <see cref="T:System.Text.Encoding" /> selected for the stream.
        /// </exception>
        public virtual int PeekChar()
        {
            if (this.m_stream == null)
            {
                throw new UnrealIOException("FileNotOpen");
            }

            if (!this.m_stream.CanSeek)
            {
                return -1;
            }

            long position = this.m_stream.Position;
            int num = this.Read();
            this.m_stream.Position = position;
            return num;
        }

        /// <summary>
        ///     Reads characters from the underlying stream and advances the current position of the stream in accordance with
        ///     the <see langword="Encoding" /> used and the specific character being read from the stream.
        /// </summary>
        /// <returns>The next character from the input stream, or -1 if no characters are currently available.</returns>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        public virtual int Read()
        {
            if (this.m_stream == null)
            {
                throw new UnrealIOException("File not open");
            }

            return this.InternalReadOneChar();
        }


        public virtual byte[] ReadAllBytes()
        {
            long position = this.m_stream.Position;
            this.StreamPosition = 0;
            int tailLength = this.DetectTail() ? 20 : 0;
            var dataLength = (int) (this.m_stream.Length - this.StreamPosition - tailLength);
            byte[] retVal = this.ReadBytes(dataLength);
            this.m_stream.Position = position;
            return retVal;
        }

        protected void FillBuffer(int numBytes)
        {
            if (this.m_buffer != null && (numBytes < 0 || numBytes > this.m_buffer.Length))
            {
                throw new UnrealIOException($"{nameof(FileReader)} - {nameof(numBytes)} out of range",new ArgumentOutOfRangeException(nameof(numBytes)));
            }

            var offset = 0;
            if (numBytes == 1)
            {
                int num = this.m_stream.ReadByte();
                if (num == -1)
                {
                    throw new UnrealIOException("End of stream", new EndOfStreamException());
                }

                this.m_buffer[0] = (byte) num;
            }
            else
            {
                do
                {
                    int num = this.m_stream.Read(this.m_buffer, offset, numBytes - offset);
                    if (num == 0)
                    {
                        throw new UnrealIOException("End of stream", new EndOfStreamException());
                    }

                    offset += num;
                } while (offset < numBytes);
            }

            for (var i = 0; i < numBytes; i++)
            {
                this.m_buffer[i] = this.Codec.Decode(this.m_buffer[i]);
            }
        }

        public virtual byte ReadByte()
        {
            if (this.m_stream == null)
            {
                throw new UnrealIOException("File not open");
            }

            int num = this.m_stream.ReadByte();
            if (num == -1)
            {
                throw new UnrealIOException("End of stream", new EndOfStreamException());
            }

            return this.Codec.Decode((byte) num);
        }

        public virtual sbyte ReadSByte()
        {
            this.FillBuffer(1);
            return (sbyte) this.m_buffer[0];
        }

        public virtual sbyte[] ReadSBytes(int count)
        {
            byte[] buffer = this.ReadBytes(count);
            var sbytes = new sbyte[buffer.Length];
            for (var i = 0; i < buffer.Length; i++)
            {
                sbytes[i] = (sbyte) buffer[i];
            }

            return sbytes;
        }

        public virtual byte[] PeekBytes(int count)
        {
            long position = this.m_stream.Position;
            byte[] retVal = this.ReadBytes(count);
            this.m_stream.Position = position;

            return retVal;
        }

        public virtual sbyte[] PeekSBytes(int count)
        {
            long position = this.m_stream.Position;
            sbyte[] retVal = this.ReadSBytes(count);
            this.m_stream.Position = position;

            return retVal;
        }

        public virtual byte[] ReadBytes(int count)
        {
            if (count < 0)
            {
                throw new UnrealIOException("Need non negative number", new ArgumentOutOfRangeException(nameof(count), "ArgumentOutOfRange_NeedNonNegNum"));
            }

            if (this.m_stream == null)
            {
                throw new UnrealIOException("File not open");
            }

            if (count == 0)
            {
                return new byte[0];
            }

            var buffer = new byte[count];
            var length = 0;
            do
            {
                int num = this.m_stream.Read(buffer, length, count);
                if (num != 0)
                {
                    length += num;
                    count -= num;
                }
                else
                {
                    break;
                }
            } while (count > 0);

            if (length != buffer.Length)
            {
                var numArray = new byte[length];
                Buffer.BlockCopy(buffer, 0, numArray, 0, length);
                buffer = numArray;
            }

            return this.Codec.Decode(buffer);
        }

        /// <summary>
        ///     Reads the next character from the current stream and advances the current position of the stream in accordance
        ///     with the <see langword="Encoding" /> used and the specific character being read from the stream.
        /// </summary>
        /// <returns>A character read from the current stream.</returns>
        /// <exception cref="T:System.IO.EndOfStreamException">The end of the stream is reached. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ArgumentException">A surrogate character was read. </exception>
        public virtual char ReadChar()
        {
            int num = this.Read();
            if (num == -1)
            {
                throw new UnrealIOException("End of stream", new EndOfStreamException());
            }

            return (char) num;
        }

        /// <summary>
        ///     Reads a 2-byte signed integer from the current stream and advances the current position of the stream by two
        ///     bytes.
        /// </summary>
        /// <returns>A 2-byte signed integer read from the current stream.</returns>
        /// <exception cref="T:System.IO.EndOfStreamException">The end of the stream is reached. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        public virtual short ReadInt16()
        {
            this.FillBuffer(2);
            return (short) (this.m_buffer[0] | (this.m_buffer[1] << 8));
        }

        /// <summary>
        ///     Reads a 2-byte unsigned integer from the current stream using little-endian encoding and advances the position
        ///     of the stream by two bytes.
        /// </summary>
        /// <returns>A 2-byte unsigned integer read from this stream.</returns>
        /// <exception cref="T:System.IO.EndOfStreamException">The end of the stream is reached. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        [CLSCompliant(false)]
        public virtual ushort ReadUInt16()
        {
            this.FillBuffer(2);
            return (ushort) (this.m_buffer[0] | ((uint) this.m_buffer[1] << 8));
        }

        /// <summary>
        ///     Reads a 4-byte signed integer from the current stream and advances the current position of the stream by four
        ///     bytes.
        /// </summary>
        /// <returns>A 4-byte signed integer read from the current stream.</returns>
        /// <exception cref="T:System.IO.EndOfStreamException">The end of the stream is reached. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        public virtual int ReadInt32()
        {
            this.FillBuffer(4);
            return this.m_buffer[0] | (this.m_buffer[1] << 8) | (this.m_buffer[2] << 16) | (this.m_buffer[3] << 24);
        }

        /// <summary>Reads a 4-byte unsigned integer from the current stream and advances the position of the stream by four bytes.</summary>
        /// <returns>A 4-byte unsigned integer read from this stream.</returns>
        /// <exception cref="T:System.IO.EndOfStreamException">The end of the stream is reached. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        [CLSCompliant(false)]
        public virtual uint ReadUInt32()
        {
            this.FillBuffer(4);
            return (uint) (this.m_buffer[0] | (this.m_buffer[1] << 8) | (this.m_buffer[2] << 16) | (this.m_buffer[3] << 24));
        }

        /// <summary>
        ///     Reads an 8-byte signed integer from the current stream and advances the current position of the stream by
        ///     eight bytes.
        /// </summary>
        /// <returns>An 8-byte signed integer read from the current stream.</returns>
        /// <exception cref="T:System.IO.EndOfStreamException">The end of the stream is reached. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        public virtual long ReadInt64()
        {
            this.FillBuffer(8);
            return ((long) (uint) (this.m_buffer[4] | (this.m_buffer[5] << 8) | (this.m_buffer[6] << 16) | (this.m_buffer[7] << 24)) << 32) | (uint) (this.m_buffer[0] | (this.m_buffer[1] << 8) | (this.m_buffer[2] << 16) | (this.m_buffer[3] << 24));
        }

        /// <summary>
        ///     Reads an 8-byte unsigned integer from the current stream and advances the position of the stream by eight
        ///     bytes.
        /// </summary>
        /// <returns>An 8-byte unsigned integer read from this stream.</returns>
        /// <exception cref="T:System.IO.EndOfStreamException">The end of the stream is reached. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        [CLSCompliant(false)]
        public virtual ulong ReadUInt64()
        {
            this.FillBuffer(8);
            return ((ulong) (uint) (this.m_buffer[4] | (this.m_buffer[5] << 8) | (this.m_buffer[6] << 16) | (this.m_buffer[7] << 24)) << 32) | (uint) (this.m_buffer[0] | (this.m_buffer[1] << 8) | (this.m_buffer[2] << 16) | (this.m_buffer[3] << 24));
        }

        /// <summary>
        ///     Reads a 4-byte floating point value from the current stream and advances the current position of the stream by
        ///     four bytes.
        /// </summary>
        /// <returns>A 4-byte floating point value read from the current stream.</returns>
        /// <exception cref="T:System.IO.EndOfStreamException">The end of the stream is reached. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        [SecuritySafeCritical]
        public virtual unsafe float ReadSingle()
        {
            this.FillBuffer(4);
            var semiresult = (uint) (this.m_buffer[0] | (this.m_buffer[1] << 8) |
                (this.m_buffer[2] << 16) | (this.m_buffer[3] << 24));
            return *(float*) &semiresult;
        }

        /// <summary>
        ///     Reads an 8-byte floating point value from the current stream and advances the current position of the stream
        ///     by eight bytes.
        /// </summary>
        /// <returns>An 8-byte floating point value read from the current stream.</returns>
        /// <exception cref="T:System.IO.EndOfStreamException">The end of the stream is reached. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        [SecuritySafeCritical]
        public virtual unsafe double ReadDouble()
        {
            this.FillBuffer(8);
            ulong semiresult = ((ulong) (uint) (this.m_buffer[4] | (this.m_buffer[5] << 8) | (this.m_buffer[6] << 16) | (this.m_buffer[7] << 24)) << 32) | (uint) (this.m_buffer[0] | (this.m_buffer[1] << 8) | (this.m_buffer[2] << 16) | (this.m_buffer[3] << 24));
            return *(double*) &semiresult;
        }

        /// <summary>
        ///     Reads a decimal value from the current stream and advances the current position of the stream by sixteen
        ///     bytes.
        /// </summary>
        /// <returns>A decimal value read from the current stream.</returns>
        /// <exception cref="T:System.IO.EndOfStreamException">The end of the stream is reached. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        public virtual decimal ReadDecimal()
        {
            this.FillBuffer(16);
            try
            {
                var bits = new int[4];
                for (var i = 0; i <= 15; i += 4)
                {
                    //convert every 4 bytes into an int32
                    bits[i / 4] = BitConverter.ToInt32(this.m_buffer, i);
                }

                //Use the decimal's new constructor to
                //create an instance of decimal
                return new decimal(bits);
            }
            catch (ArgumentException ex)
            {
                throw new UnrealIOException("Arg_DecBitCtor", ex);
            }
        }

        /// <summary>
        ///     Reads a string from the current stream. The string is prefixed with the length, encoded as an UnrealCompactInteger.
        /// </summary>
        /// <returns>The string being read.</returns>
        /// <exception cref="T:System.IO.EndOfStreamException">The end of the stream is reached. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        public virtual string ReadString()
        {
            if (this.m_stream == null)
            {
                throw new Exception();
            }

            var num = 0;
            int len = this.ReadCompactInt();
            int bytesLen = len > 0 ? len : -2 * len;
            string ret = string.Empty;
            if (len == 0) return ret;
            sbyte[] bytes = this.ReadSBytes(bytesLen);
            unsafe
            {
                fixed (sbyte* pbArr = &bytes[0])
                {
                    ret = new string(pbArr, 0, bytes.Length - (len > 0 ? 1 : 2),
                        len > 0 ? Encoding.ASCII : Encoding.Unicode);
                }
            }

            return ret;
        }
        
        /// <summary>
        ///     Reads a string from the current stream. The string is prefixed with the length, encoded as an UnrealCompactInteger.
        /// </summary>
        /// <returns>The string being read.</returns>
        /// <exception cref="T:System.IO.EndOfStreamException">The end of the stream is reached. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        public virtual string ReadIntSizedUnicodeString()
        {
            if (this.m_stream == null)
            {
                throw new Exception();
            }

            var num = 0;
            int len = this.ReadInt32();
            string ret = string.Empty;
            if (len == 0) return ret;
            sbyte[] bytes = this.ReadSBytes(len);
            unsafe
            {
                fixed (sbyte* pbArr = &bytes[0])
                {
                    ret = new string(pbArr, 0, bytes.Length, Encoding.Unicode);
                }
            }

            return ret;
        }

        public virtual int ReadCompactInt()
        {
            long base_pos = this.m_stream.Position;
            var value = 0;


            var negative = 1;
            var shift = 6;
            byte b = this.ReadByte();
            if ((b & 0x80) == 0x80)
            {
                negative = -1;
            }

            value |= b & 0x3F;
            if ((b & 0x40) == 0)
            {
                return value * negative;
            }

            do
            {
                // Check for a corrupted stream.  Read a max of 5 bytes.
                // In a future version, add a DataFormatException.
                if (shift == 6 + 4 * 7) // 5 bytes max per Int32, shift += 7
                {
                    throw new UnrealIOException("Compact INT bad Format: position " + base_pos, new FormatException());
                }

                // ReadByte handles end of stream cases for us.
                b = this.ReadByte();
                value |= (b & 0x7F) << shift;
                shift += 7;
            } while ((b & 0x80) != 0);

            return value * negative;
        }

        private int InternalReadOneChar()
        {
            var num1 = 0;
            long num2;
            long num3 = num2 = 0L;
            if (this.m_stream.CanSeek)
            {
                num3 = this.m_stream.Position;
            }

            if (this.m_charBytes == null)
            {
                this.m_charBytes = new byte[128];
            }

            if (this.m_singleChar == null)
            {
                this.m_singleChar = new char[1];
            }

            while (num1 == 0)
            {
                int byteCount = this.m_2BytesPerChar ? 2 : 1;
                int num4 = this.m_stream.ReadByte();
                this.m_charBytes[0] = (byte) num4;
                if (num4 == -1)
                {
                    byteCount = 0;
                }

                if (byteCount == 2)
                {
                    int num5 = this.m_stream.ReadByte();
                    this.m_charBytes[1] = (byte) num5;
                    if (num5 == -1)
                    {
                        byteCount = 1;
                    }
                }

                if (byteCount == 0)
                {
                    return -1;
                }

                try
                {
                    num1 = this.m_decoder.GetChars(this.Codec.Decode(this.m_charBytes), 0, byteCount, this.m_singleChar, 0);
                }
                catch
                {
                    if (this.m_stream.CanSeek)
                    {
                        this.m_stream.Seek(num3 - this.m_stream.Position, SeekOrigin.Current);
                    }

                    throw;
                }
            }

            if (num1 == 0)
            {
                return -1;
            }

            return this.m_singleChar[0];
        }

        public unsafe string PeekHeader()
        {
            sbyte[] bytes = this.PeekSBytes(CodecFactory.Lineage2VerxxxLength);
            fixed (sbyte* pbArr = &bytes[0])
            {
                return new string(pbArr, 0, bytes.Length,
                    Encoding.Unicode);
            }
        }

        private bool DetectTail()
        {
            var bytebuffer = new byte[20];
            var result = true;
            long position = this.m_stream.Position;
            this.m_stream.Position = (int) this.m_stream.Length - 20;
            this.m_stream.Read(bytebuffer, 0, 20);
            this.m_stream.Position = position;
            for (var i = 0; i < bytebuffer.Length; i++)
            {
                if (i < 4 || i > 4 && i < 8 || i > 9 && i < 12)
                {
                    result = result && bytebuffer[i] == 0x0;
                }

                if (i > 15)
                {
                    result = result && bytebuffer[i] == 0x0;
                }
            }

            return result;
        }

        /// <summary>
        /// Reads array of <see cref="IUnrealSerializable"/> compatible objects at selected <see cref="position"/> with selected <see cref="size"/>
        /// </summary>
        /// <typeparam name="T"><see cref="IUnrealSerializable"/> compatible objects, must have default constructor</typeparam>
        /// <param name="position">offset in <see cref="FileReader"/> data stream</param>
        /// <param name="size"></param>
        /// <returns></returns>
        public T[] ReadArray<T>(int position, int size)
            where T : class, IUnrealSerializable, new()
        {
            try
            {
                var result = new T[size];
                StreamPosition = position;
                for (int i = 0; i < size; i++)
                {
                    result[i] = this.Read<T>();
                }

                return result;
            }
            catch (UnrealIOException exception)
            {
                Console.WriteLine($"Some error occured when reading {typeof(T)} at {StreamPosition}");
#if DEBUG
                Console.WriteLine($"Stack trace: \r\n\t{exception.StackTrace}");
#endif
                return null;
            }
        }

        /// <summary>
        /// Reads array of <see cref="IUnrealSerializable"/> compatible objects at selected <see cref="position"/> with selected <see cref="size"/>
        /// </summary>
        /// <typeparam name="T"><see cref="IUnrealSerializable"/> compatible objects, must have default constructor</typeparam>
        /// <param name="position">offset in <see cref="FileReader"/> data stream</param>
        /// <param name="size"></param>
        /// <returns></returns>
        public T[] ReadArray<T>()
            where T : class, IUnrealSerializable, new()
        {
            try
            {
                var result = new T[this.ReadInt32()];
                for (int i = 0; i < result.Length; i++)
                {
                    result[i] = this.Read<T>();
                }

                return result;
            }
            catch (UnrealIOException exception)
            {
                Console.WriteLine($"Some error occured when reading {typeof(T)} at {StreamPosition}");
#if DEBUG
                Console.WriteLine($"Stack trace: \r\n\t{exception.StackTrace}");
#endif
                return null;
            }
        }

        /// <summary>
        /// Reads any of <see cref="IUnrealSerializable"/> compatible object
        /// </summary>
        /// <typeparam name="T"><see cref="IUnrealSerializable"/> compatible objects, must have default constructor</typeparam>
        /// <returns></returns>
        public T Read<T>()
            where T : class, IUnrealSerializable, new()
        {
            try
            {
                var result = new T();
                result.Read(this);
                return result;
            }
            catch (UnrealIOException exception)
            {
                Console.WriteLine($"Some error occured when reading {typeof(T)} at {StreamPosition}");
#if DEBUG
                Console.WriteLine($"Stack trace: \r\n\t{exception.StackTrace}");
#endif
                return null;
            }
        }

        /// <summary>
        /// Reads any of <see cref="default"/> compatible object
        /// </summary>
        /// <typeparam name="T"><see cref="IUnrealSerializable"/> compatible objects, must have default constructor</typeparam>
        /// <returns></returns>
        public T ReadStruct<T>()
            where T : struct
        {
            try
            {
                var result = default(T);

                if (typeof(T) == typeof(int))
                {
                    result = this.ReadInt32().Unbox<T>();
                }
                else if (typeof(T) == typeof(long))
                {
                    result = this.ReadInt64().Unbox<T>();
                }
                else if (typeof(T) == typeof(float))
                {
                    result = this.ReadSingle().Unbox<T>();
                }
                //else if (typeof(T) == typeof(float))
                //{
                //    result = this.ReadSingle().Unbox<T>();
                //}

                return result;
            }
            catch (UnrealIOException exception)
            {
                Console.WriteLine($"Some error occured when reading {typeof(T)} at {StreamPosition}");
#if DEBUG
                Console.WriteLine($"Stack trace: \r\n\t{exception.StackTrace}");
#endif
                return default(T);
            }
        }

        
    }
}