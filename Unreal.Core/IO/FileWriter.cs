﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Security;
using System.Text;
using Unreal.Core.Codecs;
using Unreal.Core.Codecs.Rsa;
using Unreal.Core.Exceptions;
using Unreal.Core.IO.Interfaces;
using Unreal.Core.Objects;

namespace Unreal.Core.IO
{
    public class FileWriter : IEncodedWriter, IDisposable
    {
        protected Stream OutStream;
        private byte[] _buffer;
        private Encoding _encoding;
        private Encoder _encoder;
        [OptionalField]
        private bool _leaveOpen;
        [OptionalField]
        private char[] _tmpOneCharBuffer;
        private byte[] _largeByteBuffer;
        private int _maxChars;
        private const int LargeByteBufferSize = 256;

        public FileInfo FileEntry { get; set; }
        public ICodec Codec { get; set; }
        public string EncodeKeyBase => FileEntry.Name;
        /// <summary>Exposes access to the underlying stream position of the <see cref="T:Unreal.Core.FileIO" />.</summary>
        /// <returns>The underlying stream position associated with the <see langword="FileIO" />.</returns>
        public virtual long StreamPosition
        {
            get => this.OutStream.Position;

            set => this.OutStream.Position = value + this.Codec.DataOffset;
        }
        public void SetHeader()
        {
            var headerBytes = Encoding.Unicode.GetBytes(Codec.Header);
            if (headerBytes.Length > 0)
            {
                Write(headerBytes, false);
            }
        }

        /// <summary>Specifies a <see cref="T:System.IO.BinaryWriter" /> with no backing store.</summary>
        
        public static readonly FileWriter Null = new FileWriter();

        /// <summary>Holds the underlying stream.</summary>


        /// <summary>Initializes a new instance of the <see cref="T:System.IO.BinaryWriter" /> class that writes to a stream.</summary>

        protected FileWriter()
            : this(new Lineage2NullCodec())
        {

        }
        protected FileWriter(ICodec codec)
        {
            this.FileEntry = null;
            this.OutStream = Stream.Null;
            this._buffer = new byte[16];
            this._encoding = (Encoding)new UTF8Encoding(false, true);
            this._encoder = this._encoding.GetEncoder();
            if (codec is Lineage2Ver41x)
            {
                throw new UnrealCoreException("RSA based codecs is not supported for serial streams");
            }

            this.Codec = codec;
        }

        /// <summary>Initializes a new instance of the <see cref="T:System.IO.BinaryWriter" /> class based on the specified stream and using UTF-8 encoding.</summary>
        /// <param name="codec">Lineage compatible file reader codec</param>
        /// <param name="outputEntry">An output file.</param>
        /// <exception cref="T:System.ArgumentException">The stream does not support writing or is already closed. </exception>
        /// <exception cref="T:System.ArgumentNullException">
        /// <paramref name="outputEntry" /> is <see langword="null" />. </exception>
        public FileWriter(ICodec codec, FileInfo outputEntry)
          : this(codec, outputEntry, (Encoding)new UTF8Encoding(false, true), false)
        {
        }

        /// <summary>Initializes a new instance of the <see cref="T:System.IO.BinaryWriter" /> class based on the specified stream and character encoding.</summary>
        /// <param name="codec">Lineage compatible file reader codec</param>
        /// <param name="outputEntry">An output file.</param>
        /// <param name="encoding">The character encoding to use. </param>
        /// <exception cref="T:System.ArgumentException">The stream does not support writing or is already closed. </exception>
        /// <exception cref="T:System.ArgumentNullException">
        /// <paramref name="output" /> or <paramref name="encoding" /> is <see langword="null" />. </exception>
        public FileWriter(ICodec codec, FileInfo outputEntry, Encoding encoding)
          : this(codec, outputEntry, encoding, false)
        {
        }

        /// <summary>Initializes a new instance of the <see cref="T:System.IO.BinaryWriter" /> class based on the specified stream and character encoding, and optionally leaves the stream open.</summary>
        /// <param name="codec">Lineage compatible file reader codec</param>
        /// <param name="outputEntry">An output file.</param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <param name="leaveOpen">
        /// <see langword="true" /> to leave the stream open after the <see cref="T:System.IO.BinaryWriter" /> object is disposed; otherwise, <see langword="false" />.</param>
        /// <exception cref="T:System.ArgumentException">The stream does not support writing or is already closed. </exception>
        /// <exception cref="T:System.ArgumentNullException">
        /// <paramref name="outputEntry" /> or <paramref name="encoding" /> is <see langword="null" />. </exception>

        public FileWriter(ICodec codec, FileInfo outputEntry, Encoding encoding, bool leaveOpen)
        {
            if (outputEntry == null)
                throw new UnrealIOException("Target file info is null", new ArgumentNullException(nameof(outputEntry)));
            if (encoding == null)
                throw new UnrealIOException("Encoding is null", new ArgumentNullException(nameof(encoding)));
            if (outputEntry.Exists)
            {
                outputEntry.Delete();
            }
            var output = outputEntry.Create();
            if (!output.CanWrite)
                throw new UnrealIOException("Stream Not Writable", new ArgumentException(("Argument_StreamNotWritable")));

            if (codec is Lineage2Ver41x)
            {
                throw new UnrealCoreException("RSA based codecs is not supported for serial streams");
            }

            this.FileEntry = outputEntry;
            this.Codec = codec;
            this.Codec.GenerateKey(EncodeKeyBase);
            this.OutStream = output;
            this._buffer = new byte[16];
            this._encoding = encoding;
            this._encoder = this._encoding.GetEncoder();
            this._leaveOpen = leaveOpen;
            SetHeader();
        }

        /// <summary>Closes the current <see cref="T:System.IO.BinaryWriter" /> and the underlying stream.</summary>
        public virtual void Close()
        {
            this.Dispose(true);
        }

        /// <summary>Releases the unmanaged resources used by the <see cref="T:System.IO.BinaryWriter" /> and optionally releases the managed resources.</summary>
        /// <param name="disposing">
        /// <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only unmanaged resources. </param>
        
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return;
            if (this._leaveOpen)
                this.OutStream.Flush();
            else
                this.OutStream.Close();
        }

        /// <summary>Releases all resources used by the current instance of the <see cref="T:System.IO.BinaryWriter" /> class.</summary>
        
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>Gets the underlying stream of the <see cref="T:System.IO.BinaryWriter" />.</summary>
        /// <returns>The underlying stream associated with the <see langword="BinaryWriter" />.</returns>
        
        public virtual Stream BaseStream
        {
            
            get
            {
                this.Flush();
                return this.OutStream;
            }
        }

        /// <summary>Clears all buffers for the current writer and causes any buffered data to be written to the underlying device.</summary>
        
        public virtual void Flush()
        {
            this.OutStream.Flush();
        }

        /// <summary>Sets the position within the current stream.</summary>
        /// <param name="offset">A byte offset relative to <paramref name="origin" />. </param>
        /// <param name="origin">A field of <see cref="T:System.IO.SeekOrigin" /> indicating the reference point from which the new position is to be obtained. </param>
        /// <returns>The position with the current stream.</returns>
        /// <exception cref="T:System.IO.IOException">The file pointer was moved to an invalid location. </exception>
        /// <exception cref="T:System.ArgumentException">The <see cref="T:System.IO.SeekOrigin" /> value is invalid. </exception>
        
        public virtual long Seek(int offset, SeekOrigin origin)
        {
            return this.OutStream.Seek((long)offset, origin);
        }

        /// <summary>Writes a one-byte <see langword="Boolean" /> value to the current stream, with 0 representing <see langword="false" /> and 1 representing <see langword="true" />.</summary>
        /// <param name="value">The <see langword="Boolean" /> value to write (0 or 1). </param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        
        public virtual void Write(bool value)
        {
            this._buffer[0] = value ? (byte)1 : (byte)0;
            this.OutStream.Write(this._buffer, 0, 1);
        }

        /// <summary>Writes an unsigned byte to the current stream and advances the stream position by one byte.</summary>
        /// <param name="value">The unsigned byte to write. </param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        
        public virtual void Write(byte value)
        {
            this.OutStream.WriteByte(Codec.Encode(value));
        }

        /// <summary>Writes a signed byte to the current stream and advances the stream position by one byte.</summary>
        /// <param name="value">The signed byte to write. </param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        [CLSCompliant(false)]
        
        public virtual void Write(sbyte value)
        {
            this.OutStream.WriteByte(Codec.Encode((byte)value));
        }

        /// <summary>Writes a byte array to the underlying stream.</summary>
        /// <param name="buffer">A byte array containing the data to write. </param>
        /// <param name="encoded">Provides ability to write unencoded data</param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.ArgumentNullException">
        /// <paramref name="buffer" /> is <see langword="null" />. </exception>
        public virtual void Write(byte[] buffer, bool encoded)
        {
            if (buffer == null)
                throw new UnrealIOException($"Argument is null", new ArgumentNullException(nameof(buffer)));
            this.OutStream.Write(encoded ? Codec.Encode(buffer) : buffer, 0, buffer.Length);
        }
        /// <summary>Writes a byte array to the underlying stream.</summary>
        /// <param name="buffer">A byte array containing the data to write. </param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.ArgumentNullException">
        /// <paramref name="buffer" /> is <see langword="null" />. </exception>
        
        public virtual void Write(byte[] buffer)
        {
            if (buffer == null)
                throw new UnrealIOException($"Argument is null", new ArgumentNullException(nameof(buffer)));
            this.OutStream.Write(Codec.Encode(buffer), 0, buffer.Length);
        }

        /// <summary>Writes a region of a byte array to the current stream.</summary>
        /// <param name="buffer">A byte array containing the data to write. </param>
        /// <param name="index">The starting point in <paramref name="buffer" /> at which to begin writing. </param>
        /// <param name="count">The number of bytes to write. </param>
        /// <exception cref="T:System.ArgumentException">The buffer length minus <paramref name="index" /> is less than <paramref name="count" />. </exception>
        /// <exception cref="T:System.ArgumentNullException">
        /// <paramref name="buffer" /> is <see langword="null" />. </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// <paramref name="index" /> or <paramref name="count" /> is negative. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        
        public virtual void Write(byte[] buffer, int index, int count)
        {
            this.OutStream.Write(Codec.Encode(buffer), index, count);
        }

        /// <summary>Writes a Unicode character to the current stream and advances the current position of the stream in accordance with the <see langword="Encoding" /> used and the specific characters being written to the stream.</summary>
        /// <param name="ch">The non-surrogate, Unicode character to write. </param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.ArgumentException">
        /// <paramref name="ch" /> is a single surrogate character.</exception>
        [SecuritySafeCritical]
        
        public virtual unsafe void Write(char ch)
        {
            if (char.IsSurrogate(ch))
                throw new UnrealIOException($"Surrogates Not Allowed As Single Char", new ArgumentException(("Arg_SurrogatesNotAllowedAsSingleChar")));
            int bytes1;
            fixed (byte* bytes2 = this._buffer)
                bytes1 = this._encoder.GetBytes(&ch, 1, bytes2, this._buffer.Length, true);
            this.OutStream.Write(Codec.Encode(this._buffer), 0, bytes1);
        }

        /// <summary>Writes a character array to the current stream and advances the current position of the stream in accordance with the <see langword="Encoding" /> used and the specific characters being written to the stream.</summary>
        /// <param name="chars">A character array containing the data to write. </param>
        /// <exception cref="T:System.ArgumentNullException">
        /// <paramref name="chars" /> is <see langword="null" />. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        
        public virtual void Write(char[] chars)
        {
            if (chars == null)
                throw new UnrealIOException($"Argument is null", new ArgumentNullException(nameof(chars)));
            byte[] bytes = this._encoding.GetBytes(chars, 0, chars.Length);
            this.OutStream.Write(Codec.Encode(bytes), 0, bytes.Length);
        }

        /// <summary>Writes a section of a character array to the current stream, and advances the current position of the stream in accordance with the <see langword="Encoding" /> used and perhaps the specific characters being written to the stream.</summary>
        /// <param name="chars">A character array containing the data to write. </param>
        /// <param name="index">The starting point in <paramref name="chars" /> from which to begin writing. </param>
        /// <param name="count">The number of characters to write. </param>
        /// <exception cref="T:System.ArgumentException">The buffer length minus <paramref name="index" /> is less than <paramref name="count" />. </exception>
        /// <exception cref="T:System.ArgumentNullException">
        /// <paramref name="chars" /> is <see langword="null" />. </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// <paramref name="index" /> or <paramref name="count" /> is negative. </exception>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        
        public virtual void Write(char[] chars, int index, int count)
        {
            byte[] bytes = this._encoding.GetBytes(chars, index, count);
            this.OutStream.Write(Codec.Encode(bytes), 0, bytes.Length);
        }

        /// <summary>Writes an eight-byte floating-point value to the current stream and advances the stream position by eight bytes.</summary>
        /// <param name="value">The eight-byte floating-point value to write. </param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        [SecuritySafeCritical]
        
        public virtual unsafe void Write(double value)
        {
            ulong num = (ulong)*(long*)&value;
            this._buffer[0] = (byte)num;
            this._buffer[1] = (byte)(num >> 8);
            this._buffer[2] = (byte)(num >> 16);
            this._buffer[3] = (byte)(num >> 24);
            this._buffer[4] = (byte)(num >> 32);
            this._buffer[5] = (byte)(num >> 40);
            this._buffer[6] = (byte)(num >> 48);
            this._buffer[7] = (byte)(num >> 56);
            this.OutStream.Write(Codec.Encode(this._buffer), 0, 8);
        }

        /// <summary>Writes a two-byte signed integer to the current stream and advances the stream position by two bytes.</summary>
        /// <param name="value">The two-byte signed integer to write. </param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        
        public virtual void Write(short value)
        {
            this._buffer[0] = (byte)value;
            this._buffer[1] = (byte)((uint)value >> 8);
            this.OutStream.Write(Codec.Encode(this._buffer), 0, 2);
        }

        /// <summary>Writes a two-byte unsigned integer to the current stream and advances the stream position by two bytes.</summary>
        /// <param name="value">The two-byte unsigned integer to write. </param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        [CLSCompliant(false)]
        
        public virtual void Write(ushort value)
        {
            this._buffer[0] = (byte)value;
            this._buffer[1] = (byte)((uint)value >> 8);
            this.OutStream.Write(Codec.Encode(this._buffer), 0, 2);
        }

        /// <summary>Writes a four-byte signed integer to the current stream and advances the stream position by four bytes.</summary>
        /// <param name="value">The four-byte signed integer to write. </param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        
        public virtual void Write(int value)
        {
            this._buffer[0] = (byte)value;
            this._buffer[1] = (byte)(value >> 8);
            this._buffer[2] = (byte)(value >> 16);
            this._buffer[3] = (byte)(value >> 24);
            this.OutStream.Write(Codec.Encode(this._buffer), 0, 4);
        }

        /// <summary>Writes a four-byte unsigned integer to the current stream and advances the stream position by four bytes.</summary>
        /// <param name="value">The four-byte unsigned integer to write. </param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        [CLSCompliant(false)]
        
        public virtual void Write(uint value)
        {
            this._buffer[0] = (byte)value;
            this._buffer[1] = (byte)(value >> 8);
            this._buffer[2] = (byte)(value >> 16);
            this._buffer[3] = (byte)(value >> 24);
            this.OutStream.Write(Codec.Encode(this._buffer), 0, 4);
        }

        /// <summary>Writes an eight-byte signed integer to the current stream and advances the stream position by eight bytes.</summary>
        /// <param name="value">The eight-byte signed integer to write. </param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        
        public virtual void Write(long value)
        {
            this._buffer[0] = (byte)value;
            this._buffer[1] = (byte)(value >> 8);
            this._buffer[2] = (byte)(value >> 16);
            this._buffer[3] = (byte)(value >> 24);
            this._buffer[4] = (byte)(value >> 32);
            this._buffer[5] = (byte)(value >> 40);
            this._buffer[6] = (byte)(value >> 48);
            this._buffer[7] = (byte)(value >> 56);
            this.OutStream.Write(Codec.Encode(this._buffer), 0, 8);
        }

        /// <summary>Writes an eight-byte unsigned integer to the current stream and advances the stream position by eight bytes.</summary>
        /// <param name="value">The eight-byte unsigned integer to write. </param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        [CLSCompliant(false)]
        
        public virtual void Write(ulong value)
        {
            this._buffer[0] = (byte)value;
            this._buffer[1] = (byte)(value >> 8);
            this._buffer[2] = (byte)(value >> 16);
            this._buffer[3] = (byte)(value >> 24);
            this._buffer[4] = (byte)(value >> 32);
            this._buffer[5] = (byte)(value >> 40);
            this._buffer[6] = (byte)(value >> 48);
            this._buffer[7] = (byte)(value >> 56);
            this.OutStream.Write(Codec.Encode(this._buffer), 0, 8);
        }

        /// <summary>Writes a four-byte floating-point value to the current stream and advances the stream position by four bytes.</summary>
        /// <param name="value">The four-byte floating-point value to write. </param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        [SecuritySafeCritical]
        
        public virtual unsafe void Write(float value)
        {
            uint num = *(uint*)&value;
            this._buffer[0] = (byte)num;
            this._buffer[1] = (byte)(num >> 8);
            this._buffer[2] = (byte)(num >> 16);
            this._buffer[3] = (byte)(num >> 24);
            this.OutStream.Write(Codec.Encode(this._buffer), 0, 4);
        }

        /// <summary>Writes a length-prefixed string to this stream in the current encoding of the <see cref="T:System.IO.BinaryWriter" />, and advances the current position of the stream in accordance with the encoding used and the specific characters being written to the stream.</summary>
        /// <param name="value">The value to write. </param>
        /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        /// <exception cref="T:System.ArgumentNullException">
        /// <paramref name="value" /> is <see langword="null" />. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        [SecuritySafeCritical]

        public virtual void Write(string value)
        {
            if (value == null)
                throw new UnrealIOException($"Argument is null", new ArgumentNullException(nameof(value)));
            if (value.Length > 0)
            {
                var ascii = Encoding.ASCII;
                var unicode = Encoding.Unicode;

                // Convert the string into a byte array.
                var unicodeBytes = unicode.GetBytes(value);

                // Perform the conversion from one encoding to the other.
                var asciiBytes = Encoding.Convert(unicode, ascii, unicodeBytes);

                // Convert the new byte[] into a char[] and then into a string.
                var asciiChars = new char[ascii.GetCharCount(asciiBytes, 0, asciiBytes.Length)];
                ascii.GetChars(asciiBytes, 0, asciiBytes.Length, asciiChars, 0);
                var asciiString = new string(asciiChars);
                if (asciiString == value)
                {
                    asciiBytes = ascii.GetBytes(value + '\0');
                    WriteCompactInt(asciiBytes.Length);
                    Write(asciiBytes);
                }
                else
                {

                    unicodeBytes = unicode.GetBytes(value + '\0');
                    WriteCompactInt(-unicodeBytes.Length / 2);
                    Write(unicodeBytes);
                }
            }
            else
            {
                Write((byte) 0x00);
            }
        }
        
        /// <summary>Writes an Int32 length-prefixed string to this stream in the Unicode encoding of the <see cref="T:System.IO.BinaryWriter" />, and advances the current position of the stream in accordance with the encoding used and the specific characters being written to the stream.</summary>
         /// <param name="value">The value to write. </param>
         /// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
         /// <exception cref="T:System.ArgumentNullException">
         /// <paramref name="value" /> is <see langword="null" />. </exception>
         /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        [SecuritySafeCritical]

        public virtual void WriteIntSizedUnicodeString(string value)
        {
            if (value == null)
                throw new UnrealIOException($"Argument is null", new ArgumentNullException(nameof(value)));
            if (value.Length > 0)
            {
                var unicode = Encoding.Unicode;

                // Convert the string into a byte array.
                byte[] unicodeBytes = unicode.GetBytes(value + '\0');
                this.Write(unicodeBytes.Length);
                Write(unicodeBytes);
                
            }
            else
            {
                Write((byte)0x00);
            }
        }

        /// <summary>Writes a 32-bit integer in a compressed format.</summary>
        /// <param name="value">The 32-bit integer to be written. </param>
        /// <exception cref="T:System.IO.EndOfStreamException">The end of the stream is reached. </exception>
        /// <exception cref="T:System.ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="T:System.IO.IOException">The stream is closed. </exception>

        public void WriteCompactInt(int value)
        {
            var negative = value < 0;
            value = Math.Abs(value);
            var bytes = new[]{
                (value) & 0x3f,
                (value >> 6) & 0x7f,
                (value >> 6 + 7) & 0x7f,
                (value >> 6 + 7 + 7) & 0x7f,
                (value >> 6 + 7 + 7 + 7) & 0x7f
            };

            if (negative) bytes[0] |= 0x80;

            var lastInd = 4;
            while (lastInd > 0 && bytes[lastInd] == 0)
            {
                lastInd--;
            }

            var res = new byte[lastInd + 1];

            for (var i = 0; i <= lastInd; i++)
            {
                if (i != lastInd)
                    bytes[i] |= i == 0 ? 0x40 : 0x80;
                res[i] = (byte)bytes[i];
            }
            Write(res);
        }

        public void Write<T>(T serializable)
            where T : IUnrealSerializable
        {
            serializable.Write(this);
        }
        public void WriteArray<T>(T[] serializableBuffer)
            where T : IUnrealSerializable
        {
            for (int i = 0; i < serializableBuffer.Length; i++)
            {
                serializableBuffer[i].Write(this);
            }
        }
        public void WriteArray<T>(T[] serializableBuffer, int position)
            where T : IUnrealSerializable
        {
            StreamPosition = position;
            for (int i = 0; i < serializableBuffer.Length; i++)
            {
                serializableBuffer[i].Write(this);
            }
        }
        public void WriteArray<T>(T[] serializableBuffer, int position, int size)
            where T : IUnrealSerializable
        {
            StreamPosition = position;
            for (int i = 0; i < size; i++)
            {
                serializableBuffer[i].Write(this);
            }
        }
    }
}