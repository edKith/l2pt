﻿namespace Unreal.Core.IO.Interfaces
{
    public interface IEncodedReader : IEncodedStream
    {
        string PeekHeader();
    }
}