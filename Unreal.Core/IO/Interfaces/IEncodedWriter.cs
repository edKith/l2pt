﻿namespace Unreal.Core.IO.Interfaces
{
    public interface IEncodedWriter : IEncodedStream
    {
        void SetHeader();
    }
}