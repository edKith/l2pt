﻿using System.IO;

namespace Unreal.Core.IO.Interfaces
{
    public interface IUnrealFileStream
    {
        FileInfo FileEntry { get; set; }
    }
}