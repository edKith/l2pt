﻿using System;
using Unreal.Core.Objects;

namespace Unreal.Core.IO.Interfaces
{
    public interface IUnrealSerializer
    {
        long StreamPosition { get; set; }
       // IUnrealSerializer Serialize<T>(ref T value);
        IUnrealSerializer Serialize(ref bool value);
        IUnrealSerializer Serialize(ref short value);
        IUnrealSerializer Serialize(ref ushort value);
        IUnrealSerializer Serialize(ref float value);
        IUnrealSerializer Serialize(ref double value);
        //Temporary Disabled
        //IUnrealSerializer Serialize(ref decimal value);
        IUnrealSerializer Serialize(ref int value);
        IUnrealSerializer Serialize(ref int value, bool compact);
        IUnrealSerializer Serialize(ref uint value);
        IUnrealSerializer Serialize(ref long value);
        IUnrealSerializer Serialize(ref ulong value);
        IUnrealSerializer Serialize(ref byte value);
        IUnrealSerializer Serialize(ref sbyte value);
        IUnrealSerializer Serialize(ref char value);
        IUnrealSerializer Serialize(ref string value);

        IUnrealSerializer Serialize<T>(ref T value)
            where T : IUnrealSerializable, new();
        IUnrealSerializer Serialize<T>(ref T[] value)
            where T : IUnrealSerializable, new();
        IUnrealSerializer Serialize<T>(ref T[] value, int size)
            where T : IUnrealSerializable, new();
    }
}