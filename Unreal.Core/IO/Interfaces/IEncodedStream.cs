﻿using Unreal.Core.Codecs;

namespace Unreal.Core.IO.Interfaces
{
    public interface IEncodedStream : IUnrealFileStream
    {
        ICodec Codec { get; set; }
        string EncodeKeyBase { get; }
    }
}