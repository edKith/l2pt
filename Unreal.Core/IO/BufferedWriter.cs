﻿using System;
using System.IO;
using System.Text;
using Unreal.Core.Codecs;
using Unreal.Core.Codecs.Xor;
using Unreal.Core.IO.Interfaces;

namespace Unreal.Core.IO
{
    public class BufferedWriter : IEncodedStream, IDisposable
    {
        private BinaryWriter _innerStream;
        private ICodec codec;

        public FileInfo FileEntry { get; set; }

        public ICodec Codec
        {
            get => this.codec;
            set
            {
                this.codec = value;
                if (!string.IsNullOrEmpty(FileEntry?.Name))
                {
                    this.codec.Title = FileEntry.Name;
                }
            }

        }

        public string EncodeKeyBase => this.GetEncodeKeyBase();

        private string GetEncodeKeyBase()
        {
            string result = string.Empty;
            switch (this.Codec)
            {
                case Lineage2Ver121 fileNameBasedCodec:
                    result = this.FileEntry.Name;
                    break;
                default:
                    break;
            }

            this.Codec.GenerateKey(result);

            return result;
        }

        public BufferedWriter(FileInfo targetFile, ICodec codec)
        {
            this.FileEntry = targetFile;
            this.Codec = codec;
            this.GetEncodeKeyBase();
        }

        public void Write(params byte[] data)
        {
            using (this._innerStream = new BinaryWriter(File.Create(this.FileEntry.FullName), Encoding.UTF8))
            {
                if (this.Codec is Lineage2NullCodec)
                {
                    Console.WriteLine("Action chosen: pure writing");
                    Console.WriteLine($"Pure data size: {data.Length}");
                    Console.WriteLine();
                    this._innerStream.Write(data);
                }
                else
                {
                    Console.WriteLine($"Header \"{this.Codec.Header}\"");
                    Console.WriteLine($"Action chosen: encode {CodecFactory.PeekNumber(this.Codec.Header)}");
                    this._innerStream.Write(Encoding.Unicode.GetBytes(this.Codec.Header));
                    byte[] encodedData = this.Codec.Encode(data);
                    this._innerStream.Write(encodedData);
                }

                Console.WriteLine("Stream written");
            }
        }

        public void Dispose()
        {
            this._innerStream?.Dispose();
        }
    }
}