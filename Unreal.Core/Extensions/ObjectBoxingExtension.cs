﻿namespace Unreal.Core.Extensions
{
    public static class ObjectBoxingExtension
    {
        public static T Unbox<T>(this object input)
            where T : struct
        {
            return (T)input;
        }
    }
}