﻿using System.Collections.Generic;
using System.IO;
using Unreal.Core.IO.Compression;

namespace Unreal.Core.Extensions
{
    public static class MemoryStreamExtensions
    {
        public static byte[] DecompressStream(this MemoryStream stream)
        {
            using (MemoryStream decompressedFileStream = new MemoryStream())
            {
                using (ZInputStream decompressionStream =
                    new ZInputStream(stream))
                {
                    decompressionStream.CopyTo(decompressedFileStream);
                }

                return decompressedFileStream.ToArray();
            }
        }

        public static byte[] CompressStream(this MemoryStream stream)
        {
            var compressed = new List<byte>();
            using (MemoryStream compressedMemoryStream = new MemoryStream())
            {
                using (ZOutputStream compressionStream = new ZOutputStream(compressedMemoryStream, 6))
                    //using (var compressionStream = new ZDeflaterOutputStream(compressedMemoryStream, 6))
                {
                    stream.CopyTo(compressionStream);
                }

                compressed.AddRange(compressedMemoryStream.ToArray());
            }

            return compressed.ToArray();
        }
    }
}