﻿using System;
using System.Runtime.Serialization;

namespace Unreal.Core.RSA.Exceptions
{
    public class RSAException : Exception
    {
        public RSAException()
        {
        }

        public RSAException(string message) : base(message)
        {
        }

        public RSAException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected RSAException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}