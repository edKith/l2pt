using System;
using Unreal.Core.RSA.Exceptions;
using Unreal.Core.RSA.Parameters;

namespace Unreal.Core.RSA
{
    /**
    * a buffer wrapper for an asymmetric block cipher, allowing input
    * to be accumulated in a piecemeal fashion until final processing.
    */
    public class RSACipher
    {
		private readonly RsaBlindedEngine cipher = new RsaBlindedEngine();
        protected static readonly byte[] EmptyBuffer = new byte[0];

        private byte[] buffer;
		private int bufOff;
        
		/**
        * initialise the buffer and the underlying cipher.
        *
        * @param forEncryption if true the cipher is initialised for
        *  encryption, if false for decryption.
        * @param param the key and other data required by the cipher.
        */
        public  void Init(
            bool				forEncryption,
            ICipherParameters	parameters)
        {
			Reset();

			cipher.Init(forEncryption, parameters);

			//
			// we allow for an extra byte where people are using their own padding
			// mechanisms on a raw cipher.
			//
			this.buffer = new byte[cipher.GetInputBlockSize() + (forEncryption ? 1 : 0)];
			this.bufOff = 0;
        }
        

		public byte[] ProcessBytes(
			byte[]	input,
			int		inOff,
			int		length)
		{
			if (length < 1)
				return null;

			if (input == null)
				throw new ArgumentNullException("input");
			if (bufOff + length > buffer.Length)
				throw new DataLengthException("attempt to process message to long for cipher");

			Array.Copy(input, inOff, buffer, bufOff, length);
			bufOff += length;
			return null;
		}

        /**
        * process the contents of the buffer using the underlying
        * cipher.
        *
        * @return the result of the encryption/decryption process on the
        * buffer.
        * @exception InvalidCipherTextException if we are given a garbage block.
        */
        public virtual byte[] DoFinal(
            byte[] input)
        {
            return DoFinal(input, 0, input.Length);
        }
        public byte[] DoFinal()
        {
			byte[] outBytes = bufOff > 0
				?	cipher.ProcessBlock(buffer, 0, bufOff)
				:	EmptyBuffer;

			Reset();

			return outBytes;
        }

		public byte[] DoFinal(
			byte[]	input,
			int		inOff,
			int		length)
		{
			ProcessBytes(input, inOff, length);
			return DoFinal();
		}

		/// <summary>Reset the buffer</summary>
        public void Reset()
        {
			if (buffer != null)
			{
				Array.Clear(buffer, 0, buffer.Length);
				bufOff = 0;
			}
        }
    }
}
