using System;
using Unreal.Core.RSA.Math;

namespace Unreal.Core.RSA.Parameters
{
	public class RsaKeyParameters
		: AsymmetricKeyParameter
    {
        private readonly BigInteger modulus;
        private readonly BigInteger exponent;

		public RsaKeyParameters(
            bool		isPrivate,
            BigInteger	modulus,
            BigInteger	exponent)
			: base(isPrivate)
        {
			if (modulus == null)
				throw new ArgumentNullException("modulus");
			if (exponent == null)
				throw new ArgumentNullException("exponent");
			if (modulus.SignValue <= 0)
				throw new ArgumentException("Not a valid RSA modulus", "modulus");
			if (exponent.SignValue <= 0)
				throw new ArgumentException("Not a valid RSA exponent", "exponent");

			this.modulus = modulus;
			this.exponent = exponent;
        }

		public BigInteger Modulus => this.modulus;

        public BigInteger Exponent => this.exponent;

        public override bool Equals(
			object obj)
        {
            if (!(obj is RsaKeyParameters kp))
			{
				return false;
			}

			return kp.IsPrivate == this.IsPrivate
				&& kp.Modulus.Equals(this.modulus)
				&& kp.Exponent.Equals(this.exponent);
        }

		public override int GetHashCode()
        {
            return this.modulus.GetHashCode() ^ this.exponent.GetHashCode() ^ this.IsPrivate.GetHashCode();
        }
    }
}
