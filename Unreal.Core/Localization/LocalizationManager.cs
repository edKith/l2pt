﻿using System;
using System.IO;
using Unreal.Core.Exceptions;
using Unreal.Core.IniFileParser;
using Unreal.Core.IniFileParser.Model;
using Unreal.Core.IO;

namespace Unreal.Core.Localization
{
    public static class LocalizationManager
    {
        public const string HeadSectionKey = "Language";
        public const string HeadSectionName = "LanguageSet";
        public const string FileName = "Localization.ini";

        public static IniData Data;
        public static int CurrentID;
        public static LanguageConfig CurrentLanguage;

        private static (int ID, LanguageConfig Config)[] localizations;

        public static (int ID, LanguageConfig Config)[] Localizations
        {
            get { return localizations ?? throw new UnrealConfigNotLoadedException(FileName); }
            set { localizations = value; }
        }

        public static bool LoadConfig(DirectoryInfo runningDirectory)
        {
            if (!runningDirectory.Exists) return false;
            var localizationFileInfo = new FileInfo(Path.Combine(runningDirectory.FullName, FileName));
            if (localizationFileInfo.Exists)
            {
                var bufferedReader = new BufferedIniParser();
                Data = bufferedReader.ReadFile(localizationFileInfo);
                CurrentID = int.Parse(Data[HeadSectionName][HeadSectionKey]);
                var currentLanguage = Data[HeadSectionName][CurrentID.ToString()];
                CurrentLanguage.Name = currentLanguage;
                CurrentLanguage.LoadFromIni(Data);
                return true;
            }
            return false;
        }
    }
}