﻿using System;
using Unreal.Core.IniFileParser.Model;

namespace Unreal.Core.Localization
{
    public struct LanguageConfig
    {
        public string Name;
        public bool EnableEngSelection;
        public LanguageInfo LanguageInfo;
        public FontMapInfo FontMap;

        public void LoadFromIni(IniData localizationData)
        {
            EnableEngSelection = localizationData[LocalizationManager.HeadSectionName][nameof(EnableEngSelection)].Equals("true", StringComparison.InvariantCultureIgnoreCase);

            var languageSection = localizationData.Sections.GetSectionData(this.Name);
            this.LanguageInfo.LoadFromIniSection(languageSection);

            var fontMapSection = localizationData.Sections.GetSectionData($"{this.Name}_FontMap");
            this.FontMap.LoadFromIniSection(fontMapSection);
        }
    }
}