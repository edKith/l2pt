﻿using Unreal.Core.IniFileParser.Model;

namespace Unreal.Core.Localization
{
    public struct LanguageInfo
    {
        public string Code;

        public string Font;
        public string Glyph;

        public string Font2;
        public string Glyph2;

        public string Minimap;

        public string LogoTexture;
        public string MiniLogoTexture;

        public string TownMap;
        public string ReplayLogoTexture;

        public string FontLib;

        public void LoadFromIniSection(SectionData section)
        {
            var name = section.SectionName;
            this.Code = section.Keys[$"{name}_{nameof(this.Code)}"];
            this.Font = section.Keys[$"{name}_{nameof(this.Font)}"];
            this.Glyph = section.Keys[$"{name}_{nameof(this.Glyph)}"];
            this.Font2 = section.Keys[$"{name}_{nameof(this.Font2)}"];
            this.Glyph2 = section.Keys[$"{name}_{nameof(this.Glyph2)}"];
            this.Minimap = section.Keys[$"{name}_{nameof(this.Minimap)}"];
            this.LogoTexture = section.Keys[$"{name}_{nameof(this.LogoTexture)}"];
            this.MiniLogoTexture = section.Keys[$"{name}_{nameof(this.MiniLogoTexture)}"];
            this.TownMap = section.Keys[$"{name}_{nameof(this.TownMap)}"];
            this.ReplayLogoTexture = section.Keys[$"{name}_{nameof(this.ReplayLogoTexture)}"];
            this.FontLib = section.Keys[$"{name}_{nameof(this.FontLib)}"];
        }
    }
}