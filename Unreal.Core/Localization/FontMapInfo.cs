﻿// ReSharper disable InconsistentNaming

using Unreal.Core.IniFileParser.Model;

namespace Unreal.Core.Localization
{
    public struct FontMapInfo
    {
        public string NormalFont;
        public string NormalFont_Bold;
        public string NormalFont_Italic;
        public string NormalFont_Bold_Italic;
        public string DamageTextFont;


        public void LoadFromIniSection(SectionData section)
        {
            var name = section.SectionName;
            this.NormalFont = section.Keys[$"${nameof(this.NormalFont)}"];
            this.NormalFont_Bold = section.Keys[$"${nameof(this.NormalFont_Bold)}"];
            this.NormalFont_Italic = section.Keys[$"${nameof(this.NormalFont_Italic)}"];
            this.NormalFont_Bold_Italic = section.Keys[$"${nameof(this.NormalFont_Bold_Italic)}"];
            this.DamageTextFont = section.Keys[$"${nameof(this.DamageTextFont)}"];
        }
    }
}