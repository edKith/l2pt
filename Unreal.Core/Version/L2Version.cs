﻿using System.ComponentModel;
using Unreal.Core.Version.Attributes;

namespace Unreal.Core.Version
{
    public enum L2Version
    {
        [VersionGroup("The Chaotic Chronicle")]
        [Description("Chronicle - Prelude")]
        Prelude,
        [VersionGroup("The Chaotic Chronicle")]
        [Description("Chronicle 1 - Harbringers of War")]
        Chronicle1,
        [VersionGroup("The Chaotic Chronicle")]
        [Description("Chronicle 2 - Age of Splendor")]
        Chronicle2,
        [VersionGroup("The Chaotic Chronicle")]
        [Description("Chronicle 3 - Rise of Darkness")]
        Chronicle3,
        [VersionGroup("The Chaotic Chronicle")]
        [Description("Chronicle 4 - Scions of Destiny")]
        Chronicle4,
        [VersionGroup("The Chaotic Chronicle")]
        [Description("Chronicle 5 - Oath of Blood")]
        Chronicle5,
        [VersionGroup("The Chaotic Throne")]
        [Description("Throne - Interlude")]
        Interlude,
        [VersionGroup("The Chaotic Throne")]
        [Description("Throne 1 - Kamael")]
        Throne1,
        [VersionGroup("The Chaotic Throne")]
        [Description("Throne 1 - Hellbound")]
        Throne15,
        [VersionGroup("The Chaotic Throne")]
        [Description("Throne 2 - Gracia pt. 1")]
        Throne21,
        [VersionGroup("The Chaotic Throne")]
        [Description("Throne 2 - Gracia pt. 2")]
        Throne22,
        [VersionGroup("The Chaotic Throne")]
        [Description("Throne 2 - Gracia Final")]
        Throne23,
        [VersionGroup("The Chaotic Throne")]
        [Description("Throne 2 - Gracia Epilogue")]
        Throne24,
        [VersionGroup("The Chaotic Throne")]
        [Description("Throne 2 - Freya")]
        Throne25,
        [VersionGroup("The Chaotic Throne")]
        [Description("Throne 2 - High Five pt. 1-3")]
        Throne261,
        [VersionGroup("The Chaotic Throne")]
        [Description("Throne 2 - High Five pt. 4-5")]
        Throne263,
        [VersionGroup("Goddess of Destruction")]
        [Description("Goddess of Destruction 1 - Awakening")]
        GddessofDestruction1,
        [VersionGroup("Epic Tales Of Aden")]
        [Description("Epic Tales Of Aden 1 - Ertheia")]
        EpicTalesOfAden1,
        [VersionGroup("Epic Tales Of Aden")]
        [Description("Epic Tales Of Aden 2 - Infinity Odyssey")]
        EpicTalesOfAden2,
        [VersionGroup("Epic Tales Of Aden")]
        [Description("Epic Tales Of Aden 3 - Helios")]
        EpicTalesOfAden3,
        [VersionGroup("Epic Tales Of Aden")]
        [Description("Epic Tales Of Aden 4 - Grand Crusade")]
        EpicTalesOfAden4,
        Max
    }
}