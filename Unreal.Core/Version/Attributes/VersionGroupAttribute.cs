﻿using System;

namespace Unreal.Core.Version.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    sealed class VersionGroupAttribute : Attribute
    {
        public string GroupName { get; set; }
        public VersionGroupAttribute()
        {
            GroupName = string.Empty;
        }

        public VersionGroupAttribute(string groupName)
        {
            this.GroupName = groupName;
        }
    }
}