﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unreal.Core.IO;
using Unreal.Core.Localization;
using Unreal.Test.Core.IO;

namespace Unreal.Test.Core.Localization
{
    [TestClass]
    public class LocalizationManagerTest
    {

        [TestMethod]
        public void TestLocalizationLoading()
        {
            UnrealFileSystem.Initialize(UnrealFileSystemTest.ClientPath);
            LocalizationManager.LoadConfig(UnrealFileSystem.Instance.Directory);


            Assert.AreEqual(false, LocalizationManager.CurrentLanguage.EnableEngSelection);
        }
    }
}