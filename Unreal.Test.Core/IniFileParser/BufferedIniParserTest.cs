﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unreal.Core.Codecs;
using Unreal.Core.IniFileParser;
using Unreal.Core.IniFileParser.Model;
using Unreal.Core.IniFileParser.Parser;

namespace Unreal.Test.Core.IniFileParser
{
    [TestClass]
    public class BufferedIniParserTest
    {
        [TestMethod]
        public void TestFileReading()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var expected = "false";
            var result = string.Empty;
            var localizationFileInfo = new FileInfo(Path.Combine(currentDirectory, "localization.ini"));
            if (localizationFileInfo.Exists)
            {
                var bufferedReader = new BufferedIniParser();
                var localizationData = bufferedReader.ReadFile(localizationFileInfo);
                result = localizationData["LanguageSet"]["EnableEngSelection"];
            }

            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void TestFileReadWrite()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var expected = "false";
            var result = string.Empty;
            var localizationFileInfo = new FileInfo(Path.Combine(currentDirectory, "localization.ini"));
            if (localizationFileInfo.Exists)
            {
                var bufferedIniParser = new BufferedIniParser();
                IniData localizationData = bufferedIniParser.ReadFile(localizationFileInfo);
                result = localizationData["LanguageSet"]["EnableEngSelection"];
                Assert.AreEqual(expected, result);


                var targetFileInfo = new FileInfo(Path.Combine(currentDirectory, "localization-saved.ini"));
                localizationData["LanguageSet"]["EnableEngSelection"] = true.ToString();
                bufferedIniParser.WriteFile(targetFileInfo, localizationData, Lineage2NullCodec.Instance);
            }

        }
        [TestMethod]
        public void TestL2Ini()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var expected = "pwlogin.playdefo.com";
            var result = string.Empty;
            var l2iniParser = new ConcatenateDuplicatedKeysIniDataParser();
            l2iniParser.Configuration.ConcatenateDuplicateKeys = true;
            l2iniParser.Configuration.CommentString = ";";
            l2iniParser.Configuration.CaseInsensitive = true;
            var l2iniFileInfo = new FileInfo(Path.Combine(currentDirectory, "L2.ini"));
            if (l2iniFileInfo.Exists)
            {
                var bufferedIniParser = new BufferedIniParser(l2iniParser);
                IniData l2iniData = bufferedIniParser.ReadFile(l2iniFileInfo);
                result = l2iniData["URL"]["ServerAddr"];
                Assert.AreEqual(expected, result);


                var targetFileInfo = new FileInfo(Path.Combine(currentDirectory, "L2-saved.ini"));
                l2iniData["URL"]["ServerAddr"] = "localhost";
                bufferedIniParser.WriteFile(targetFileInfo, l2iniData, Lineage2NullCodec.Instance);
            }

        }
    }
}