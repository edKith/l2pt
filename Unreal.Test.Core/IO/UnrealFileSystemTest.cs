﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unreal.Core.IO;

namespace Unreal.Test.Core.IO
{
    [TestClass]
    public class UnrealFileSystemTest
    {
        public const string ClientPath = "e:\\Lineage2\\L2Clients\\L2H5T0\\System\\";
        [TestMethod]
        public void TestInitialize()
        {
            UnrealFileSystem.Initialize(ClientPath);
            Assert.IsNotNull(UnrealFileSystem.Instance.Executable);
        }

        //[TestMethod]
        //public void TestRun()
        //{
        //    UnrealFileSystem.Initialize(ClientPath);
        //    UnrealFileSystem.Instance.Run();
        //    Assert.IsNotNull(UnrealFileSystem.Instance.Executable);
        //}
    }
}