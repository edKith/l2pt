﻿using Unreal.Core.Submodules;

namespace Patcher.NET
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                FileCodecProcessing.SystemPatcher(args[0]?.Equals("-n") == true);
            }
            else
            {
                FileCodecProcessing.SystemPatcher(false);
            }
        }
    }
}