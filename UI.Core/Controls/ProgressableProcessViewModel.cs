﻿using UI.Core.Mvvm;
using Unreal.Core.Output;

namespace UI.Core.Controls
{
    public class ProgressableProcessViewModel : BindableBase
    {
        private IProgressable progressable;
        public string Title => this.progressable.Title;
        public string Message => this.progressable.Progress.Message;
        public double Minimum => this.progressable.Progress.Minimum;
        public double Current => this.progressable.Progress.Current;
        public double Maximum => this.progressable.Progress.Maximum;
        public bool IsIndeterminate => this.progressable.Progress.State == ProgressState.Indeterminate;

        public ProgressableProcessViewModel(IProgressable progressable)
        {
            this.progressable = progressable;
            this.RaisePropertyChanged(nameof(Title));
            progressable.Progress.StateUpdated += ProgressStateUpdated;
            progressable.Progress.MessageUpdated += ProgressMessageUpdated;
            progressable.Progress.CurrentUpdated += ProgressCurrentUpdated;
            progressable.Progress.MinimumUpdated += ProgressMinimumUpdated;
            progressable.Progress.MaximumUpdated += ProgressMaximumUpdated;
        }

        public ProgressableProcessViewModel()
        {
            this.progressable = NullProgressable.Instance;
            this.RaisePropertyChanged(nameof(Title));
            progressable.Progress.StateUpdated += ProgressStateUpdated;
            progressable.Progress.MessageUpdated += ProgressMessageUpdated;
            progressable.Progress.CurrentUpdated += ProgressCurrentUpdated;
            progressable.Progress.MinimumUpdated += ProgressMinimumUpdated;
            progressable.Progress.MaximumUpdated += ProgressMaximumUpdated;
        }

        private void ProgressMaximumUpdated(double maximum)
        {
            this.RaisePropertyChanged(nameof(Maximum));
        }

        private void ProgressCurrentUpdated(double current)
        {
            this.RaisePropertyChanged(nameof(Current));
        }

        private void ProgressMinimumUpdated(double minimum)
        {
            this.RaisePropertyChanged(nameof(Minimum));
        }

        private void ProgressMessageUpdated(string message)
        {
            this.RaisePropertyChanged(nameof(Message));
        }

        private void ProgressStateUpdated(ProgressState state)
        {
            this.RaisePropertyChanged(nameof(IsIndeterminate));
        }
    }
}