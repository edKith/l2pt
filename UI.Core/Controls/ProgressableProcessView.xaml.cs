﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI.Core.Controls
{
    /// <summary>
    /// Interaction logic for ProgressableProcessView.xaml
    /// </summary>
    public partial class ProgressableProcessView : Window
    {
        public ProgressableProcessView()
        {
            InitializeComponent();
        }
    }
}
