﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace UI.Core.Wpf.Mvvm
{
    /// <summary>
    /// Represents each node of nested properties expression and takes care of 
    /// subscribing/unsubscribing INotifyPropertyChanged.PropertyChanged listeners on it.
    /// </summary>
    internal class PropertyObserverNode
    {
        private readonly Action _action;
        private INotifyPropertyChanged _inpcObject;

        public string PropertyName { get; }
        public PropertyObserverNode Next { get; set; }

        public PropertyObserverNode(string propertyName, Action action)
        {
            this.PropertyName = propertyName;
            this._action = () =>
            {
                action?.Invoke();
                if (this.Next == null) return;
                this.Next.UnsubscribeListener();
                this.GenerateNextNode();
            };
        }

        public void SubscribeListenerFor(INotifyPropertyChanged inpcObject)
        {
            this._inpcObject = inpcObject;
            this._inpcObject.PropertyChanged += this.OnPropertyChanged;

            if (this.Next != null) this.GenerateNextNode();
        }

        private void GenerateNextNode()
        {
            var propertyInfo = this._inpcObject.GetType().GetRuntimeProperty(this.PropertyName); // TODO: To cache, if the step consume significant performance. Note: The type of _inpcObject may become its base type or derived type.
            var nextProperty = propertyInfo.GetValue(this._inpcObject);
            if (nextProperty == null) return;
            if (!(nextProperty is INotifyPropertyChanged nextInpcObject))
                throw new InvalidOperationException("Trying to subscribe PropertyChanged listener in object that " +
                                                    $"owns '{this.Next.PropertyName}' property, but the object does not implements INotifyPropertyChanged.");

            this.Next.SubscribeListenerFor(nextInpcObject);
        }

        private void UnsubscribeListener()
        {
            if (this._inpcObject != null)
                this._inpcObject.PropertyChanged -= this.OnPropertyChanged;

            this.Next?.UnsubscribeListener();
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // Invoke action when e.PropertyName == null in order to satisfy:
            //  - DelegateCommandFixture.GenericDelegateCommandObservingPropertyShouldRaiseOnEmptyPropertyName
            //  - DelegateCommandFixture.NonGenericDelegateCommandObservingPropertyShouldRaiseOnEmptyPropertyName
            if (e?.PropertyName == this.PropertyName || e?.PropertyName == null)
            {
                this._action?.Invoke();
            }
        }
    }
}
