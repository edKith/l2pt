﻿using UI.Core.Controls;
using Unreal.Core.Output;

namespace UI.Core
{
    public static class UICore
    {
        public static void CreateProgressableView(IProgressable progressable)
        {
            var view = new ProgressableProcessView();
            var viewModel = new ProgressableProcessViewModel(progressable);
            view.DataContext = viewModel;
            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            view.Left = desktopWorkingArea.Right - view.Width;
            view.Top = desktopWorkingArea.Bottom - view.Height;
            view.Show();
        }
    }
}