﻿using Unreal.Core.Submodules;
using Unreal.Engine.TextureConverter;

namespace L2EncDec
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length > 1)
            {
                if (args[0].Equals("folder"))
                {
                    FileCodecProcessing.SystemPatcher(args[1], true);
                }
                if (args[0].Equals("pngconvert"))
                {
                    PngToTga.Convert(args[1]);
                }
            }
            FileCodecProcessing.CodecApply(args);
        }
    }
}