﻿using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unreal.Engine.LineageData;
using Unreal.Engine.LineageData.Definition;
using Unreal.Engine.LineageData.Definition.BinaryField;
using Unreal.Engine.LineageData.Factories;
using Unreal.Engine.LineageData.Parsers;

namespace Unreal.Test.Engine.LineageData.Definition
{
    [TestClass]
    public class LineageDataFileTest
    {
        [TestMethod]
        public void TestLineageDataDefinitionCreation()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var mockFile = new LineageDataFileMock
            {
                Name = "SysString",
                Localizable = true
            };
            mockFile.Text.Definition.Add(new TextField { Name = "string_begin", Type = DataFieldType.DataBegin} );
            mockFile.Text.Definition.Add(new TextField { Index = true, Name = "id", Type = DataFieldType.Integer} );
            mockFile.Text.Definition.Add(new TextField { Name = "string", Type = DataFieldType.String} );
            mockFile.Text.Definition.Add(new TextField { Name = "string_end", Type = DataFieldType.DataEnd } );

            //mockFile.Binary.Definition.Add(new ComplexBinaryField()
            //{
            //    Name = "id_value_pair",
            //    DataLength = 1,
            //    LengthFormat = ArrayLengthFormat.Constant,
            //    Value = new List<IBinaryField>()
            //    {
            //        new IntegerBinaryField() { Name = "id" },
            //        new StringBinaryField() { Name = "string" }
            //    }
            //});

            mockFile.Binary.Definition.Add(new IntegerBinaryField(){Name = "id"});
            mockFile.Binary.Definition.Add(new StringBinaryField(){Name = "string"});

            var mockInstance = new LineageDataInstance();

            mockInstance.AddDataFile(mockFile);

            XmlStorageManager.SaveXml(mockFile, new DirectoryInfo(currentDirectory), mockFile.Name);


            Assert.AreEqual(true, true);
        }

        [TestMethod]
        public void TestLineageDataComplexDefinitionCreation()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var mockFile = new LineageDataFileMock
            {
                Name = "SysString",
                Localizable = true
            };
            mockFile.Text.Definition.Add(new TextField { Name = "string_begin", Type = DataFieldType.DataBegin} );
            mockFile.Text.Definition.Add(new TextField { Name = "id_value_pair", Type = DataFieldType.ComplexArray} );
            mockFile.Text.Definition.Add(new TextField { Name = "string_end", Type = DataFieldType.DataEnd } );

            mockFile.Binary.Definition.Add(new ComplexBinaryField()
            {
                Name = "id_value_pair",
                DataLength = 1,
                LengthFormat = ArrayLengthFormat.Constant,
                Value = new BinaryDataHolder()
                {
                    Pattern =
                    {
                        new IntegerBinaryField() { Name = "id" },
                        new StringBinaryField() { Name = "string" }
                    }
                }
            });
            
            var mockInstance = new LineageDataInstance();

            mockInstance.AddDataFile(mockFile);

            XmlStorageManager.SaveXml(mockFile, new DirectoryInfo(currentDirectory), mockFile.Name);


            Assert.AreEqual(true, true);
        }
        [TestMethod]
        public void TestLineageDataDefinitionLoading()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            
            var test = new LineageDataFileMock
            {
                Name = "SysString",
                Localizable = true
            };
            var loaded = XmlStorageManager.LoadXml(test, new DirectoryInfo(currentDirectory), test.Name);
            if (loaded)
            {
                XmlStorageManager.SaveXml(test, new DirectoryInfo(currentDirectory), "SysString-saved");
            }



            Assert.AreEqual(true, loaded);
        }
        
        [TestMethod]
        public void TestLineageDataFileLoading()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            
            var test = new LineageDataFileMock
            {
                Name = "SysString",
                Localizable = true
            };
            var loaded = XmlStorageManager.LoadXml(test, new DirectoryInfo(currentDirectory), test.Name);
            if (loaded)
            {
                test.Read(DataIO.CreateFileReader("sysstring-e.dat"));
            }

            test.ToText();
            File.WriteAllText("sysstring-e.txt", test.Text.ToText());

            Assert.AreEqual(true, loaded);
        }
    }
}
