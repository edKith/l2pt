﻿using Unreal.Engine.LineageData.Definition;

namespace Unreal.Test.Engine.LineageData.Definition
{
    public class LineageDataFileMock : LineageDataFile
    {
        public LineageDataFileMock() : base()
        {
            this.Name = "MockGrp";
        }

        public override bool ToBinary()
        {
            throw new System.NotImplementedException();
        }
        
    }
}