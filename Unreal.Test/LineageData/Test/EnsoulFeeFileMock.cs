﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unreal.Core.Codecs;
using Unreal.Core.IO;
using Unreal.Engine.LineageData.Factories;

namespace Unreal.Test.Engine.LineageData.Test
{
    [TestClass]
    public class EnsoulFeeFileMock
    {

        private string filename = "e:\\Lineage2\\L2Clients\\Classic2\\system\\ensoul_fee_client_Classic.dat";
        
        [TestMethod]
        public void TestToText()
        {
            var fileInfo = new FileInfo(this.filename);
            var array = new Byte[0];
            using (BufferedReader reader = new BufferedReader(fileInfo))
            {
                array = reader.Read();
            }

            using (BufferedWriter writer = new BufferedWriter(fileInfo, Lineage2NullCodec.Instance))
            {
                writer.Write(array);
            }

            var ensoul_fee_client =  new ensoul_fee_client();

            using (FileReader readerFile = new FileReader(fileInfo))
            {
                ensoul_fee_client.Read(readerFile);
            }

            XmlStorageManager.SaveXml(ensoul_fee_client, new DirectoryInfo(fileInfo.DirectoryName), $"{Path.GetFileNameWithoutExtension(this.filename)}");

            Assert.AreEqual(true, true);
        }

        [TestMethod]
        public void TestToBinary()
        {
            var fileInfo = new FileInfo(this.filename);

            var ensoul_fee_client =  new ensoul_fee_client();
            
            XmlStorageManager.LoadXml(ensoul_fee_client, new DirectoryInfo(fileInfo.DirectoryName), $"{Path.GetFileNameWithoutExtension(this.filename)}");

            using (FileWriter fileWriter = new FileWriter(Lineage2NullCodec.Instance, new FileInfo(this.filename+ ".dat")))
            {
                ensoul_fee_client.Write(fileWriter);
                fileWriter.Write("SafePackage");
            }

            Assert.AreEqual(true, true);
        }
    }
}