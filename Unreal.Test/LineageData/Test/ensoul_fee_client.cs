﻿using System.Collections.Generic;
using System.Xml.Linq;
using Unreal.Core.IO;
using Unreal.Core.Objects;

namespace Unreal.Test.Engine.LineageData.Test
{
    public class ensoul_fee_client : IUnrealSerializable, IXmlStorage
    {
        public List<Definition> Definitions = new List<Definition>();
        public class Definition : IUnrealSerializable, IXmlStorage
        {
            public int crystal_type;
            public IDCountPairs ensoul_fee_normal = new IDCountPairs() {Name = "ensoul_fee_normal" };
            public IDCountPairs ensoul_fee_bm = new IDCountPairs() { Name = "ensoul_fee_bm" };
            public IDCountPairs ensoul_refee_normal = new IDCountPairs() { Name = "ensoul_refee_normal" };
            public IDCountPairs ensoul_refee_bm = new IDCountPairs() { Name = "ensoul_refee_bm" };
            public IDCountPairs ensoul_extraction_normal = new IDCountPairs() { Name = "ensoul_extraction_normal" };
            public IDCountPairs ensoul_extraction_bm = new IDCountPairs() { Name = "ensoul_extraction_bm" };

            public void Read(FileReader reader)
            {
                this.crystal_type = reader.ReadInt32();
                ensoul_fee_normal.Read(reader);
                ensoul_fee_bm.Read(reader);
                ensoul_refee_normal.Read(reader);
                ensoul_refee_bm.Read(reader);
                ensoul_extraction_normal.Read(reader);
                ensoul_extraction_bm.Read(reader);
            }

            public void Write(FileWriter writer)
            {
                writer.Write(this.crystal_type);
                ensoul_fee_normal.Write(writer);
                ensoul_fee_bm.Write(writer);
                ensoul_refee_normal.Write(writer);
                ensoul_refee_bm.Write(writer);
                ensoul_extraction_normal.Write(writer);
                ensoul_extraction_bm.Write(writer);
            }

            public XElement ToXml()
            {
                var rootElement = new XElement(nameof(Definition));

                rootElement.Add(new XAttribute(nameof(this.crystal_type), this.crystal_type));
                rootElement.Add(ensoul_fee_normal.ToXml());
                rootElement.Add(ensoul_fee_bm.ToXml());
                rootElement.Add(ensoul_refee_normal.ToXml());
                rootElement.Add(ensoul_refee_bm.ToXml());
                rootElement.Add(ensoul_extraction_normal.ToXml());
                rootElement.Add(ensoul_extraction_bm.ToXml());

                return rootElement;
            }

            public bool FromXml(XElement rootElement)
            {
                crystal_type = (int)rootElement.Attribute(nameof(this.crystal_type));
                ensoul_fee_normal.FromXml(rootElement.Element(nameof(this.ensoul_fee_normal)));
                ensoul_fee_bm.FromXml(rootElement.Element(nameof(this.ensoul_fee_bm)));
                ensoul_refee_normal.FromXml(rootElement.Element(nameof(this.ensoul_refee_normal)));
                ensoul_refee_bm.FromXml(rootElement.Element(nameof(this.ensoul_refee_bm)));
                ensoul_extraction_normal.FromXml(rootElement.Element(nameof(this.ensoul_extraction_normal)));
                ensoul_extraction_bm.FromXml(rootElement.Element(nameof(this.ensoul_extraction_bm)));

                return true;
            }
        }
        public void Read(FileReader reader)
        {
            var count = reader.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                var defi = new Definition();
                defi.Read(reader);
                Definitions.Add(defi);
            }
        }

        public void Write(FileWriter writer)
        {
            writer.Write(this.Definitions.Count);
            foreach (Definition definition in this.Definitions)
            {
                writer.Write(definition);
            }
        }

        public XElement ToXml()
        {
            var rootElement = new XElement(nameof(ensoul_fee_client));
            foreach (Definition definition in this.Definitions)
            {
                rootElement.Add(definition.ToXml());
            }
            return rootElement;
        }

        public bool FromXml(XElement rootElement)
        {
            if (rootElement.Name == nameof(ensoul_fee_client))
            {
                foreach (XElement xElement in rootElement.Elements())
                {
                    var definitionField = new Definition();
                    if (definitionField.FromXml(xElement))
                    {
                        this.Definitions.Add(definitionField);
                    }
                }

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

