﻿using System;
using System.Linq;
using System.Xml.Linq;
using Unreal.Core.IO;
using Unreal.Core.Objects;

namespace Unreal.Test.Engine.LineageData.Test
{
    public class IDCountPair : IXmlStorage, IUnrealSerializable
    {
        public int ID;
        public int Count;

        public void Read(FileReader reader)
        {
            this.ID = reader.ReadInt32();
            this.Count = reader.ReadInt32();
        }

        public void Write(FileWriter writer)
        {
            writer.Write(ID);
            writer.Write(Count);
        }

        public XElement ToXml()
        {
            var rootElement = new XElement(nameof(IDCountPair));
            rootElement.Add(new XAttribute(nameof(ID), this.ID));
            rootElement.Add(new XAttribute(nameof(Count), this.Count));
            return rootElement;
        }

        public bool FromXml(XElement rootElement)
        {
            this.ID = (int)rootElement.Attribute(nameof(this.ID));
            this.Count = (int)rootElement.Attribute(nameof(this.Count));

            return true;
        }
    }

    public class IDCountPairs : IUnrealSerializable, IXmlStorage
    {
        public string Name = nameof(IDCountPairs);
        private int count = 0;
        private IDCountPair[] pairs;

        public void Read(FileReader reader)
        {
            count = reader.ReadInt32();
            this.pairs = new IDCountPair[this.count];
            for (var index = 0; index < this.pairs.Length; index++)
            {
                this.pairs[index] = reader.Read<IDCountPair>();
            }
        }

        public void Write(FileWriter writer)
        {
            writer.Write(pairs.Length);
            foreach (IDCountPair idCountPair in this.pairs)
            {
                idCountPair.Write(writer);
            }
        }

        public XElement ToXml()
        {
            var rootElement = new XElement(Name);
            foreach (IDCountPair idCountPair in this.pairs)
            {
                rootElement.Add(idCountPair.ToXml());
            }

            return rootElement;
        }

        public bool FromXml(XElement rootElement)
        {
            var elements = rootElement.Elements(nameof(IDCountPair)).ToArray();
            var count = elements.Length;
            pairs = new IDCountPair[count];

            for (int i = 0; i < count; i++)
            {
                pairs[i] = new IDCountPair();
                pairs[i].FromXml(elements[i]);
            }

            return true;
        }
    }
}
