﻿using System.Collections.Generic;
using Unreal.Engine.LineageData.Native;

namespace Unreal.Test.Engine.LineageData.HighFiveDefinition
{
    public class HighFiveNativeData
    {
        public static LineageNativeData CreateAttachType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "AttachType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "relative" },
                    new LineageNativeDataEntry { ID = 1, Value = "absolute" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateDataTableScriptType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "DataTableScriptType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 65, Value = "ST_L_LA" },
                    new LineageNativeDataEntry { ID = 67, Value = "ST_L_C_LA" },
                    new LineageNativeDataEntry { ID = 7, Value = "ST_EACH" },
                    new LineageNativeDataEntry { ID = 3, Value = "ST_LIVE_CLASSIC" },
                    new LineageNativeDataEntry { ID = 5, Value = "ST_LIVE_ARENA" },
                    new LineageNativeDataEntry { ID = 6, Value = "ST_CLASSIC_ARENA" },
                    new LineageNativeDataEntry { ID = 17, Value = "ST_LIVEONLY" },
                    new LineageNativeDataEntry { ID = 18, Value = "ST_CLASSICONLY" },
                    new LineageNativeDataEntry { ID = 20, Value = "ST_ARENAONLY" },
                    new LineageNativeDataEntry { ID = 0, Value = "None" }
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateOnMapContext()
        {
            var nativeData = new LineageNativeData
            {
                Name = "OnMapContext",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = -1, Value = "NONE" },
                    new LineageNativeDataEntry { ID = 0, Value = "PIN" },
                    new LineageNativeDataEntry { ID = 1, Value = "RANGE0" },
                    new LineageNativeDataEntry { ID = 2, Value = "RANGE1" },
                    new LineageNativeDataEntry { ID = 3, Value = "RANGE2" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateBlinkType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "BlinkType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "always" },
                    new LineageNativeDataEntry { ID = 2, Value = "blink_long" },
                    new LineageNativeDataEntry { ID = 3, Value = "blink_short" }
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateUnit()
        {
            var nativeData = new LineageNativeData
            {
                Name = "Unit",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "hour" },
                    new LineageNativeDataEntry { ID = 2, Value = "minute" },
                    new LineageNativeDataEntry { ID = 3, Value = "second" },
                    new LineageNativeDataEntry { ID = 4, Value = "raid" },
                    new LineageNativeDataEntry { ID = 5, Value = "time" }
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateEnchantEffectType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "EnchantEffectType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "Enchant" },
                    new LineageNativeDataEntry { ID = 0x10000000, Value = "Variation" },
                    new LineageNativeDataEntry { ID = 0x20000000, Value = "None" },
                    new LineageNativeDataEntry { ID = 0x40000000, Value = "EnchantedVariation" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateTexException()
        {
            var nativeData = new LineageNativeData
            {
                Name = "TexException",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0x40, Value = "mantle" },
                    new LineageNativeDataEntry { ID = 0x100, Value = "alldressplushair" },
                    new LineageNativeDataEntry { ID = 0x80, Value = "hidehead" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateRace()
        {
            var nativeData = new LineageNativeData
            {
                Name = "Race",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "human" },
                    new LineageNativeDataEntry { ID = 1, Value = "elf" },
                    new LineageNativeDataEntry { ID = 2, Value = "dark_elf" },
                    new LineageNativeDataEntry { ID = 3, Value = "orc" },
                    new LineageNativeDataEntry { ID = 4, Value = "dwarf" },
                    new LineageNativeDataEntry { ID = 5, Value = "kamael" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateTutorialType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "TutorialType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "novice" },
                    new LineageNativeDataEntry { ID = 1, Value = "content" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateEnsoulType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "EnsoulType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "Phy_1" },
                    new LineageNativeDataEntry { ID = 2, Value = "Phy_2" },
                    new LineageNativeDataEntry { ID = 3, Value = "Phy_3" },
                    new LineageNativeDataEntry { ID = 4, Value = "Phy_4" },
                    new LineageNativeDataEntry { ID = 5, Value = "Phy_5" },
                    new LineageNativeDataEntry { ID = 6, Value = "Phy_6" },
                    new LineageNativeDataEntry { ID = 7, Value = "Phy_7" },
                    new LineageNativeDataEntry { ID = 8, Value = "Phy_8" },
                    new LineageNativeDataEntry { ID = 9, Value = "Phy_9" },
                    new LineageNativeDataEntry { ID = 10, Value = "Mag_1" },
                    new LineageNativeDataEntry { ID = 11, Value = "Mag_2" },
                    new LineageNativeDataEntry { ID = 12, Value = "Mag_3" },
                    new LineageNativeDataEntry { ID = 13, Value = "Mag_4" },
                    new LineageNativeDataEntry { ID = 14, Value = "Mag_5" },
                    new LineageNativeDataEntry { ID = 15, Value = "Mag_6" },
                    new LineageNativeDataEntry { ID = 16, Value = "Mag_7" },
                    new LineageNativeDataEntry { ID = 17, Value = "Mag_8" },
                    new LineageNativeDataEntry { ID = 18, Value = "Mag_9" },
                    new LineageNativeDataEntry { ID = 19, Value = "PPhy_1" },
                    new LineageNativeDataEntry { ID = 20, Value = "PPhy_2" },
                    new LineageNativeDataEntry { ID = 21, Value = "PPhy_3" },
                    new LineageNativeDataEntry { ID = 22, Value = "PPhy_4" },
                    new LineageNativeDataEntry { ID = 23, Value = "PPhy_5" },
                    new LineageNativeDataEntry { ID = 24, Value = "PPhy_6" },
                    new LineageNativeDataEntry { ID = 25, Value = "PPhy_7" },
                    new LineageNativeDataEntry { ID = 26, Value = "PPhy_8" },
                    new LineageNativeDataEntry { ID = 27, Value = "PPhy_9" },
                    new LineageNativeDataEntry { ID = 28, Value = "MMag_1" },
                    new LineageNativeDataEntry { ID = 29, Value = "MMag_2" },
                    new LineageNativeDataEntry { ID = 30, Value = "MMag_3" },
                    new LineageNativeDataEntry { ID = 31, Value = "MMag_4" },
                    new LineageNativeDataEntry { ID = 32, Value = "MMag_5" },
                    new LineageNativeDataEntry { ID = 33, Value = "MMag_6" },
                    new LineageNativeDataEntry { ID = 34, Value = "MMag_7" },
                    new LineageNativeDataEntry { ID = 35, Value = "MMag_8" },
                    new LineageNativeDataEntry { ID = 36, Value = "MMag_9" },
                    new LineageNativeDataEntry { ID = 37, Value = "PM_1" },
                    new LineageNativeDataEntry { ID = 38, Value = "PM_2" },
                    new LineageNativeDataEntry { ID = 39, Value = "PM_3" },
                    new LineageNativeDataEntry { ID = 40, Value = "PM_4" },
                    new LineageNativeDataEntry { ID = 41, Value = "PM_5" },
                    new LineageNativeDataEntry { ID = 42, Value = "PM_6" },
                    new LineageNativeDataEntry { ID = 43, Value = "PM_7" },
                    new LineageNativeDataEntry { ID = 44, Value = "PM_8" },
                    new LineageNativeDataEntry { ID = 45, Value = "PM_9" },
                    new LineageNativeDataEntry { ID = 46, Value = "Tra_1" },
                    new LineageNativeDataEntry { ID = 47, Value = "Tra_2" },
                    new LineageNativeDataEntry { ID = 48, Value = "Tra_3" },
                    new LineageNativeDataEntry { ID = 49, Value = "Tra_4" },
                    new LineageNativeDataEntry { ID = 50, Value = "Tra_5" },
                    new LineageNativeDataEntry { ID = 51, Value = "Tra_6" },
                    new LineageNativeDataEntry { ID = 52, Value = "Tra_7" },
                    new LineageNativeDataEntry { ID = 53, Value = "Tra_8" },
                    new LineageNativeDataEntry { ID = 54, Value = "Tra_9" },
                    new LineageNativeDataEntry { ID = 55, Value = "Att_1" },
                    new LineageNativeDataEntry { ID = 56, Value = "Att_2" },
                    new LineageNativeDataEntry { ID = 57, Value = "Att_3" },
                    new LineageNativeDataEntry { ID = 58, Value = "Att_4" },
                    new LineageNativeDataEntry { ID = 59, Value = "Att_5" },
                    new LineageNativeDataEntry { ID = 60, Value = "Att_6" },
                    new LineageNativeDataEntry { ID = 61, Value = "Att_7" },
                    new LineageNativeDataEntry { ID = 62, Value = "Att_8" },
                    new LineageNativeDataEntry { ID = 63, Value = "Att_9" },
                    new LineageNativeDataEntry { ID = 64, Value = "Etc_1" },
                    new LineageNativeDataEntry { ID = 65, Value = "Etc_2" },
                    new LineageNativeDataEntry { ID = 66, Value = "Etc_3" },
                    new LineageNativeDataEntry { ID = 67, Value = "Etc_4" },
                    new LineageNativeDataEntry { ID = 68, Value = "Etc_5" },
                    new LineageNativeDataEntry { ID = 69, Value = "Etc_6" },
                    new LineageNativeDataEntry { ID = 70, Value = "Etc_7" },
                    new LineageNativeDataEntry { ID = 71, Value = "Etc_8" },
                    new LineageNativeDataEntry { ID = 72, Value = "Etc_9" },
                    new LineageNativeDataEntry { ID = 73, Value = "SIGEL_DEFENSE" },
                    new LineageNativeDataEntry { ID = 74, Value = "SIGEL_HEALTH" },
                    new LineageNativeDataEntry { ID = 75, Value = "TIR_MIGHT" },
                    new LineageNativeDataEntry { ID = 76, Value = "TIR_HASTE" },
                    new LineageNativeDataEntry { ID = 77, Value = "OTHEL_FOCUS" },
                    new LineageNativeDataEntry { ID = 78, Value = "OTHEL_CRTDMG" },
                    new LineageNativeDataEntry { ID = 79, Value = "YR_WINDWALK" },
                    new LineageNativeDataEntry { ID = 80, Value = "YR_GUIDANCE" },
                    new LineageNativeDataEntry { ID = 81, Value = "FEOH_MAGICPOWER" },
                    new LineageNativeDataEntry { ID = 82, Value = "FEOH_EMPOWER" },
                    new LineageNativeDataEntry { ID = 83, Value = "IS_MPREGEN" },
                    new LineageNativeDataEntry { ID = 84, Value = "IS_MPUP" },
                    new LineageNativeDataEntry { ID = 85, Value = "WYNN_ACUMEN" },
                    new LineageNativeDataEntry { ID = 86, Value = "WYNN_LIGHT" },
                    new LineageNativeDataEntry { ID = 87, Value = "EOLH_HPREGEN" },
                    new LineageNativeDataEntry { ID = 88, Value = "EOLH_HEAL" }
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateClass()
        {
            var nativeData = new LineageNativeData
            {
                Name = "Class",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = -1, Value = "none" },
                    new LineageNativeDataEntry { ID = 0, Value = "fighter" },
                    new LineageNativeDataEntry { ID = 1, Value = "warrior" },
                    new LineageNativeDataEntry { ID = 2, Value = "gladiator" },
                    new LineageNativeDataEntry { ID = 3, Value = "warlord" },
                    new LineageNativeDataEntry { ID = 4, Value = "knight" },
                    new LineageNativeDataEntry { ID = 5, Value = "paladin" },
                    new LineageNativeDataEntry { ID = 6, Value = "dark_avenger" },
                    new LineageNativeDataEntry { ID = 7, Value = "rogue" },
                    new LineageNativeDataEntry { ID = 8, Value = "treasure_hunter" },
                    new LineageNativeDataEntry { ID = 9, Value = "hawkeye" },
                    new LineageNativeDataEntry { ID = 10, Value = "mage" },
                    new LineageNativeDataEntry { ID = 11, Value = "wizard" },
                    new LineageNativeDataEntry { ID = 12, Value = "sorcerer" },
                    new LineageNativeDataEntry { ID = 13, Value = "necromancer" },
                    new LineageNativeDataEntry { ID = 14, Value = "warlock" },
                    new LineageNativeDataEntry { ID = 15, Value = "cleric" },
                    new LineageNativeDataEntry { ID = 16, Value = "bishop" },
                    new LineageNativeDataEntry { ID = 17, Value = "prophet" },
                    new LineageNativeDataEntry { ID = 18, Value = "elven_fighter" },
                    new LineageNativeDataEntry { ID = 19, Value = "elven_knight" },
                    new LineageNativeDataEntry { ID = 20, Value = "temple_knight" },
                    new LineageNativeDataEntry { ID = 21, Value = "swordsinger" },
                    new LineageNativeDataEntry { ID = 22, Value = "elven_scout" },
                    new LineageNativeDataEntry { ID = 23, Value = "plains_walker" },
                    new LineageNativeDataEntry { ID = 24, Value = "silver_ranger" },
                    new LineageNativeDataEntry { ID = 25, Value = "elven_mage" },
                    new LineageNativeDataEntry { ID = 26, Value = "elven_wizard" },
                    new LineageNativeDataEntry { ID = 27, Value = "spellsinger" },
                    new LineageNativeDataEntry { ID = 28, Value = "elemental_summoner" },
                    new LineageNativeDataEntry { ID = 29, Value = "oracle" },
                    new LineageNativeDataEntry { ID = 30, Value = "elder" },
                    new LineageNativeDataEntry { ID = 31, Value = "dark_fighter" },
                    new LineageNativeDataEntry { ID = 32, Value = "palus_knight" },
                    new LineageNativeDataEntry { ID = 33, Value = "shillien_knight" },
                    new LineageNativeDataEntry { ID = 34, Value = "bladedancer" },
                    new LineageNativeDataEntry { ID = 35, Value = "assassin" },
                    new LineageNativeDataEntry { ID = 36, Value = "abyss_walker" },
                    new LineageNativeDataEntry { ID = 37, Value = "phantom_ranger" },
                    new LineageNativeDataEntry { ID = 38, Value = "dark_mage" },
                    new LineageNativeDataEntry { ID = 39, Value = "dark_wizard" },
                    new LineageNativeDataEntry { ID = 40, Value = "spellhowler" },
                    new LineageNativeDataEntry { ID = 41, Value = "phantom_summoner" },
                    new LineageNativeDataEntry { ID = 42, Value = "shillien_oracle" },
                    new LineageNativeDataEntry { ID = 43, Value = "shillien_elder" },
                    new LineageNativeDataEntry { ID = 44, Value = "orc_fighter" },
                    new LineageNativeDataEntry { ID = 45, Value = "orc_raider" },
                    new LineageNativeDataEntry { ID = 46, Value = "destroyer" },
                    new LineageNativeDataEntry { ID = 47, Value = "orc_monk" },
                    new LineageNativeDataEntry { ID = 48, Value = "tyrant" },
                    new LineageNativeDataEntry { ID = 49, Value = "orc_mage" },
                    new LineageNativeDataEntry { ID = 50, Value = "orc_shaman" },
                    new LineageNativeDataEntry { ID = 51, Value = "overlord" },
                    new LineageNativeDataEntry { ID = 52, Value = "warcryer" },
                    new LineageNativeDataEntry { ID = 53, Value = "dwarven_fighter" },
                    new LineageNativeDataEntry { ID = 54, Value = "scavenger" },
                    new LineageNativeDataEntry { ID = 55, Value = "bounty_hunter" },
                    new LineageNativeDataEntry { ID = 56, Value = "artisan" },
                    new LineageNativeDataEntry { ID = 57, Value = "warsmith" },
                    new LineageNativeDataEntry { ID = 88, Value = "duelist" },
                    new LineageNativeDataEntry { ID = 89, Value = "dreadnought" },
                    new LineageNativeDataEntry { ID = 90, Value = "phoenix_knight" },
                    new LineageNativeDataEntry { ID = 91, Value = "hell_knight" },
                    new LineageNativeDataEntry { ID = 92, Value = "sagittarius" },
                    new LineageNativeDataEntry { ID = 93, Value = "adventurer" },
                    new LineageNativeDataEntry { ID = 94, Value = "archmage" },
                    new LineageNativeDataEntry { ID = 95, Value = "soultaker" },
                    new LineageNativeDataEntry { ID = 96, Value = "arcana_lord" },
                    new LineageNativeDataEntry { ID = 97, Value = "cardinal" },
                    new LineageNativeDataEntry { ID = 98, Value = "hierophant" },
                    new LineageNativeDataEntry { ID = 99, Value = "evas_templar" },
                    new LineageNativeDataEntry { ID = 100, Value = "sword_muse" },
                    new LineageNativeDataEntry { ID = 101, Value = "wind_rider" },
                    new LineageNativeDataEntry { ID = 102, Value = "moonlight_sentinel" },
                    new LineageNativeDataEntry { ID = 103, Value = "mystic_muse" },
                    new LineageNativeDataEntry { ID = 104, Value = "elemental_master" },
                    new LineageNativeDataEntry { ID = 105, Value = "evas_saint" },
                    new LineageNativeDataEntry { ID = 106, Value = "shillien_templar" },
                    new LineageNativeDataEntry { ID = 107, Value = "spectral_dancer" },
                    new LineageNativeDataEntry { ID = 108, Value = "ghost_hunter" },
                    new LineageNativeDataEntry { ID = 109, Value = "ghost_sentinel" },
                    new LineageNativeDataEntry { ID = 110, Value = "storm_screamer" },
                    new LineageNativeDataEntry { ID = 111, Value = "spectral_master" },
                    new LineageNativeDataEntry { ID = 112, Value = "shillien_saint" },
                    new LineageNativeDataEntry { ID = 113, Value = "titan" },
                    new LineageNativeDataEntry { ID = 114, Value = "grand_khavatari" },
                    new LineageNativeDataEntry { ID = 115, Value = "dominator" },
                    new LineageNativeDataEntry { ID = 116, Value = "doomcryer" },
                    new LineageNativeDataEntry { ID = 117, Value = "fortune_seeker" },
                    new LineageNativeDataEntry { ID = 118, Value = "maestro" },
                    new LineageNativeDataEntry { ID = 123, Value = "kamael_m_soldier" },
                    new LineageNativeDataEntry { ID = 124, Value = "kamael_f_soldier" },
                    new LineageNativeDataEntry { ID = 125, Value = "trooper" },
                    new LineageNativeDataEntry { ID = 126, Value = "warder" },
                    new LineageNativeDataEntry { ID = 127, Value = "berserker" },
                    new LineageNativeDataEntry { ID = 128, Value = "m_soul_breaker" },
                    new LineageNativeDataEntry { ID = 129, Value = "f_soul_breaker" },
                    new LineageNativeDataEntry { ID = 130, Value = "arbalester" },
                    new LineageNativeDataEntry { ID = 131, Value = "doombringer" },
                    new LineageNativeDataEntry { ID = 132, Value = "m_soul_hound" },
                    new LineageNativeDataEntry { ID = 133, Value = "f_soul_hound" },
                    new LineageNativeDataEntry { ID = 134, Value = "trickster" },
                    new LineageNativeDataEntry { ID = 135, Value = "inspector" },
                    new LineageNativeDataEntry { ID = 136, Value = "judicator" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateLogOn()
        {
            var nativeData = new LineageNativeData
            {
                Name = "LogOn",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "Slot_0" },
                    new LineageNativeDataEntry { ID = 1, Value = "Slot_1" },
                    new LineageNativeDataEntry { ID = 2, Value = "Slot_2" },
                    new LineageNativeDataEntry { ID = 3, Value = "Slot_3" },
                    new LineageNativeDataEntry { ID = 4, Value = "Slot_4" },
                    new LineageNativeDataEntry { ID = 5, Value = "Slot_5" },
                    new LineageNativeDataEntry { ID = 6, Value = "Slot_6" },
                    new LineageNativeDataEntry { ID = 7, Value = "Slot_center" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateCharCreate()
        {
            var nativeData = new LineageNativeData
            {
                Name = "CharCreate",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "Human_Knight_M" },
                    new LineageNativeDataEntry { ID = 1, Value = "Human_Knight_F" },
                    new LineageNativeDataEntry { ID = 2, Value = "Human_Mage_M" },
                    new LineageNativeDataEntry { ID = 3, Value = "Human_Mage_F" },
                    new LineageNativeDataEntry { ID = 4, Value = "Elf_Knight_M" },
                    new LineageNativeDataEntry { ID = 5, Value = "Elf_Knight_F" },
                    new LineageNativeDataEntry { ID = 6, Value = "Elf_Mage_M" },
                    new LineageNativeDataEntry { ID = 7, Value = "Elf_Mage_F" },
                    new LineageNativeDataEntry { ID = 8, Value = "DarkElf_Knight_M" },
                    new LineageNativeDataEntry { ID = 9, Value = "DarkElf_Knight_F" },
                    new LineageNativeDataEntry { ID = 10, Value = "DarkElf_Mage_M" },
                    new LineageNativeDataEntry { ID = 11, Value = "DarkElf_Mage_F" },
                    new LineageNativeDataEntry { ID = 12, Value = "Orc_Knight_M" },
                    new LineageNativeDataEntry { ID = 13, Value = "Orc_Knight_F" },
                    new LineageNativeDataEntry { ID = 14, Value = "Orc_Mage_M" },
                    new LineageNativeDataEntry { ID = 15, Value = "Orc_Mage_F" },
                    new LineageNativeDataEntry { ID = 16, Value = "Dwarf_Knight_M" },
                    new LineageNativeDataEntry { ID = 17, Value = "Dwarf_Knight_F" },
                    new LineageNativeDataEntry { ID = 18, Value = "Kamael_soldier_M" },
                    new LineageNativeDataEntry { ID = 19, Value = "Kamael_soldier_F" }
                }
            };
            return nativeData;
        }
        
        public static LineageNativeData CreateExGrpClass()
        {
            var nativeData = new LineageNativeData
            {
                Name = "ExGrpClass",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = -1, Value = "None" },
                    new LineageNativeDataEntry { ID = 1, Value = "FFighter" },
                    new LineageNativeDataEntry { ID = 0, Value = "MFighter" },
                    new LineageNativeDataEntry { ID = 9, Value = "FMagic" },
                    new LineageNativeDataEntry { ID = 8, Value = "MMagic" },
                    new LineageNativeDataEntry { ID = 7, Value = "FElf" },
                    new LineageNativeDataEntry { ID = 6, Value = "MElf" },
                    new LineageNativeDataEntry { ID = 3, Value = "FDarkElf" },
                    new LineageNativeDataEntry { ID = 2, Value = "MDarkElf" },
                    new LineageNativeDataEntry { ID = 11, Value = "FOrc" },
                    new LineageNativeDataEntry { ID = 10, Value = "MOrc" },
                    new LineageNativeDataEntry { ID = 13, Value = "FShaman" },
                    new LineageNativeDataEntry { ID = 12, Value = "MShaman" },
                    new LineageNativeDataEntry { ID = 5, Value = "FDwarf" },
                    new LineageNativeDataEntry { ID = 4, Value = "MDwarf" },
                    new LineageNativeDataEntry { ID = 15, Value = "FKamael" },
                    new LineageNativeDataEntry { ID = 14, Value = "MKamael" }
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateArmorType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "ArmorType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "light" },
                    new LineageNativeDataEntry { ID = 2, Value = "heavy" },
                    new LineageNativeDataEntry { ID = 3, Value = "magic" },
                    new LineageNativeDataEntry { ID = 4, Value = "sigil" }
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateConsumeType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "ConsumeType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "consume_type_normal" },
                    new LineageNativeDataEntry { ID = 1, Value = "consume_type_charge" },
                    new LineageNativeDataEntry { ID = 2, Value = "consume_type_stackable" },
                    new LineageNativeDataEntry { ID = 3, Value = "consume_type_asset" }
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateCrystalType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "CrystalType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "crystal_free" },
                    new LineageNativeDataEntry { ID = 4, Value = "a" },
                    new LineageNativeDataEntry { ID = 3, Value = "b" },
                    new LineageNativeDataEntry { ID = 2, Value = "c" },
                    new LineageNativeDataEntry { ID = 1, Value = "d" },
                    new LineageNativeDataEntry { ID = 5, Value = "s" },
                    new LineageNativeDataEntry { ID = 6, Value = "s80" },
                    new LineageNativeDataEntry { ID = 7, Value = "s84" },
                    new LineageNativeDataEntry { ID = 8, Value = "Event" },
                }
            };
            return nativeData;
        }
        
        public static LineageNativeData CreateMaterialType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "MaterialType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "oriharukon" },
                    new LineageNativeDataEntry { ID = 2, Value = "mithril" },
                    new LineageNativeDataEntry { ID = 3, Value = "gold" },
                    new LineageNativeDataEntry { ID = 4, Value = "silver" },
                    new LineageNativeDataEntry { ID = 5, Value = "brass" },
                    new LineageNativeDataEntry { ID = 6, Value = "bronze" },
                    new LineageNativeDataEntry { ID = 7, Value = "diamond" },
                    new LineageNativeDataEntry { ID = 8, Value = "steel" },
                    new LineageNativeDataEntry { ID = 9, Value = "damascus_steel" },
                    new LineageNativeDataEntry { ID = 10, Value = "bone_of_dragon" },
                    new LineageNativeDataEntry { ID = 11, Value = "air" },
                    new LineageNativeDataEntry { ID = 12, Value = "water" },
                    new LineageNativeDataEntry { ID = 13, Value = "wood" },
                    new LineageNativeDataEntry { ID = 14, Value = "bone" },
                    new LineageNativeDataEntry { ID = 15, Value = "branch_of_orbis_arbor" },
                    new LineageNativeDataEntry { ID = 16, Value = "branch_of_worldtree" },
                    new LineageNativeDataEntry { ID = 17, Value = "cloth" },
                    new LineageNativeDataEntry { ID = 18, Value = "paper" },
                    new LineageNativeDataEntry { ID = 19, Value = "leather" },
                    new LineageNativeDataEntry { ID = 20, Value = "skull" },
                    new LineageNativeDataEntry { ID = 21, Value = "feather" },
                    new LineageNativeDataEntry { ID = 22, Value = "sand" },
                    new LineageNativeDataEntry { ID = 23, Value = "crystal" },
                    new LineageNativeDataEntry { ID = 24, Value = "fur" },
                    new LineageNativeDataEntry { ID = 25, Value = "parchment" },
                    new LineageNativeDataEntry { ID = 26, Value = "eyeball" },
                    new LineageNativeDataEntry { ID = 27, Value = "insect" },
                    new LineageNativeDataEntry { ID = 28, Value = "corpse" },
                    new LineageNativeDataEntry { ID = 29, Value = "crow" },
                    new LineageNativeDataEntry { ID = 30, Value = "hatching_egg" },
                    new LineageNativeDataEntry { ID = 31, Value = "skull_of_wyvern" },
                    new LineageNativeDataEntry { ID = 32, Value = "drug" },
                    new LineageNativeDataEntry { ID = 33, Value = "cotton" },
                    new LineageNativeDataEntry { ID = 34, Value = "silk" },
                    new LineageNativeDataEntry { ID = 35, Value = "wool" },
                    new LineageNativeDataEntry { ID = 36, Value = "flax" },
                    new LineageNativeDataEntry { ID = 37, Value = "cobweb" },
                    new LineageNativeDataEntry { ID = 38, Value = "dyestuff" },
                    new LineageNativeDataEntry { ID = 39, Value = "leather_of_puma" },
                    new LineageNativeDataEntry { ID = 40, Value = "leather_of_lion" },
                    new LineageNativeDataEntry { ID = 41, Value = "leather_of_manticore" },
                    new LineageNativeDataEntry { ID = 42, Value = "leather_of_drake" },
                    new LineageNativeDataEntry { ID = 43, Value = "leather_of_salamander" },
                    new LineageNativeDataEntry { ID = 44, Value = "leather_of_unicorn" },
                    new LineageNativeDataEntry { ID = 45, Value = "leather_of_dragon" },
                    new LineageNativeDataEntry { ID = 46, Value = "scale_of_dragon" },
                    new LineageNativeDataEntry { ID = 47, Value = "adamantaite" },
                    new LineageNativeDataEntry { ID = 48, Value = "blood_steel" },
                    new LineageNativeDataEntry { ID = 49, Value = "chrysolite" },
                    new LineageNativeDataEntry { ID = 50, Value = "damascus" },
                    new LineageNativeDataEntry { ID = 51, Value = "fine_steel" },
                    new LineageNativeDataEntry { ID = 52, Value = "horn" },
                    new LineageNativeDataEntry { ID = 53, Value = "liquid" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateEtcItemType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "EtcItemType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "scroll" },
                    new LineageNativeDataEntry { ID = 2, Value = "arrow" },
                    new LineageNativeDataEntry { ID = 3, Value = "potion" },
                    new LineageNativeDataEntry { ID = 4, Value = "spellbook" },
                    new LineageNativeDataEntry { ID = 5, Value = "recipe" },
                    new LineageNativeDataEntry { ID = 6, Value = "material" },
                    new LineageNativeDataEntry { ID = 7, Value = "pet_collar" },
                    new LineageNativeDataEntry { ID = 8, Value = "castle_guard" },
                    new LineageNativeDataEntry { ID = 9, Value = "dye" },
                    new LineageNativeDataEntry { ID = 10, Value = "seed" },
                    new LineageNativeDataEntry { ID = 11, Value = "seed2" },
                    new LineageNativeDataEntry { ID = 12, Value = "harvest" },
                    new LineageNativeDataEntry { ID = 13, Value = "lotto" },
                    new LineageNativeDataEntry { ID = 14, Value = "race_ticket" },
                    new LineageNativeDataEntry { ID = 15, Value = "ticket_of_lord" },
                    new LineageNativeDataEntry { ID = 16, Value = "lure" },
                    new LineageNativeDataEntry { ID = 35, Value = "teleportbookmark" },
                    new LineageNativeDataEntry { ID = 26, Value = "bolt" },
                    new LineageNativeDataEntry { ID = 17, Value = "crop" },
                    new LineageNativeDataEntry { ID = 18, Value = "maturecrop" },
                    new LineageNativeDataEntry { ID = 20, Value = "scrl_enchant_am" },
                    new LineageNativeDataEntry { ID = 19, Value = "scrl_enchant_wp" },
                    new LineageNativeDataEntry { ID = 22, Value = "bless_scrl_enchant_am" },
                    new LineageNativeDataEntry { ID = 21, Value = "bless_scrl_enchant_wp" },
                    new LineageNativeDataEntry { ID = 23, Value = "coupon" },
                    new LineageNativeDataEntry { ID = 24, Value = "elixir" },
                    new LineageNativeDataEntry { ID = 25, Value = "scrl_enchant_attr" },
                    new LineageNativeDataEntry { ID = 28, Value = "scrl_inc_enchant_prop_am" },
                    new LineageNativeDataEntry { ID = 27, Value = "scrl_inc_enchant_prop_wp" },
                    new LineageNativeDataEntry { ID = 29, Value = "crystal_enchant_am" },
                    new LineageNativeDataEntry { ID = 30, Value = "crystal_enchant_wp" },
                    new LineageNativeDataEntry { ID = 31, Value = "ancient_crystal_enchant_am" },
                    new LineageNativeDataEntry { ID = 32, Value = "ancient_crystal_enchant_wp" },
                    new LineageNativeDataEntry { ID = 33, Value = "rune" },
                    new LineageNativeDataEntry { ID = 34, Value = "rune_select" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateSlotIntType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "SlotIntType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "underwear" },
                    new LineageNativeDataEntry { ID = 1, Value = "rear" },
                    new LineageNativeDataEntry { ID = 2, Value = "lear" },
                    new LineageNativeDataEntry { ID = 3, Value = "neck" },
                    new LineageNativeDataEntry { ID = 4, Value = "rfinger" },
                    new LineageNativeDataEntry { ID = 5, Value = "lfinger" },
                    new LineageNativeDataEntry { ID = 6, Value = "head" },
                    new LineageNativeDataEntry { ID = 7, Value = "lrhand" },
                    new LineageNativeDataEntry { ID = 7, Value = "rlhand" },
                    new LineageNativeDataEntry { ID = 8, Value = "onepiece" },
                    new LineageNativeDataEntry { ID = 9, Value = "alldress" },
                    new LineageNativeDataEntry { ID = 10, Value = "hairall" },
                    new LineageNativeDataEntry { ID = 11, Value = "rbracelet" },
                    new LineageNativeDataEntry { ID = 12, Value = "lbracelet" },
                    new LineageNativeDataEntry { ID = 13, Value = "deco1" },
                    new LineageNativeDataEntry { ID = 19, Value = "waist" },
                    new LineageNativeDataEntry { ID = 20, Value = "gloves" },
                    new LineageNativeDataEntry { ID = 21, Value = "chest" },
                    new LineageNativeDataEntry { ID = 22, Value = "legs" },
                    new LineageNativeDataEntry { ID = 23, Value = "feet" },
                    new LineageNativeDataEntry { ID = 24, Value = "back" },
                    new LineageNativeDataEntry { ID = 25, Value = "hair" },
                    new LineageNativeDataEntry { ID = 26, Value = "hair2" },
                    new LineageNativeDataEntry { ID = 27, Value = "rhand" },
                    new LineageNativeDataEntry { ID = 28, Value = "lhand" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateSlotIntTypeErtheia()
        {
            var nativeData = new LineageNativeData
            {
                Name = "SlotIntTypeErtheia",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = -1, Value = "none" },
                    new LineageNativeDataEntry { ID = 0, Value = "underwear" },
                    new LineageNativeDataEntry { ID = 1, Value = "rear" },
                    new LineageNativeDataEntry { ID = 2, Value = "lear" },
                    new LineageNativeDataEntry { ID = 3, Value = "neck" },
                    new LineageNativeDataEntry { ID = 4, Value = "rfinger" },
                    new LineageNativeDataEntry { ID = 5, Value = "lfinger" },
                    new LineageNativeDataEntry { ID = 6, Value = "head" },
                    new LineageNativeDataEntry { ID = 7, Value = "lrhand" },
                    new LineageNativeDataEntry { ID = 7, Value = "rlhand" },
                    new LineageNativeDataEntry { ID = 8, Value = "onepiece" },
                    new LineageNativeDataEntry { ID = 9, Value = "alldress" },
                    new LineageNativeDataEntry { ID = 10, Value = "hairall" },
                    new LineageNativeDataEntry { ID = 11, Value = "rbracelet" },
                    new LineageNativeDataEntry { ID = 12, Value = "lbracelet" },
                    new LineageNativeDataEntry { ID = 13, Value = "deco1" },
                    new LineageNativeDataEntry { ID = 14, Value = "deco2" },
                    new LineageNativeDataEntry { ID = 15, Value = "deco3" },
                    new LineageNativeDataEntry { ID = 16, Value = "deco4" },
                    new LineageNativeDataEntry { ID = 17, Value = "deco5" },
                    new LineageNativeDataEntry { ID = 18, Value = "deco6" },
                    new LineageNativeDataEntry { ID = 19, Value = "waist" },
                    new LineageNativeDataEntry { ID = 20, Value = "brooch" },
                    new LineageNativeDataEntry { ID = 21, Value = "jewel1" },
                    new LineageNativeDataEntry { ID = 22, Value = "jewel2" },
                    new LineageNativeDataEntry { ID = 23, Value = "jewel3" },
                    new LineageNativeDataEntry { ID = 24, Value = "jewel4" },
                    new LineageNativeDataEntry { ID = 25, Value = "jewel5" },
                    new LineageNativeDataEntry { ID = 26, Value = "jewel6" },
                    new LineageNativeDataEntry { ID = 27, Value = "gloves" },
                    new LineageNativeDataEntry { ID = 28, Value = "chest" },
                    new LineageNativeDataEntry { ID = 29, Value = "legs" },
                    new LineageNativeDataEntry { ID = 30, Value = "feet" },
                    new LineageNativeDataEntry { ID = 31, Value = "back" },
                    new LineageNativeDataEntry { ID = 32, Value = "hair" },
                    new LineageNativeDataEntry { ID = 33, Value = "hair2" },
                    new LineageNativeDataEntry { ID = 34, Value = "rhand" },
                    new LineageNativeDataEntry { ID = 35, Value = "lhand" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateWeaponType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "WeaponType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "sword" },
                    new LineageNativeDataEntry { ID = 2, Value = "blunt" },
                    new LineageNativeDataEntry { ID = 3, Value = "dagger" },
                    new LineageNativeDataEntry { ID = 4, Value = "pole" },
                    new LineageNativeDataEntry { ID = 5, Value = "dualfist" },
                    new LineageNativeDataEntry { ID = 6, Value = "bow" },
                    new LineageNativeDataEntry { ID = 7, Value = "etc" },
                    new LineageNativeDataEntry { ID = 8, Value = "dual" },
                    new LineageNativeDataEntry { ID = 10, Value = "fishingrod" },
                    new LineageNativeDataEntry { ID = 11, Value = "rapier" },
                    new LineageNativeDataEntry { ID = 12, Value = "crossbow" },
                    new LineageNativeDataEntry { ID = 13, Value = "ancientsword" },
                    new LineageNativeDataEntry { ID = 14, Value = "flag" },
                    new LineageNativeDataEntry { ID = 15, Value = "dualdagger" },
                    new LineageNativeDataEntry { ID = 16, Value = "ownthing" },
                    new LineageNativeDataEntry { ID = 17, Value = "twohandcrossbow" },
                    new LineageNativeDataEntry { ID = 18, Value = "dualblunt" },
                    new LineageNativeDataEntry { ID = 19, Value = "twohandsword" },
                    new LineageNativeDataEntry { ID = 20, Value = "twohandblunt" },
                    new LineageNativeDataEntry { ID = 21, Value = "mage_sword" },
                    new LineageNativeDataEntry { ID = 22, Value = "mage_blunt" },
                    new LineageNativeDataEntry { ID = 23, Value = "mage_twohandsword" },
                    new LineageNativeDataEntry { ID = 24, Value = "mage_twohandblunt" },
                    new LineageNativeDataEntry { ID = 25, Value = "shield" },
                    new LineageNativeDataEntry { ID = 26, Value = "sigil" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateActionCategory2()
        {
            var nativeData = new LineageNativeData
            {
                Name = "ActionCategory2",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = -1, Value = "riding" },
                    new LineageNativeDataEntry { ID = -2, Value = "common" },
                    new LineageNativeDataEntry { ID = -3, Value = "airship" },
                    new LineageNativeDataEntry { ID = -4, Value = "summon_org_common" },
                    new LineageNativeDataEntry { ID = -5, Value = "MAX" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateActionCategory()
        {
            var nativeData = new LineageNativeData
            {
                Name = "ActionCategory",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "basic" },
                    new LineageNativeDataEntry { ID = 2, Value = "party" },
                    new LineageNativeDataEntry { ID = 3, Value = "social" },
                    new LineageNativeDataEntry { ID = 4, Value = "pet" },
                    new LineageNativeDataEntry { ID = 5, Value = "summon" },
                    new LineageNativeDataEntry { ID = 6, Value = "MAX" }
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateNpcNameColor()
        {
            var nativeData = new LineageNativeData
            {
                Name = "NpcNameColor",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0xFFA9E89C, Value = "Default" },
                    new LineageNativeDataEntry { ID = 0xFFFF8000, Value = "quest" },
                    new LineageNativeDataEntry { ID = 0xFFEF1431, Value = "bloody" },
                    new LineageNativeDataEntry { ID = 0xFFFE8B3F, Value = "raid" } //,
                    //mantle = 0xFFF0
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateMantleException()
        {
            var nativeData = new LineageNativeData
            {
                Name = "MantleException",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "mfighter_texture" },
                    new LineageNativeDataEntry { ID = 1, Value = "ffighter_texture" },
                    new LineageNativeDataEntry { ID = 8, Value = "mmagic_texture" },
                    new LineageNativeDataEntry { ID = 9, Value = "fmagic_texture" },
                    new LineageNativeDataEntry { ID = 6, Value = "melf_texture" },
                    new LineageNativeDataEntry { ID = 7, Value = "felf_texture" },
                    new LineageNativeDataEntry { ID = 2, Value = "mdarkelf_texture" },
                    new LineageNativeDataEntry { ID = 3, Value = "fdarkelf_texture" },
                    new LineageNativeDataEntry { ID = 4, Value = "mdwarf_texture" },
                    new LineageNativeDataEntry { ID = 5, Value = "fdwarf_texture" },
                    new LineageNativeDataEntry { ID = 10, Value = "morc_texture" },
                    new LineageNativeDataEntry { ID = 11, Value = "forc_texture" },
                    new LineageNativeDataEntry { ID = 12, Value = "mshaman_texture" },
                    new LineageNativeDataEntry { ID = 13, Value = "fshaman_texture" },
                    new LineageNativeDataEntry { ID = 14, Value = "mkamael_texture" },
                    new LineageNativeDataEntry { ID = 15, Value = "fkamael_texture" },
                    new LineageNativeDataEntry { ID = 16, Value = "MAX" }
                }
            };
            return nativeData;
        }
//what da hell i'm doing???

        public static LineageNativeData CreateArmorTypeErtheia()
        {
            var nativeData = new LineageNativeData
            {
                Name = "ArmorTypeErtheia",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "gloves" },
                    new LineageNativeDataEntry { ID = 2, Value = "chest" },
                    new LineageNativeDataEntry { ID = 3, Value = "legs" },
                    new LineageNativeDataEntry { ID = 4, Value = "feet" },
                    new LineageNativeDataEntry { ID = 5, Value = "onepiece" },
                    new LineageNativeDataEntry { ID = 6, Value = "alldress" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateMagicType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "MagicType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "Default" },
                    new LineageNativeDataEntry { ID = 1, Value = "earth" },
                    new LineageNativeDataEntry { ID = 2, Value = "wind" },
                    new LineageNativeDataEntry { ID = 3, Value = "water" },
                    new LineageNativeDataEntry { ID = 4, Value = "fire" },
                    new LineageNativeDataEntry { ID = 5, Value = "applied" },
                    new LineageNativeDataEntry { ID = 6, Value = "alter" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateNpcType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "NpcType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = -1, Value = "hide" },
                    new LineageNativeDataEntry { ID = 0, Value = "citizen_normal" },
                    new LineageNativeDataEntry { ID = 1, Value = "citizen_gatekeeper" },
                    new LineageNativeDataEntry { ID = 2, Value = "citizen_teleport_device" },
                    new LineageNativeDataEntry { ID = 3, Value = "citizen_seed_teleport_device" },
                    new LineageNativeDataEntry { ID = 4, Value = "citizen_blessing_benefactor" },
                    new LineageNativeDataEntry { ID = 5, Value = "citizen_trade_broker" },
                    new LineageNativeDataEntry { ID = 6, Value = "citizen_vitamin_manager" },
                    new LineageNativeDataEntry { ID = 7, Value = "citizen_beauty_manager" },
                    new LineageNativeDataEntry { ID = 8, Value = "citizen_guard_captain" },
                    new LineageNativeDataEntry { ID = 9, Value = "citizen_warehouse_keeper" },
                    new LineageNativeDataEntry { ID = 1000, Value = "monster_normal" }
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateAlterSkillType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "AlterSkillType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "target" },
                    new LineageNativeDataEntry { ID = 2, Value = "self" },
                    new LineageNativeDataEntry { ID = 3, Value = "auto" }
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateStatType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "StatType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "myhp" },
                    new LineageNativeDataEntry { ID = 2, Value = "mymp" },
                    new LineageNativeDataEntry { ID = 3, Value = "mycp" },
                    new LineageNativeDataEntry { ID = 4, Value = "targethp" },
                    new LineageNativeDataEntry { ID = 5, Value = "targetmp" },
                    new LineageNativeDataEntry { ID = 6, Value = "targetcp" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateEquipType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "EquipType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "shield" },
                    new LineageNativeDataEntry { ID = 2, Value = "weapon" }
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateWeaponSlotType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "WeaponSlotType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "shield" },
                    new LineageNativeDataEntry { ID = 2, Value = "weapon" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateCasterTargetEffect()
        {
            var nativeData = new LineageNativeData
            {
                Name = "CasterTargetEffect",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "caster_soul" },
                    new LineageNativeDataEntry { ID = 2, Value = "caster_water" },
                    new LineageNativeDataEntry { ID = 3, Value = "caster_wind" },
                    new LineageNativeDataEntry { ID = 4, Value = "caster_knight" },
                    new LineageNativeDataEntry { ID = 5, Value = "target_fire" },
                    new LineageNativeDataEntry { ID = 6, Value = "target_bleeding" },
                    new LineageNativeDataEntry { ID = 7, Value = "target_death_mark" },
                    new LineageNativeDataEntry { ID = 8, Value = "target_life_force_kamael" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateStatTypeBit1()
        {
            var nativeData = new LineageNativeData
            {
                Name = "StatTypeBit1",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 1, Value = "pdefense" },
                    new LineageNativeDataEntry { ID = 2, Value = "mdefense" },
                    new LineageNativeDataEntry { ID = 4, Value = "pattack" },
                    new LineageNativeDataEntry { ID = 8, Value = "mattack" },
                    new LineageNativeDataEntry { ID = 16, Value = "pattackspeed" },
                    new LineageNativeDataEntry { ID = 32, Value = "mattackspeed" },
                    new LineageNativeDataEntry { ID = 64, Value = "phit" },
                    new LineageNativeDataEntry { ID = 128, Value = "mhit" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateStatTypeBit2()
        {
            var nativeData = new LineageNativeData
            {
                Name = "StatTypeBit2",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 1, Value = "pcritical" },
                    new LineageNativeDataEntry { ID = 2, Value = "mcritical" },
                    new LineageNativeDataEntry { ID = 4, Value = "speed" },
                    new LineageNativeDataEntry { ID = 8, Value = "shiedldefense" },
                    new LineageNativeDataEntry { ID = 16, Value = "shiedldefenserate" },
                    new LineageNativeDataEntry { ID = 32, Value = "pavoid" },
                    new LineageNativeDataEntry { ID = 64, Value = "mavoid" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateItemInventoryType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "ItemInventoryType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "equipment" },
                    new LineageNativeDataEntry { ID = 2, Value = "consumable" },
                    new LineageNativeDataEntry { ID = 3, Value = "material" },
                    new LineageNativeDataEntry { ID = 4, Value = "etc" },
                    new LineageNativeDataEntry { ID = 5, Value = "quest" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateSystemMsgType()
        {
            var nativeData = new LineageNativeData
            {
                Name = "SystemMsgType",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "battle" },
                    new LineageNativeDataEntry { ID = 2, Value = "server" },
                    new LineageNativeDataEntry { ID = 3, Value = "damage" },
                    new LineageNativeDataEntry { ID = 4, Value = "popup" },
                    new LineageNativeDataEntry { ID = 5, Value = "error" },
                    new LineageNativeDataEntry { ID = 6, Value = "petition" },
                    new LineageNativeDataEntry { ID = 7, Value = "useitems" },
                    new LineageNativeDataEntry { ID = 8, Value = "popupwithmsg" },
                    new LineageNativeDataEntry { ID = 9, Value = "clientdebugmsg" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateBodyPartEx()
        {
            var nativeData = new LineageNativeData
            {
                Name = "BodyPartEx",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = -1, Value = "none" },
                    new LineageNativeDataEntry { ID = 27, Value = "gloves" },
                    new LineageNativeDataEntry { ID = 28, Value = "chest" },
                    new LineageNativeDataEntry { ID = 29, Value = "legs" },
                    new LineageNativeDataEntry { ID = 30, Value = "feet" },
                }
            };
            return nativeData;
        }
        public static LineageNativeData CreateCommandName()
        {
            var nativeData = new LineageNativeData
            {
                Name = "CommandName",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "loc" },
                    new LineageNativeDataEntry { ID = 1, Value = "petition" },
                    new LineageNativeDataEntry { ID = 2, Value = "petitioncancel" },
                    new LineageNativeDataEntry { ID = 3, Value = "gmlist" },
                    new LineageNativeDataEntry { ID = 4, Value = "sit" },
                    new LineageNativeDataEntry { ID = 5, Value = "stand" },
                    new LineageNativeDataEntry { ID = 6, Value = "walk" },
                    new LineageNativeDataEntry { ID = 7, Value = "run" },
                    new LineageNativeDataEntry { ID = 8, Value = "attack" },
                    new LineageNativeDataEntry { ID = 9, Value = "attackforce" },
                    new LineageNativeDataEntry { ID = 10, Value = "attackstand" },
                    new LineageNativeDataEntry { ID = 11, Value = "trade" },
                    new LineageNativeDataEntry { ID = 12, Value = "target" },
                    new LineageNativeDataEntry { ID = 13, Value = "targetnext" },
                    new LineageNativeDataEntry { ID = 14, Value = "pickup" },
                    new LineageNativeDataEntry { ID = 15, Value = "assist" },
                    new LineageNativeDataEntry { ID = 16, Value = "partyinvite" },
                    new LineageNativeDataEntry { ID = 17, Value = "partyleave" },
                    new LineageNativeDataEntry { ID = 18, Value = "partydismiss" },
                    new LineageNativeDataEntry { ID = 19, Value = "vendor" },
                    new LineageNativeDataEntry { ID = 20, Value = "partymatching" },
                    new LineageNativeDataEntry { ID = 21, Value = "socialhello" },
                    new LineageNativeDataEntry { ID = 22, Value = "socialvictory" },
                    new LineageNativeDataEntry { ID = 23, Value = "socialcharge" },
                    new LineageNativeDataEntry { ID = 24, Value = "useskill" },
                    new LineageNativeDataEntry { ID = 25, Value = "useskillforce" },
                    new LineageNativeDataEntry { ID = 26, Value = "useskillstand" },
                    new LineageNativeDataEntry { ID = 27, Value = "useshortcut" },
                    new LineageNativeDataEntry { ID = 28, Value = "useshortcutforce" },
                    new LineageNativeDataEntry { ID = 29, Value = "useshortcutstand" },
                    new LineageNativeDataEntry { ID = 30, Value = "allyinvite" },
                    new LineageNativeDataEntry { ID = 31, Value = "allyleave" },
                    new LineageNativeDataEntry { ID = 32, Value = "allyoust" },
                    new LineageNativeDataEntry { ID = 33, Value = "allydismiss" },
                    new LineageNativeDataEntry { ID = 34, Value = "allycrest" },
                    new LineageNativeDataEntry { ID = 35, Value = "allyInfo" },
                    new LineageNativeDataEntry { ID = 36, Value = "friendinvite" },
                    new LineageNativeDataEntry { ID = 37, Value = "friendlist" },
                    new LineageNativeDataEntry { ID = 38, Value = "frienddel" },
                    new LineageNativeDataEntry { ID = 39, Value = "buy" },
                    new LineageNativeDataEntry { ID = 40, Value = "allywarstart" },
                    new LineageNativeDataEntry { ID = 41, Value = "allywarstop" },
                    new LineageNativeDataEntry { ID = 42, Value = "allywarsurrender" },
                    new LineageNativeDataEntry { ID = 43, Value = "blocklist" },
                    new LineageNativeDataEntry { ID = 44, Value = "block" },
                    new LineageNativeDataEntry { ID = 45, Value = "unblock" },
                    new LineageNativeDataEntry { ID = 46, Value = "socialno" },
                    new LineageNativeDataEntry { ID = 47, Value = "socialyes" },
                    new LineageNativeDataEntry { ID = 48, Value = "socialbow" },
                    new LineageNativeDataEntry { ID = 49, Value = "socialunaware" },
                    new LineageNativeDataEntry { ID = 50, Value = "socialwaitinga" },
                    new LineageNativeDataEntry { ID = 51, Value = "sociallaugh" },
                    new LineageNativeDataEntry { ID = 52, Value = "escape" },
                    new LineageNativeDataEntry { ID = 53, Value = "remaintime" },
                    new LineageNativeDataEntry { ID = 54, Value = "socialapplause" },
                    new LineageNativeDataEntry { ID = 55, Value = "socialdance" },
                    new LineageNativeDataEntry { ID = 56, Value = "socialsad" },
                    new LineageNativeDataEntry { ID = 139, Value = "socialpropose" },
                    new LineageNativeDataEntry { ID = 140, Value = "socialprovocation" },
                    new LineageNativeDataEntry { ID = 57, Value = "sitstand" },
                    new LineageNativeDataEntry { ID = 58, Value = "walkrun" },
                    new LineageNativeDataEntry { ID = 59, Value = "manufacture" },
                    new LineageNativeDataEntry { ID = 60, Value = "mountdismount" },
                    new LineageNativeDataEntry { ID = 61, Value = "mount" },
                    new LineageNativeDataEntry { ID = 62, Value = "dismount" },
                    new LineageNativeDataEntry { ID = 63, Value = "pethold" },
                    new LineageNativeDataEntry { ID = 64, Value = "petattack" },
                    new LineageNativeDataEntry { ID = 65, Value = "petstop" },
                    new LineageNativeDataEntry { ID = 66, Value = "petpickup" },
                    new LineageNativeDataEntry { ID = 67, Value = "petrevert" },
                    new LineageNativeDataEntry { ID = 68, Value = "petspecial" },
                    new LineageNativeDataEntry { ID = 69, Value = "summonhold" },
                    new LineageNativeDataEntry { ID = 70, Value = "summonattack" },
                    new LineageNativeDataEntry { ID = 71, Value = "summonstop" },
                    new LineageNativeDataEntry { ID = 72, Value = "summonsiege" },
                    new LineageNativeDataEntry { ID = 73, Value = "skill4036" },
                    new LineageNativeDataEntry { ID = 74, Value = "skill4138" },
                    new LineageNativeDataEntry { ID = 75, Value = "commend" },
                    new LineageNativeDataEntry { ID = 76, Value = "wait" },
                    new LineageNativeDataEntry { ID = 77, Value = "time" },
                    new LineageNativeDataEntry { ID = 78, Value = "allblock" },
                    new LineageNativeDataEntry { ID = 79, Value = "allunblock" },
                    new LineageNativeDataEntry { ID = 80, Value = "equip" },
                    new LineageNativeDataEntry { ID = 81, Value = "partyinfo" },
                    new LineageNativeDataEntry { ID = 82, Value = "pledgewarinfo" },
                    new LineageNativeDataEntry { ID = 83, Value = "allywarinfo" },
                    new LineageNativeDataEntry { ID = 84, Value = "storefind" },
                    new LineageNativeDataEntry { ID = 85, Value = "leaderchange" },
                    new LineageNativeDataEntry { ID = 86, Value = "warattack" },
                    new LineageNativeDataEntry { ID = 87, Value = "warstop" },
                    new LineageNativeDataEntry { ID = 88, Value = "attacklist" },
                    new LineageNativeDataEntry { ID = 89, Value = "underattacklist" },
                    new LineageNativeDataEntry { ID = 90, Value = "warlist" },
                    new LineageNativeDataEntry { ID = 91, Value = "allycrestdel" },
                    new LineageNativeDataEntry { ID = 92, Value = "openunion" },
                    new LineageNativeDataEntry { ID = 93, Value = "closeunion" },
                    new LineageNativeDataEntry { ID = 94, Value = "inviteunion" },
                    new LineageNativeDataEntry { ID = 95, Value = "banunion" },
                    new LineageNativeDataEntry { ID = 96, Value = "outunion" },
                    new LineageNativeDataEntry { ID = 97, Value = "unioninfo" },
                    new LineageNativeDataEntry { ID = 98, Value = "selfnickname" },
                    new LineageNativeDataEntry { ID = 99, Value = "warstatus" },
                    new LineageNativeDataEntry { ID = 100, Value = "pledgepenalty" },
                    new LineageNativeDataEntry { ID = 101, Value = "manufacture2" },
                    new LineageNativeDataEntry { ID = 102, Value = "chinatime" },
                    new LineageNativeDataEntry { ID = 103, Value = "recstart" },
                    new LineageNativeDataEntry { ID = 104, Value = "recstop" },
                    new LineageNativeDataEntry { ID = 105, Value = "recstartstop" },
                    new LineageNativeDataEntry { ID = 106, Value = "petmove" },
                    new LineageNativeDataEntry { ID = 107, Value = "summonmove" },
                    new LineageNativeDataEntry { ID = 108, Value = "unsummon" },
                    new LineageNativeDataEntry { ID = 109, Value = "olympiadstat" },
                    new LineageNativeDataEntry { ID = 110, Value = "graduatelist" },
                    new LineageNativeDataEntry { ID = 111, Value = "challenge" },
                    new LineageNativeDataEntry { ID = 112, Value = "cancelchallenge" },
                    new LineageNativeDataEntry { ID = 113, Value = "partychallenge" },
                    new LineageNativeDataEntry { ID = 114, Value = "instantzone" },
                    new LineageNativeDataEntry { ID = 115, Value = "packagevendor" },
                    new LineageNativeDataEntry { ID = 116, Value = "socialcharm" },
                    new LineageNativeDataEntry { ID = 117, Value = "resetnickname" },
                    new LineageNativeDataEntry { ID = 118, Value = "playminigame" },
                    new LineageNativeDataEntry { ID = 119, Value = "teleportbookmark" },
                    new LineageNativeDataEntry { ID = 120, Value = "botreport" },
                    new LineageNativeDataEntry { ID = 121, Value = "socialshy" },
                    new LineageNativeDataEntry { ID = 122, Value = "navigatewheel" },
                    new LineageNativeDataEntry { ID = 123, Value = "leavewheel" },
                    new LineageNativeDataEntry { ID = 124, Value = "startship" },
                    new LineageNativeDataEntry { ID = 125, Value = "leaveship" },
                    new LineageNativeDataEntry { ID = 126, Value = "mybirthday" },
                    new LineageNativeDataEntry { ID = 127, Value = "PostWnd" },
                    new LineageNativeDataEntry { ID = 128, Value = "couplebow" },
                    new LineageNativeDataEntry { ID = 129, Value = "couplehighfive" },
                    new LineageNativeDataEntry { ID = 130, Value = "coupledance" },
                    new LineageNativeDataEntry { ID = 131, Value = "addreceiver" },
                    new LineageNativeDataEntry { ID = 1001, Value = "initial" },
                    new LineageNativeDataEntry { ID = 1002, Value = "eva" },
                    new LineageNativeDataEntry { ID = 1003, Value = "mark" },
                }
            };
            return nativeData;
        }

        public static LineageNativeData CreateSoundSource()
        {
            var nativeData = new LineageNativeData
            {
                Name = "SoundSource",
                Definitions =
                {
                    new LineageNativeDataEntry { ID = 0, Value = "none" },
                    new LineageNativeDataEntry { ID = 1, Value = "host" },
                    new LineageNativeDataEntry { ID = 2, Value = "target" },
                }
            };
            return nativeData;
        }


        public static LineageNative CreateLineageNativeData()
        {
            var nativeDatas = new LineageNative();
            nativeDatas.AddDataFile(CreateRace());
            nativeDatas.AddDataFile(CreateActionCategory());
            nativeDatas.AddDataFile(CreateActionCategory2());
            nativeDatas.AddDataFile(CreateArmorType());
            nativeDatas.AddDataFile(CreateWeaponSlotType());
            nativeDatas.AddDataFile(CreateWeaponType());
            nativeDatas.AddDataFile(CreateSoundSource());
            nativeDatas.AddDataFile(CreateCharCreate());
            nativeDatas.AddDataFile(CreateMaterialType());
            nativeDatas.AddDataFile(CreateMagicType());
            nativeDatas.AddDataFile(CreateSlotIntType());
            nativeDatas.AddDataFile(CreateCrystalType());
            nativeDatas.AddDataFile(CreateClass());
            nativeDatas.AddDataFile(CreateConsumeType());
            nativeDatas.AddDataFile(CreateEtcItemType());
            nativeDatas.AddDataFile(CreateCommandName());


            return nativeDatas;
        }
    }
}