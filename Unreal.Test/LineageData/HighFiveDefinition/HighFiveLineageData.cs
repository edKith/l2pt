﻿using Unreal.Engine.LineageData;
using Unreal.Engine.LineageData.Definition;
using Unreal.Engine.LineageData.Definition.BinaryField;
using Unreal.Engine.LineageData.Parsers;

namespace Unreal.Test.Engine.LineageData.HighFiveDefinition
{
    public class HighFiveLineageData
    {
        public static LineageDataFile SysString()
        {
            var dataFile = new LineageDataFileDefinition
            {
                Name = "SysString",
                Localizable = true
            };
            dataFile.Text.Definition.Add(new TextField { Name = "string_begin", Type = DataFieldType.DataBegin });
            dataFile.Text.Definition.Add(new TextField { Index = true, Name = "id", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "string", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "string_end", Type = DataFieldType.DataEnd });
            return dataFile;
        }
        public static LineageDataFile ActionName()
        {
            var dataFile = new LineageDataFileDefinition
            {
                Name = "ActionName",
                Localizable = true
            };

            dataFile.Text.Definition.Add(new TextField { Name = "action_begin", Type = DataFieldType.DataBegin });
            dataFile.Text.Definition.Add(new TextField { Index = true, Name = "id", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "type", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "category", Type = DataFieldType.Integer, DataSource = "Native.ActionCategory" });
            dataFile.Text.Definition.Add(new TextField { Name = "category2", Type = DataFieldType.ComplexArray, DataSource = "Native.ActionCategory2" });
            dataFile.Text.Definition.Add(new TextField { Name = "cmd", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "icon", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "name", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "desc", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "action_end", Type = DataFieldType.DataEnd });

            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "tag" });
            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "id" });
            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "type" });
            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "category", DataSource = "Native.ActionCategory" });

            //actionName.Binary.Definition.Add(new NumericArrayBinaryField() { Name = "category2", LengthFormat = ArrayLengthFormat.Compact});
            dataFile.Binary.Definition.Add(new ComplexBinaryField
            {
                Name = "category2",
                Value = new BinaryDataHolder
                {
                    LengthFormat = ArrayLengthFormat.Compact,
                    Pattern = new BinaryDataDefinition
                    {
                        new IntegerBinaryField { Name = "category2_inner",
                            DataSource = "Native.ActionCategory2", }
                    }
                }
            });
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "name" });
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "icon" });
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "desc" });
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "cmd", LengthFormat = ArrayLengthFormat.Integer });

            return dataFile;
        }
        public static LineageDataFile AdditionalEffect()
        {
            var dataFile = new LineageDataFileDefinition

            {
                Name = "AdditionalEffect",
                Localizable = false
            };
            dataFile.Text.Definition.Add(new TextField { Name = "add_effect_begin", Type = DataFieldType.DataBegin });
            dataFile.Text.Definition.Add(new TextField { Index = true, Name = "id", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "eff_name", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "add_effect_end", Type = DataFieldType.DataEnd });

            return dataFile;
        }
        public static LineageDataFile AdditionalItemGrp()
        {
            var dataFile = new LineageDataFileDefinition

            {
                Name = "AdditionalItemGrp",
                Localizable = false
            };
            dataFile.Text.Definition.Add(new TextField { Name = "item_begin", Type = DataFieldType.DataBegin });
            dataFile.Text.Definition.Add(new TextField { Index = true, Name = "id", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "has_ani", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "include_item", Type = DataFieldType.NumericArray });
            dataFile.Text.Definition.Add(new TextField { Name = "max_energy", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "item_end", Type = DataFieldType.DataEnd });


            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "id" });
            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "tag" });
            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "has_ani" });
            dataFile.Binary.Definition.Add(new NumericArrayBinaryField { Name = "include_item", LengthFormat = ArrayLengthFormat.Constant, DataLength = 10 });
            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "max_energy" });

            return dataFile;
        }
        public static LineageDataFile CastleName()
        {
            var dataFile = new LineageDataFileDefinition

            {
                Name = "CastleName",
                Localizable = true
            };
            dataFile.Text.Definition.Add(new TextField { Name = "castle_name_begin", Type = DataFieldType.DataBegin });
            dataFile.Text.Definition.Add(new TextField { Index = true, Name = "id", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "name", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "loc", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "desc", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "mark", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "markgray", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "flagicon", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "mercname", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "castle_name_end", Type = DataFieldType.DataEnd });


            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "number" });
            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "tag" });
            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "id" });
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "name" });
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "loc" });
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "desc" });
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "mark" });
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "markgray" });
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "flagicon" });
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "mercname" });

            return dataFile;
        }
        public static LineageDataFile ClassInfo()
        {
            var dataFile = new LineageDataFileDefinition
            {
                Name = "ClassInfo",
                Localizable = true
            };
            dataFile.Text.Definition.Add(new TextField { Name = "text_begin", Type = DataFieldType.DataBegin });
            dataFile.Text.Definition.Add(new TextField { Index = true, Name = "id", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "text", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "text_end", Type = DataFieldType.DataEnd });
            return dataFile;
        }
        public static LineageDataFile CommandName()
        {
            var dataFile = new LineageDataFileDefinition
            {
                Name = "CommandName",
                Localizable = true
            };
            dataFile.Text.Definition.Add(new TextField { Name = "cmd_begin", Type = DataFieldType.DataBegin });
            dataFile.Text.Definition.Add(new TextField { Index = true, Name = "id", Type = DataFieldType.Integer});
            dataFile.Text.Definition.Add(new TextField { Name = "action", Type = DataFieldType.Integer, DataSource = "Native.CommandName" });
            dataFile.Text.Definition.Add(new TextField { Name = "cmd", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "cmd_end", Type = DataFieldType.DataEnd });

            return dataFile;
        }
        public static LineageDataFile CreditGrp()
        {
            var dataFile = new LineageDataFileDefinition
            {
                Name = "CreditGrp",
                Localizable = true
            };
            dataFile.Text.Definition.Add(new TextField { Name = "credit_begin", Type = DataFieldType.DataBegin });
            dataFile.Text.Definition.Add(new TextField { Index = true, Name = "id", Type = DataFieldType.Integer});
            dataFile.Text.Definition.Add(new TextField { Name = "html", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "image", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "time", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "align", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "credit_end", Type = DataFieldType.DataEnd });

            return dataFile;
        }
        public static LineageDataFile EnterEventGrp()
        {
            var dataFile = new LineageDataFileDefinition
            {
                Name = "EnterEventGrp",
                Localizable = false
            };
            dataFile.Text.Definition.Add(new TextField { Name = "enterevent_begin", Type = DataFieldType.DataBegin });
            dataFile.Text.Definition.Add(new TextField { Index = true, Name = "id", Type = DataFieldType.Integer});
            dataFile.Text.Definition.Add(new TextField { Name = "spawn_type", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "sound_name", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "sound_vol", Type = DataFieldType.Float });
            dataFile.Text.Definition.Add(new TextField { Name = "sound_rad", Type = DataFieldType.Float });
            dataFile.Text.Definition.Add(new TextField { Name = "isrise", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "effect_name", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "anim_name", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "enterevent_end", Type = DataFieldType.DataEnd });

            
            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "id"});
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "skipped_field" });
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "sound_name" });
            dataFile.Binary.Definition.Add(new FloatBinaryField { Name = "sound_vol"});
            dataFile.Binary.Definition.Add(new FloatBinaryField { Name = "sound_rad"});
            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "isrise"});
            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "spawn_type"});
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "effect_name", LengthFormat = ArrayLengthFormat.Integer});
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "anim_name", LengthFormat = ArrayLengthFormat.Integer });

            return dataFile;
        }
        public static LineageDataFile ExceptionMiniMapData()
        {
            var dataFile = new LineageDataFileDefinition
            {
                Name = "ExceptionMiniMapData",
                Localizable = false
            };
            dataFile.Text.Definition.Add(new TextField { Name = "exception_location_begin", Type = DataFieldType.DataBegin });
            dataFile.Text.Definition.Add(new TextField { Index = true, Name = "location_id", Type = DataFieldType.Integer});
            dataFile.Text.Definition.Add(new TextField { Name = "location_name", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "max_x", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "min_x", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "max_y", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "min_y", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "max_z", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "min_z", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "seen_x", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "seen_y", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "exception_location_end", Type = DataFieldType.DataEnd });
           
            return dataFile;
        }

        public static LineageDataFile GameTip()
        {
            var dataFile = new LineageDataFileDefinition
            {
                Name = "GameTip",
                Localizable = true
            };
            dataFile.Text.Definition.Add(new TextField { Name = "gametip_begin", Type = DataFieldType.DataBegin });
            dataFile.Text.Definition.Add(new TextField { Index = true, Name = "id", Type = DataFieldType.Integer});
            dataFile.Text.Definition.Add(new TextField { Name = "priority", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "target_lv", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "validity", Type = DataFieldType.Integer });
            dataFile.Text.Definition.Add(new TextField { Name = "tip_msg", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "gametip_end", Type = DataFieldType.DataEnd });
           
            return dataFile;
        }
        public static LineageDataFile GoodsIcon()
        {
            var dataFile = new LineageDataFileDefinition
            {
                Name = "GoodsIcon",
                Localizable = false
            };
            dataFile.Text.Definition.Add(new TextField { Name = "goods_icon_begin", Type = DataFieldType.DataBegin });
            dataFile.Text.Definition.Add(new TextField { Index = true, Name = "id", Type = DataFieldType.Integer});
            dataFile.Text.Definition.Add(new TextField { Name = "icon", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "goods_icon_end", Type = DataFieldType.DataEnd });
            
            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "id"});
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "icon", LengthFormat = ArrayLengthFormat.Integer});
           
            return dataFile;
        }
        public static LineageDataFile HuntingZone()
        {
            var dataFile = new LineageDataFileDefinition
            {
                Name = "HuntingZone",
                Localizable = false
            };
            dataFile.Text.Definition.Add(new TextField { Name = "Hunt_begin", Type = DataFieldType.DataBegin });
            dataFile.Text.Definition.Add(new TextField { Index = true, Name = "id", Type = DataFieldType.Integer});
            dataFile.Text.Definition.Add(new TextField { Name = "icon", Type = DataFieldType.String });
            dataFile.Text.Definition.Add(new TextField { Name = "Hunt_end", Type = DataFieldType.DataEnd });
            
            dataFile.Binary.Definition.Add(new IntegerBinaryField { Name = "id"});
            dataFile.Binary.Definition.Add(new StringBinaryField { Name = "icon", LengthFormat = ArrayLengthFormat.Integer});
           
            return dataFile;
        }
    }
}