﻿using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unreal.Core.IO;
using Unreal.Core.Localization;
using Unreal.Core.Version;
using Unreal.Engine.LineageData;
using Unreal.Engine.LineageData.Factories;

namespace Unreal.Test.Engine.LineageData.HighFiveDefinition
{
    [TestClass]
    public class HighFiveDataSetTest
    {
        private static string GetWorkingDirectory()
        {
            //return Directory.GetCurrentDirectory();
            return "e:\\Lineage2\\L2Clients\\L2H5T0\\System\\";
        }
        


        [TestMethod]
        public void GenerateDefinitions()
        {
            var currentDirectory = GetWorkingDirectory();

            var highFiveDataInstance = new LineageDataInstance();

            highFiveDataInstance.TargetVersion = L2Version.Throne263;
            highFiveDataInstance.NativeData = HighFiveNativeData.CreateLineageNativeData();

            highFiveDataInstance.AddDataFile(HighFiveLineageData.SysString());
            highFiveDataInstance.AddDataFile(HighFiveLineageData.ActionName());
            highFiveDataInstance.AddDataFile(HighFiveLineageData.AdditionalEffect());
            highFiveDataInstance.AddDataFile(HighFiveLineageData.AdditionalItemGrp());
            highFiveDataInstance.AddDataFile(HighFiveLineageData.CastleName());
            highFiveDataInstance.AddDataFile(HighFiveLineageData.ClassInfo());
            highFiveDataInstance.AddDataFile(HighFiveLineageData.CommandName());
            highFiveDataInstance.AddDataFile(HighFiveLineageData.CreditGrp());
            highFiveDataInstance.AddDataFile(HighFiveLineageData.EnterEventGrp());
            highFiveDataInstance.AddDataFile(HighFiveLineageData.ExceptionMiniMapData());
            highFiveDataInstance.AddDataFile(HighFiveLineageData.GameTip());
            highFiveDataInstance.AddDataFile(HighFiveLineageData.GoodsIcon());

            //foreach (LineageDataFile lineageDataFile in highFiveDataInstance.DataFiles)
            //{
            //    XmlStorageManager.SaveXml(lineageDataFile, new DirectoryInfo(currentDirectory), lineageDataFile.Name);
            //}
            XmlStorageManager.SaveXml(highFiveDataInstance, new DirectoryInfo(currentDirectory), highFiveDataInstance.TargetVersion.ToString());

            Assert.AreEqual(true, true);
        }
        //[TestMethod]
        //public void LoadSaveDefinitions()
        //{
        //    var currentDirectory = GetWorkingDirectory();
        //    var highFiveDataInstance = new LineageDataInstance();
        //    highFiveDataInstance.TargetVersion = L2Version.Throne263;
        //    highFiveDataInstance.NativeData = HighFiveNativeData.CreateLineageNativeData();
        //    highFiveDataInstance.AddDataFile(HighFiveLineageData.SysString());
        //    highFiveDataInstance.AddDataFile(HighFiveLineageData.ActionName());
        //    highFiveDataInstance.AddDataFile(HighFiveLineageData.AdditionalEffect());
        //    highFiveDataInstance.AddDataFile(HighFiveLineageData.AdditionalItemGrp());
        //    highFiveDataInstance.AddDataFile(HighFiveLineageData.CastleName());
        //    highFiveDataInstance.AddDataFile(HighFiveLineageData.ClassInfo());
        //    highFiveDataInstance.AddDataFile(HighFiveLineageData.CommandName());
        //    highFiveDataInstance.AddDataFile(HighFiveLineageData.CreditGrp());
        //    highFiveDataInstance.AddDataFile(HighFiveLineageData.EnterEventGrp());
        //
        //    //foreach (LineageDataFile lineageDataFile in highFiveDataInstance.DataFiles)
        //    //{
        //    //    XmlStorageManager.SaveXml(lineageDataFile, new DirectoryInfo(currentDirectory), lineageDataFile.Name);
        //    //}
        //    XmlStorageManager.SaveXml(highFiveDataInstance, new DirectoryInfo(currentDirectory), highFiveDataInstance.TargetVersion.ToString());
        //    var loaded = XmlStorageManager.LoadXml(
        //        LineageDataManager.Instance[L2Version.Throne263], new DirectoryInfo(currentDirectory), L2Version.Throne263.ToString());
        //    XmlStorageManager.SaveXml(LineageDataManager.Instance[L2Version.Throne263], new DirectoryInfo(currentDirectory), highFiveDataInstance.TargetVersion.ToString()+"2");
        //
        //    Assert.AreEqual(true, loaded);
        //}



        private static bool LoadFile(string schemaName, L2Version version)
        {
            var currentDirectory = GetWorkingDirectory();

            var loaded = XmlStorageManager.LoadXml(
                LineageDataManager.Instance[version], new DirectoryInfo(currentDirectory), version.ToString());

            var test = LineageDataManager.Instance[version][schemaName];
            UnrealFileSystem.Initialize(currentDirectory);
            LocalizationManager.LoadConfig(UnrealFileSystem.Instance.Directory);

            var datFileName = test.Localizable ? $"{test.Name}-{LocalizationManager.CurrentLanguage.LanguageInfo.Code}.dat" : $"{test.Name}.dat";
            var txtFileName = test.Localizable ? $"{test.Name}-{LocalizationManager.CurrentLanguage.LanguageInfo.Code}.txt" : $"{test.Name}.txt";

            if (loaded)
            {
                test.Read(DataIO.CreateFileReader(Path.Combine(currentDirectory, datFileName)));
            }

            var converted = test.ToText();
            File.WriteAllText(Path.Combine(currentDirectory, txtFileName), test.Text.ToText(), Encoding.Unicode);

            return converted && loaded;
        }

        [TestMethod]
        public void ActionNameToTextTest()
        {
            var fileLoaded = LoadFile("ActionName", L2Version.Throne263);

            Assert.AreEqual(true, fileLoaded);
        }

        [TestMethod]
        public void AdditionalEffectToTextTest()
        {
            var fileLoaded = LoadFile("AdditionalEffect", L2Version.Throne263);

            Assert.AreEqual(true, fileLoaded);
        }

        [TestMethod]
        public void AdditionalItemGrpToTextTest()
        {
            var fileLoaded = LoadFile("AdditionalItemGrp", L2Version.Throne263);

            Assert.AreEqual(true, fileLoaded);
        }
        [TestMethod]
        public void CastleNameToTextTest()
        {
            var fileLoaded = LoadFile("CastleName", L2Version.Throne263);

            Assert.AreEqual(true, fileLoaded);
        }

        [TestMethod]
        public void ClassInfoToTextTest()
        {
            var fileLoaded = LoadFile("ClassInfo", L2Version.Throne263);

            Assert.AreEqual(true, fileLoaded);
        }

        [TestMethod]
        public void CommandNameToTextTest()
        {
            var fileLoaded = LoadFile("CommandName", L2Version.Throne263);

            Assert.AreEqual(true, fileLoaded);
        }

        [TestMethod]
        public void CreditGrpToTextTest()
        {
            var fileLoaded = LoadFile("CreditGrp", L2Version.Throne263);

            Assert.AreEqual(true, fileLoaded);
        }

        [TestMethod]
        public void EnterEventGrpToTextTest()
        {
            var fileLoaded = LoadFile("EnterEventGrp", L2Version.Throne263);

            Assert.AreEqual(true, fileLoaded);
        }
        [TestMethod]
        public void ExceptionMiniMapDataToTextTest()
        {
            var fileLoaded = LoadFile("ExceptionMiniMapData", L2Version.Throne263);

            Assert.AreEqual(true, fileLoaded);
        }
        [TestMethod]
        public void GameTipToTextTest()
        {
            var fileLoaded = LoadFile("GameTip", L2Version.Throne263);

            Assert.AreEqual(true, fileLoaded);
        }
        [TestMethod]
        public void GoodsIconToTextTest()
        {
            var fileLoaded = LoadFile("GoodsIcon", L2Version.Throne263);

            Assert.AreEqual(true, fileLoaded);
        }
    }
}