﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Unreal.Engine.LineageData.Definition;
using Unreal.Engine.LineageData.Native;

namespace Unreal.Engine.LineageData
{
    public class LineageDataConverter
    {
        public LineageNative Natives;
        public Dictionary<string, LineageDataFile> Datas;

        public string ConvertData(int value, DataFieldType dataFieldType, string dataSource)
        {
            if (string.IsNullOrEmpty(dataSource)) return SimpleTypeToString(value, dataFieldType);
            if(!dataSource.Contains(".")) throw new FormatException($"{nameof(dataSource)} invalid format, must be like 'Native.command2', got {dataSource}");
            var splited = dataSource.Split('.');
            if (splited.Length > 1)
            {
                if (splited[0].Equals("Native"))
                {
                    var nativeData = this.Natives[splited[1]];
                    var convertedValue = nativeData[value];
                    if (string.IsNullOrEmpty(convertedValue))
                    {
                        return dataFieldType.FormatString(value.ToString(CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        return DataFieldType.String.FormatString(convertedValue);
                    }
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
            else
            {
                throw new FormatException($"{nameof(dataSource)} invalid format, must be like 'Native.command2', got {dataSource}");
            }
        }

        private string SimpleTypeToString(int value, DataFieldType dataFieldType)
        {
            return dataFieldType.FormatString(value.ToString(CultureInfo.InvariantCulture));
        }
        
    }
}