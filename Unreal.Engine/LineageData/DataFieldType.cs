﻿using System;
using System.Xml.Linq;
using Unreal.Core.Exceptions;
using Unreal.Engine.LineageData.Definition;
using Unreal.Engine.LineageData.Definition.BinaryField;

namespace Unreal.Engine.LineageData
{
    public enum DataFieldType
    {
        DataBegin,
        DataEnd,
        Integer,
        Float,
        Integer64,
        String,
        NumericArray,
        ComplexArray,
        Max
    }
    
    public static class DataFieldTypeManager
    {
        private static readonly (char Tag, DataFieldType FieldType)[] tagDataFieldTypeMap;
        private static readonly (Type Type, DataFieldType FieldType)[] typeDataFieldTypeMap;
        private static readonly (IBinaryField Field, DataFieldType FieldType)[] fieldDataFieldTypeMap;
        private static readonly (string Format, DataFieldType FieldType)[] formatDataFieldTypeMap;

        static DataFieldTypeManager()
        {
            tagDataFieldTypeMap = new (char Tag, DataFieldType FieldType)[(int)DataFieldType.Max];
            tagDataFieldTypeMap[(int)DataFieldType.DataBegin] =    ('c', DataFieldType.DataBegin);
            tagDataFieldTypeMap[(int)DataFieldType.DataEnd] =      ('e', DataFieldType.DataEnd);
            tagDataFieldTypeMap[(int)DataFieldType.Integer] =      ('d', DataFieldType.Integer);
            tagDataFieldTypeMap[(int)DataFieldType.Integer64] =    ('q', DataFieldType.Integer64);
            tagDataFieldTypeMap[(int)DataFieldType.Float] =        ('f', DataFieldType.Float);
            tagDataFieldTypeMap[(int)DataFieldType.String] =       ('s', DataFieldType.String);
            tagDataFieldTypeMap[(int)DataFieldType.NumericArray] = ('i', DataFieldType.NumericArray);
            tagDataFieldTypeMap[(int)DataFieldType.ComplexArray] =  ('m', DataFieldType.ComplexArray);


            typeDataFieldTypeMap = new (Type Type, DataFieldType FieldType)[(int)DataFieldType.Max];
            typeDataFieldTypeMap[(int)DataFieldType.DataBegin] =    (typeof(DataBeginBinaryField), DataFieldType.DataBegin);
            typeDataFieldTypeMap[(int)DataFieldType.DataEnd] =      (typeof(DataEndBinaryField), DataFieldType.DataEnd);
            typeDataFieldTypeMap[(int)DataFieldType.Integer] =      (typeof(IntegerBinaryField), DataFieldType.Integer);
            typeDataFieldTypeMap[(int)DataFieldType.Integer64] =    (typeof(Integer64BinaryField), DataFieldType.Integer64);
            typeDataFieldTypeMap[(int)DataFieldType.Float] =        (typeof(FloatBinaryField), DataFieldType.Float);
            typeDataFieldTypeMap[(int)DataFieldType.String] =       (typeof(StringBinaryField), DataFieldType.String);
            typeDataFieldTypeMap[(int)DataFieldType.NumericArray] = (typeof(NumericArrayBinaryField), DataFieldType.NumericArray);
            typeDataFieldTypeMap[(int)DataFieldType.ComplexArray] =  (typeof(ComplexBinaryField), DataFieldType.ComplexArray);

            fieldDataFieldTypeMap = new (IBinaryField Field, DataFieldType FieldType)[(int)DataFieldType.Max];
            fieldDataFieldTypeMap[(int)DataFieldType.DataBegin] =    (new DataBeginBinaryField(), DataFieldType.DataBegin);
            fieldDataFieldTypeMap[(int)DataFieldType.DataEnd] =      (new DataEndBinaryField(), DataFieldType.DataEnd);
            fieldDataFieldTypeMap[(int)DataFieldType.Integer] =      (new IntegerBinaryField(), DataFieldType.Integer);
            fieldDataFieldTypeMap[(int)DataFieldType.Integer64] =    (new Integer64BinaryField(), DataFieldType.Integer64);
            fieldDataFieldTypeMap[(int)DataFieldType.Float] =        (new FloatBinaryField(), DataFieldType.Float);
            fieldDataFieldTypeMap[(int)DataFieldType.String] =       (new StringBinaryField(), DataFieldType.String);
            fieldDataFieldTypeMap[(int)DataFieldType.NumericArray] = (new NumericArrayBinaryField(), DataFieldType.NumericArray);
            fieldDataFieldTypeMap[(int)DataFieldType.ComplexArray] = (new ComplexBinaryField(), DataFieldType.ComplexArray);

            formatDataFieldTypeMap = new (string Format, DataFieldType FieldType)[(int)DataFieldType.Max];
            formatDataFieldTypeMap[(int)DataFieldType.DataBegin] =    ("", DataFieldType.DataBegin);
            formatDataFieldTypeMap[(int)DataFieldType.DataEnd] =      ("", DataFieldType.DataEnd);
            formatDataFieldTypeMap[(int)DataFieldType.Integer] =      ("{0}", DataFieldType.Integer);
            formatDataFieldTypeMap[(int)DataFieldType.Integer64] =    ("{0}", DataFieldType.Integer64);
            formatDataFieldTypeMap[(int)DataFieldType.Float] =        ("{0}", DataFieldType.Float);
            formatDataFieldTypeMap[(int)DataFieldType.String] =       ("[{0}]", DataFieldType.String);
            formatDataFieldTypeMap[(int)DataFieldType.NumericArray] = ("{{{0}}}", DataFieldType.NumericArray);
            formatDataFieldTypeMap[(int)DataFieldType.ComplexArray] = ("{{{0}}}", DataFieldType.ComplexArray);
        }

        public static char GetTag(this DataFieldType dataFieldType)
        {
            for (int i = 0; i < (int)DataFieldType.Max; i++)
            {
                if (tagDataFieldTypeMap[i].FieldType == dataFieldType)
                    return tagDataFieldTypeMap[i].Tag;
            }
            throw new ArgumentOutOfRangeException(nameof(dataFieldType), dataFieldType, null);
        }
        public static Type GetBinaryType(this DataFieldType dataFieldType)
        {
            for (int i = 0; i < (int)DataFieldType.Max; i++)
            {
                if (typeDataFieldTypeMap[i].FieldType == dataFieldType)
                    return typeDataFieldTypeMap[i].Type ?? throw new UnrealCoreException($"Null type in {nameof(DataFieldType)} holder for {dataFieldType}");
            }
            throw new ArgumentOutOfRangeException(nameof(dataFieldType), dataFieldType, null);
        }

        public static string FormatString(this DataFieldType dataFieldType, string insert)
        {
            if (string.IsNullOrEmpty(insert))
            {
                if(dataFieldType == DataFieldType.String)
                {
                    var resultFormat = formatDataFieldTypeMap[(int)(DataFieldType.String)].Format;
                    return string.Format(resultFormat, insert);
                }
                return String.Empty;
            }
            for (int i = 0; i < (int)DataFieldType.Max; i++)
            {
                if (formatDataFieldTypeMap[i].FieldType == dataFieldType)
                {
                    var resultFormat = formatDataFieldTypeMap[i].Format;
                    return string.Format(resultFormat, insert);
                }
            }
            throw new ArgumentOutOfRangeException(nameof(dataFieldType), dataFieldType, null);
        }

        public static IBinaryField CreateBinaryFieldFrom(this DataFieldType dataFieldType, IDataField source)
        {
            for (int i = 0; i < (int)DataFieldType.Max; i++)
            {
                if (typeDataFieldTypeMap[i].FieldType == dataFieldType)
                {
                    var resultField = (IBinaryField) fieldDataFieldTypeMap[i].Field?.Clone();
                    if (resultField != null)
                    {
                        resultField.Name = source.Name;
                        resultField.DataSource = source.DataSource;
                        return resultField;
                    }
                    throw new UnrealCoreException(
                        $"Null object in {nameof(DataFieldType)} holder for {dataFieldType}");
                }
            }
            throw new ArgumentOutOfRangeException(nameof(dataFieldType), dataFieldType, null);
        }

        public static IBinaryField CreateBinaryFieldFrom(this DataFieldType dataFieldType, XElement xmlNode)
        {
            for (int i = 0; i < (int)DataFieldType.Max; i++)
            {
                if (typeDataFieldTypeMap[i].FieldType == dataFieldType)
                {
                    var resultField = (IBinaryField)fieldDataFieldTypeMap[i].Field?.Clone();
                    if (resultField != null)
                    {
                        resultField.FromXml(xmlNode);
                        return resultField;
                    }
                    throw new UnrealCoreException(
                        $"Null object in {nameof(DataFieldType)} holder for {dataFieldType}");
                }
            }
            throw new ArgumentOutOfRangeException(nameof(dataFieldType), dataFieldType, null);
        }

        public static DataFieldType GetDataFieldType(char tag)
        {
            for (int i = 0; i < (int) DataFieldType.Max; i++)
            {
                if (tagDataFieldTypeMap[i].Tag == tag)
                    return tagDataFieldTypeMap[i].FieldType;
            }
            throw new ArgumentOutOfRangeException(nameof(tag), tag, null);
        }

    }
}