﻿using Unreal.Core.Version;

namespace Unreal.Engine.LineageData
{
    public interface IVersionControl
    {
        L2Version Version { get; set; }
    }
}