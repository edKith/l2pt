﻿namespace Unreal.Engine.LineageData
{
    public enum ArrayLengthFormat
    {
        Integer,
        Compact,
        Constant
    }
}