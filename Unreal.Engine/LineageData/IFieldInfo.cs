﻿using Unreal.Core.Objects;

namespace Unreal.Engine.LineageData
{
    public interface IFieldInfo : IUnrealSerializable
    {
        DataFieldType Type { get; }
        bool IsExternalData { get; }

        //TODO: Add Data Source for external data entry
        object DataSource { get; set; }
    }
}