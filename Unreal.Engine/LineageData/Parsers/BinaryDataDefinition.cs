﻿using System;
using System.Collections.Generic;
using Unreal.Core.IO;
using Unreal.Core.Objects;
using Unreal.Engine.LineageData.Definition;
using Unreal.Engine.LineageData.Definition.BinaryField;

namespace Unreal.Engine.LineageData.Parsers
{
    public class BinaryDataDefinition : List<IBinaryField>, ICloneable, IUnrealSerializable
    {
        public IBinaryField this[string name]
        {
            get {
                foreach (IBinaryField binaryField in this)
                {
                    if (binaryField.Name.Equals(name))
                    {
                        return binaryField;
                    }
                    else if (binaryField is ComplexBinaryField complexBinaryField)
                    {
                        foreach (IBinaryField field in complexBinaryField.Value.Pattern)
                        {
                            if (field.Name.Equals(name))
                            {
                                return field;
                            }
                        }
                    }

                }
                throw new ArgumentException($"{name} not found in {nameof(BinaryDataDefinition)}");
            }
        }

        public object Clone()
        {
            var clone = new BinaryDataDefinition();
            foreach (IBinaryField binaryField in this)
            {
                clone.Add(binaryField.Clone() as IBinaryField);
            }

            return clone;
        }

        public void Read(FileReader reader)
        {
            foreach (IBinaryField binaryField in this)
            {
                binaryField.Read(reader);
            }
        }

        public void Write(FileWriter writer)
        {
            foreach (IBinaryField binaryField in this)
            {
                binaryField.Write(writer);
            }
        }
    }
}