﻿using System;
using System.Collections.Generic;
using System.Text;
using Unreal.Core.Objects;
using Unreal.Engine.LineageData.Definition;

namespace Unreal.Engine.LineageData.Parsers
{
    public class TextDataDefinition : List<ITextField>, ICloneable, ILineageTextSerializable
    {
        public object Clone()
        {
            var clone = new TextDataDefinition();
            foreach (ITextField textField in this)
            {
                clone.Add(textField.Clone() as ITextField);
            }

            return clone;
        }

        public string ToText()
        {
            var stringBuilder = new StringBuilder();
            for (var index = 0; index < this.Count; index++)
            {
                ITextField textField = this[index];
                stringBuilder.Append(textField.ToText());
                stringBuilder.Append(index < this.Count - 1 ? "\t" : "\r\n");
            }

            return stringBuilder.ToString();
        }

        public TextDataDefinition FillData(BinaryDataDefinition binaryData, LineageDataConverter converter)
        {
            foreach (ITextField textField in this)
            {
                if (textField.Type == DataFieldType.DataBegin || textField.Type == DataFieldType.DataEnd)
                    continue;
                textField.Value = binaryData[textField.Name].ValueToText(converter);
            }


            return this;
        }

        public BinaryDataDefinition CreateBinary()
        {
            var binary = new BinaryDataDefinition();
            foreach (ITextField textField in this)
            {
                if (textField.Type == DataFieldType.DataBegin || textField.Type == DataFieldType.DataEnd)
                    continue;
                binary.Add(textField.Type.CreateBinaryFieldFrom(textField));
            }


            return binary;
        }


        public bool FromText()
        {
            throw new NotImplementedException();
        }
    }
}