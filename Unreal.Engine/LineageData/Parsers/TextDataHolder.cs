﻿using System;
using System.Collections.Generic;
using System.Text;
using Unreal.Core.Objects;

namespace Unreal.Engine.LineageData.Parsers
{
    public class TextDataHolder : List<TextDataDefinition>, ILineageTextSerializable
    {
        public TextDataDefinition Pattern { get; set; }
        public TextDataHolder(TextDataDefinition pattern)
        {
            this.Pattern = pattern;
        }

        public string ToText()
        {
            var stringBuilder = new StringBuilder(); 
            foreach (TextDataDefinition textDataDefinition in this)
            {
                stringBuilder.Append(textDataDefinition.ToText());
            }

            return stringBuilder.ToString();
        }

        public bool FromText()
        {
            throw new System.NotImplementedException();
        }
    }
}