﻿using System;
using System.Collections.Generic;
using Unreal.Core.IO;
using Unreal.Core.Objects;

namespace Unreal.Engine.LineageData.Parsers
{
    public class BinaryDataHolder : List<BinaryDataDefinition>, IUnrealSerializable, ICloneable
    {
        public BinaryDataDefinition Pattern { get; set; }
        public int DataLength { get; set; } 
        public ArrayLengthFormat LengthFormat { get; set; }

        public BinaryDataHolder()
        {
            this.DataLength = -1;
            LengthFormat = ArrayLengthFormat.Integer;
            Pattern = new BinaryDataDefinition();

        }
        public BinaryDataHolder(BinaryDataDefinition pattern) :this()
        {
            Pattern = pattern;
        }

        public void Read(FileReader reader)
        {
            var count = this.DataLength;
            if (LengthFormat == ArrayLengthFormat.Integer)
            {
                count = reader.ReadInt32();
            }
            else if (LengthFormat == ArrayLengthFormat.Compact)
            {
                count = reader.ReadCompactInt();
            }

            for (int i = 0; i < count; i++)
            {
                var clonedData = Pattern.Clone() as BinaryDataDefinition;
                clonedData.Read(reader);
                Add(clonedData);
            }
        }

        public void Write(FileWriter writer)
        {
            var count = this.DataLength;
            if (LengthFormat == ArrayLengthFormat.Integer)
            {
                writer.Write(count);
            }
            else if (LengthFormat == ArrayLengthFormat.Compact)
            {
                writer.WriteCompactInt(count);
            }

            foreach (BinaryDataDefinition binaryDataDefinition in this)
            {
                binaryDataDefinition.Write(writer);
            }
        }

        public object Clone()
        {
            var clone = new BinaryDataHolder();
            clone.DataLength = this.DataLength;
            clone.LengthFormat = this.LengthFormat;
            if (this.Pattern != null)
            {
                clone.Pattern = this.Pattern.Clone() as BinaryDataDefinition;
            }
            foreach (BinaryDataDefinition binaryDataDefinition in this)
            {
                clone.Add(binaryDataDefinition.Clone() as BinaryDataDefinition);
            }
            return clone;
        }
    }
}