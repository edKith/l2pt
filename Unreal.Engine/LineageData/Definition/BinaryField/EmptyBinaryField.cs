﻿using System.Xml.Linq;
using Unreal.Core.IO;

namespace Unreal.Engine.LineageData.Definition.BinaryField
{
    public abstract class EmptyBinaryField : IBinaryField
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public string ValueToText(LineageDataConverter converter)
        {
            throw new System.NotImplementedException();
        }

        public DataFieldType Type { get; set; }
        public string DataSource { get; set; }

        public XElement ToXml()
        {
            throw new System.NotImplementedException();
        }

        public bool FromXml(XElement rootElement)
        {
            throw new System.NotImplementedException();
        }

        public void Read(FileReader reader)
        {
            throw new System.NotImplementedException();
        }

        public void Write(FileWriter writer)
        {
            throw new System.NotImplementedException();
        }

        public abstract object Clone();
    }
}