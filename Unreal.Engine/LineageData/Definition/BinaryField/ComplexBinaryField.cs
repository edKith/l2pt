﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using Unreal.Core.IO;
using Unreal.Engine.LineageData.Parsers;

namespace Unreal.Engine.LineageData.Definition.BinaryField
{
    public class ComplexBinaryField : BaseBinaryField<BinaryDataHolder>
    {
        public int DataLength
        {
            get { return Value.DataLength; }
            set { Value.DataLength = value; }
        }

        public ArrayLengthFormat LengthFormat
        {
            get { return Value.LengthFormat; }
            set { Value.LengthFormat = value; }
        }

        public ComplexBinaryField()
        {
            Value = new BinaryDataHolder();
            DataLength = -1;
            LengthFormat = ArrayLengthFormat.Integer;
            Type = DataFieldType.ComplexArray;
        }

        public override string ValueToText(LineageDataConverter converter)
        {

            var stringBuilder = new StringBuilder();
            for (var i = 0; i < this.Value.Count; i++)
            {
                var definitionEntry = this.Value[i];
                for (var j = 0; j < definitionEntry.Count; j++)
                {
                    var innerBinaryField = definitionEntry[j];
                    stringBuilder.Append(innerBinaryField.ValueToText(converter));
                    if (j < definitionEntry.Count - 1)
                    {
                        stringBuilder.Append(';');
                    }
                }
                if (i < Value.Count - 1)
                {
                    stringBuilder.Append(';');
                }


            }
            return Type.FormatString(stringBuilder.ToString());
        }

        public override XElement ToXml()
        {

            var rootElement = new XElement("Field");

            rootElement.Add(new XAttribute(nameof(Name), Name));
            rootElement.Add(new XAttribute(nameof(Type), Type.GetTag()));
            if (!string.IsNullOrEmpty(DataSource))
            {
                rootElement.Add(new XAttribute(nameof(DataSource), DataSource));
            }
            rootElement.Add(new XAttribute(nameof(LengthFormat), LengthFormat.ToString()));
            if (LengthFormat == ArrayLengthFormat.Constant)
                rootElement.Add(new XAttribute(nameof(DataLength), DataLength));

            foreach (var entry in this.Value.Pattern)
            {
                rootElement.Add(entry.ToXml());
            }
            return rootElement;
        }

        public override bool FromXml(XElement rootElement)
        {
            {
                if (rootElement.Name == "Field")
                {

                    this.Name = (string)rootElement.Attribute(nameof(this.Name));
                    this.Type = DataFieldTypeManager.GetDataFieldType(rootElement.Attribute(nameof(this.Type)).Value[0]);
                    var dataSourceAttribute = rootElement.Attribute(nameof(DataSource));
                    if (dataSourceAttribute != null)
                    {
                        this.DataSource = (string)dataSourceAttribute;
                    }
                    var lengthFormatAttribute = rootElement.Attribute(nameof(this.LengthFormat));
                    this.LengthFormat = (ArrayLengthFormat)Enum.Parse(typeof(ArrayLengthFormat), (string)lengthFormatAttribute);

                    if (LengthFormat == ArrayLengthFormat.Constant)
                    {
                        this.DataLength = (int)rootElement.Attribute(nameof(this.DataLength));
                    }
                    foreach (XElement xElement in rootElement.Elements())
                    {
                        var textField = new InitialBinaryField();
                        if (textField.FromXml(xElement))
                        {
                            this.Value.Pattern.Add(textField.ConvertToTypedField(xElement));
                        }
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public override void Read(FileReader reader)
        {
            Value.Read(reader);
        }

        public override void Write(FileWriter writer)
        {
            Value.Write(writer);
        }

        public override object Clone()
        {

            ComplexBinaryField clone = new ComplexBinaryField();
            clone.Name = this.Name;
            clone.Type = this.Type;
            clone.DataSource = this.DataSource;
            clone.Value = this.Value.Clone() as BinaryDataHolder;
            return clone;
        }
        
    }
}