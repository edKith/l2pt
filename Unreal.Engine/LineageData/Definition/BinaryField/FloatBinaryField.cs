﻿using System.Globalization;
using System.Xml.Linq;
using Unreal.Core.IO;

namespace Unreal.Engine.LineageData.Definition.BinaryField
{
    public class FloatBinaryField : BaseBinaryField<float>
    {
        public FloatBinaryField()
        {
            Type = DataFieldType.Float;
        }
        public override string ValueToText(LineageDataConverter converter)
        {
            return Type.FormatString(Value.ToString(CultureInfo.InvariantCulture));
        }

        public override void Read(FileReader reader)
        {
            Value = reader.ReadSingle();
        }

        public override void Write(FileWriter writer)
        {
            writer.Write(Value);
        }
        public override object Clone()
        {
            return new FloatBinaryField() { Name = Name, Type = Type, Value = Value, DataSource = DataSource};
        }
    }
}