﻿using System.Xml.Linq;
using Unreal.Core.IO;

namespace Unreal.Engine.LineageData.Definition.BinaryField
{
    public class Integer64BinaryField : BaseBinaryField<long>
    {
        public Integer64BinaryField()
        {
            Type = DataFieldType.Integer64;
        }

        public override string ValueToText(LineageDataConverter converter)
        {
            return Type.FormatString(Value.ToString());
        }

        public override void Read(FileReader reader)
        {
            Value = reader.ReadInt64();
        }

        public override void Write(FileWriter writer)
        {
            writer.Write(Value);
        }

        public override object Clone()
        {
            return new Integer64BinaryField(){Name = Name, Type = Type, Value = Value, DataSource = DataSource};
        }
    }
}