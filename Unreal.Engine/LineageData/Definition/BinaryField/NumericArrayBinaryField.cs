﻿using System;
using System.Text;
using System.Xml.Linq;
using Unreal.Core.Exceptions;
using Unreal.Core.IO;

namespace Unreal.Engine.LineageData.Definition.BinaryField
{
    public class NumericArrayBinaryField : BaseBinaryField<int[]>
    {
        public int DataLength { get; set; }
        public ArrayLengthFormat LengthFormat { get; set; }
        public NumericArrayBinaryField()
        {
            DataLength = -1;
            LengthFormat = ArrayLengthFormat.Integer;
            Type = DataFieldType.NumericArray;
            Value = new int[0];
        }

        public override string ValueToText(LineageDataConverter converter)
        {
            var stringBuilder = new StringBuilder();
            for (var i = 0; i < this.Value.Length; i++)
            {
                int value = this.Value[i];
                stringBuilder.Append(DataFieldType.Integer.FormatString(value.ToString()));
                if (i < Value.Length - 1)
                {
                    stringBuilder.Append(';');
                }
            }

            return Type.FormatString(stringBuilder.ToString());
        }

        public override XElement ToXml()
        {

            var rootElement = base.ToXml();

            rootElement.Add(new XAttribute(nameof(LengthFormat), LengthFormat.ToString()));
            if(LengthFormat == ArrayLengthFormat.Constant)
                rootElement.Add(new XAttribute(nameof(DataLength), DataLength));
            return rootElement;
        }

        public override bool FromXml(XElement rootElement)
        {
            {
                if (rootElement.Name == "Field")
                {

                    this.Name = (string)rootElement.Attribute(nameof(this.Name));
                    this.Type = DataFieldTypeManager.GetDataFieldType(rootElement.Attribute(nameof(this.Type)).Value[0]);
                    var dataSourceAttribute = rootElement.Attribute(nameof(DataSource));
                    if (dataSourceAttribute != null)
                    {
                        this.DataSource = (string)dataSourceAttribute;
                    }
                    this.LengthFormat = (ArrayLengthFormat)Enum.Parse(typeof(ArrayLengthFormat), (string)rootElement.Attribute(nameof(this.LengthFormat)));

                    if (LengthFormat == ArrayLengthFormat.Constant)
                    {
                        this.DataLength = (int)rootElement.Attribute(nameof(this.DataLength));
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public override void Read(FileReader reader)
        {
            var count = this.DataLength;
            try
            {
                if (LengthFormat == ArrayLengthFormat.Integer)
                {
                    count = reader.ReadInt32();
                }
                else if (LengthFormat == ArrayLengthFormat.Compact)
                {
                    count = reader.ReadCompactInt();
                }

                Value = new int[count];
                for (int i = 0; i < count; i++)
                {
                    Value[i] = reader.ReadInt32();
                }
            }
            catch (Exception ex)
            {
                throw new UnrealIOException($"Error at {reader.StreamPosition} with count {count}", ex);
            }

        }

        public override void Write(FileWriter writer)
        {
            var count = this.DataLength;
            if (LengthFormat == ArrayLengthFormat.Integer)
            {
                writer.Write(count);
            }
            else if (LengthFormat == ArrayLengthFormat.Compact)
            {
                writer.WriteCompactInt(count);
            }

            foreach (var value in Value)
            {
                writer.Write(value);
            }
        }

        public override object Clone()
        {
            NumericArrayBinaryField clone = new NumericArrayBinaryField();
            clone.Name = this.Name;
            clone.Type = this.Type;
            clone.DataSource = this.DataSource;
            clone.Value = new int[Value.Length];
            clone.DataLength = this.DataLength;
            clone.LengthFormat = this.LengthFormat;
            Array.Copy(Value, clone.Value, Value.Length);
            return clone;
        }
    }
}