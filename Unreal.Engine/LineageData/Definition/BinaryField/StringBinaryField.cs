﻿using System;
using System.Xml.Linq;
using Unreal.Core.IO;

namespace Unreal.Engine.LineageData.Definition.BinaryField
{
    public class StringBinaryField : BaseBinaryField<string>
    {
        public ArrayLengthFormat LengthFormat { get; set; }
        public StringBinaryField()
        {
            Type = DataFieldType.String;
            LengthFormat = ArrayLengthFormat.Compact;
        }
        public override XElement ToXml()
        {

            var rootElement = base.ToXml();
            if(LengthFormat != ArrayLengthFormat.Compact)
                rootElement.Add(new XAttribute(nameof(LengthFormat), LengthFormat.ToString()));
            return rootElement;
        }

        public override bool FromXml(XElement rootElement)
        {
            {
                if (rootElement.Name == "Field")
                {

                    this.Name = (string)rootElement.Attribute(nameof(this.Name));
                    this.Type = DataFieldTypeManager.GetDataFieldType(rootElement.Attribute(nameof(this.Type)).Value[0]);

                    var dataSourceAttribute = rootElement.Attribute(nameof(DataSource));
                    if (dataSourceAttribute != null)
                    {
                        this.DataSource = (string)dataSourceAttribute;
                    }
                    var lengthFormatAttribute = rootElement.Attribute(nameof(this.LengthFormat));
                    if (lengthFormatAttribute != null)
                    {
                        this.LengthFormat = (ArrayLengthFormat) Enum.Parse(typeof(ArrayLengthFormat),
                            (string) lengthFormatAttribute);
                    }
                    else
                    {
                        this.LengthFormat = ArrayLengthFormat.Compact;
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public override string ValueToText(LineageDataConverter converter)
        {
            return Type.FormatString(Value);
        }

        public override void Read(FileReader reader)
        {
            if (this.LengthFormat == ArrayLengthFormat.Compact)
            {
                this.Value = reader.ReadString();
            }
            else
            {
                this.Value = reader.ReadIntSizedUnicodeString();
            }
        }

        public override void Write(FileWriter writer)
        {
            if(this.LengthFormat == ArrayLengthFormat.Compact)
            {
                writer.Write(Value);
            }
            else
            {
                writer.WriteIntSizedUnicodeString(Value);
            }
        }
        public override object Clone()
        {
            return new StringBinaryField() { Name = Name, Type = Type, Value = Value, LengthFormat = LengthFormat, DataSource = DataSource};
        }
    }
}