﻿using System;
using System.Xml.Linq;
using Unreal.Core.Exceptions;
using Unreal.Core.IO;

namespace Unreal.Engine.LineageData.Definition.BinaryField
{
    public abstract class BaseBinaryField<T> : IBinaryField<T>
    {
        public DataFieldType Type { get; set; }
        public string DataSource { get; set; }
        public T Value { get; set; }
        public abstract string ValueToText(LineageDataConverter converter);

        public virtual XElement ToXml()
        {

            var rootElement = new XElement("Field");

            rootElement.Add(new XAttribute(nameof(Name), Name));
            rootElement.Add(new XAttribute(nameof(Type), Type.GetTag()));
            if (!string.IsNullOrEmpty(DataSource))
            {
                rootElement.Add(new XAttribute(nameof(DataSource), DataSource));
            }
            return rootElement;
        }

        public virtual bool FromXml(XElement rootElement)
        {
            if (rootElement.Name == "Field")
            {
                this.Name = (string) rootElement.Attribute(nameof(this.Name));
                this.Type = DataFieldTypeManager.GetDataFieldType(rootElement.Attribute(nameof(this.Type)).Value[0]);
                var dataSourceAttribute = rootElement.Attribute(nameof(DataSource));
                if (dataSourceAttribute != null)
                {
                    this.DataSource = (string)dataSourceAttribute;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public abstract void Read(FileReader reader);

        public abstract void Write(FileWriter writer);

        public string Name { get; set; }

        object IBinaryField.Value
        {
            get => this.Value;
            set
            {
                if (value is T typed)
                    this.Value = typed;
                else
                {
                    throw new UnrealTypeConversionException(typeof(T));
                }
            }
        }

        public abstract object Clone();

    }
}