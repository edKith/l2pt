﻿using System.Globalization;
using System.Xml.Linq;
using Unreal.Core.IO;

namespace Unreal.Engine.LineageData.Definition.BinaryField
{
    public class IntegerBinaryField : BaseBinaryField<int>
    {
        public IntegerBinaryField()
        {
            Type = DataFieldType.Integer;
        }

        public override string ValueToText(LineageDataConverter converter)
        {
            return converter.ConvertData(Value, Type, DataSource);
        }

        public override void Read(FileReader reader)
        {
            Value = reader.ReadInt32();
        }

        public override void Write(FileWriter writer)
        {
            writer.Write(Value);
        }
        public override object Clone()
        {
            return new IntegerBinaryField() { Name = Name, Type = Type, Value = Value, DataSource = DataSource};
        }

    }
}