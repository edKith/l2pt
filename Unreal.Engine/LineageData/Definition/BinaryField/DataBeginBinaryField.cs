﻿namespace Unreal.Engine.LineageData.Definition.BinaryField
{
    public class DataBeginBinaryField : EmptyBinaryField
    {
        public DataBeginBinaryField()
        {
            Type = DataFieldType.DataBegin;
        }

        public override object Clone()
        {
            return new DataBeginBinaryField();
        }
    }
}