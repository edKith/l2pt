﻿namespace Unreal.Engine.LineageData.Definition.BinaryField
{
    public class DataEndBinaryField : EmptyBinaryField
    {
        public DataEndBinaryField()
        {
            Type = DataFieldType.DataEnd;
        }

        public override object Clone()
        {
            return new DataEndBinaryField();
        }
    }
}