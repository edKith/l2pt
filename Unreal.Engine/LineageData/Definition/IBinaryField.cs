﻿using System;
using Unreal.Core.Objects;

namespace Unreal.Engine.LineageData.Definition
{
    public interface IBinaryField : IDataField, IXmlStorage, IUnrealSerializable, ICloneable
    {
        object Value { get; set; }
        string ValueToText(LineageDataConverter converter);
    }

    public interface IBinaryField<T> : IBinaryField
    {
        new T Value { get; set; }
    }
}