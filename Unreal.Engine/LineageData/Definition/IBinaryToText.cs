﻿namespace Unreal.Engine.LineageData.Definition
{
    public interface IBinaryToText
    {
        bool ToBinary();
        bool ToText();
    }
}