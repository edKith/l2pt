﻿using System.Xml.Linq;

namespace Unreal.Engine.LineageData.Definition
{
    public class TextField : ITextField
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public DataFieldType Type { get; set; }
        public string DataSource { get; set; }
        public bool Index { get; set; }

        public XElement ToXml()
        {
            var rootElement = new XElement("Field");

            rootElement.Add(new XAttribute(nameof(Name), Name));
            rootElement.Add(new XAttribute(nameof(Type), Type.GetTag()));
            if(this.Index) rootElement.Add(new XAttribute(nameof(Index), Index));
            if(!string.IsNullOrEmpty(DataSource)) rootElement.Add(new XAttribute(nameof(DataSource), DataSource));

            return rootElement;
        }

        public bool FromXml(XElement rootElement)
        {
            if (rootElement.Name == "Field")
            {

                this.Name = (string)rootElement.Attribute(nameof(this.Name));
                this.Type = DataFieldTypeManager.GetDataFieldType(rootElement.Attribute(nameof(this.Type)).Value[0]);
                this.Index = (bool?)rootElement.Attribute(nameof(this.Index)) ?? false;
                this.DataSource = (string)rootElement.Attribute(nameof(DataSource));

                return true;
            }
            else
            {
                return false;
            }
        }

        public string ToText()
        {
            if (Type == DataFieldType.DataBegin || Type == DataFieldType.DataEnd)
            {
                return Name;
            }
            return $"{Name}={Value}";
        }

        public bool FromText()
        {
            throw new System.NotImplementedException();
        }

        public object Clone()
        {
            return new TextField(){Name = Name, Index = Index, Type = Type, Value = Value, DataSource = DataSource};
        }
    }
}