﻿using System.Xml.Linq;
using Unreal.Core.IO;
using Unreal.Core.Objects;
using Unreal.Engine.LineageData.Parsers;

namespace Unreal.Engine.LineageData.Definition
{
    public abstract class LineageDataFile : IUnrealSerializable, IBinaryToText, IXmlStorage
    {
        public string Name;
        //TODO: Unreal.Core -> add loading Localization.ini in Lineage2-like way
        //TODO: Store in runtime current client localization data -> see Unreal.Core.Localization namespace
        public bool Localizable;

        public BinaryData Binary;
        public TextData Text;

        public LineageDataConverter Converter { get; set; }

        protected LineageDataFile()
        {
            this.Localizable = false;
            this.Binary = new BinaryData();
            this.Text = new TextData();
        }
        public virtual void Read(FileReader reader)
        {
            this.Binary.Read(reader);
        }

        public virtual void Write(FileWriter writer)
        {
            writer.Write(this.Binary);
        }

        public abstract bool ToBinary();

        public virtual bool ToText()
        {
            foreach (BinaryDataDefinition binaryDataDefinition in this.Binary.Holder)
            {
                this.Text.Holder.Add((this.Text.Definition.Clone() as TextDataDefinition).FillData(binaryDataDefinition, Converter));
            }
            return true;
        }
        public virtual XElement ToXml()
        {
            var rootElement = new XElement("LineageData");
            rootElement.Add(new XAttribute(nameof(this.Name), this.Name));
            rootElement.Add(new XAttribute(nameof(this.Localizable), this.Localizable));
            if(this.Binary.Definition.Count != 0)
                rootElement.Add(this.Binary.ToXml());
            rootElement.Add(this.Text.ToXml());

            return rootElement;
        }

        public virtual bool FromXml(XElement rootElement)
        {
            if (rootElement.Name == "LineageData")
            {
                this.Name = (string)rootElement.Attribute(nameof(this.Name));
                this.Localizable = (bool)rootElement.Attribute(nameof(this.Localizable));

                var binaryLoaded = this.Binary.FromXml(rootElement.Element(nameof(this.Binary)));
                this.Text.FromXml(rootElement.Element(nameof(this.Text)));
                if (!binaryLoaded)
                {
                    this.Binary.Definition = this.Text.Definition.CreateBinary();
                    this.Binary.Holder = new BinaryDataHolder(this.Binary.Definition);
                }

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}