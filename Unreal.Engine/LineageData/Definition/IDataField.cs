﻿namespace Unreal.Engine.LineageData.Definition
{
    public interface IDataField
    {
        string Name { get; set; }
        DataFieldType Type { get; set; }
        string DataSource { get; set; }
    }
}