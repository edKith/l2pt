﻿using System.Collections.Generic;
using System.Xml.Linq;
using Unreal.Core.Objects;
using Unreal.Engine.LineageData.Parsers;

namespace Unreal.Engine.LineageData.Definition
{
    public class TextData : ILineageTextSerializable, IXmlStorage
    {
        public TextDataDefinition Definition;
        public TextDataHolder Holder;
        public TextData()
        {
            this.Definition = new TextDataDefinition();
            this.Holder = new TextDataHolder(this.Definition);
        }
        public XElement ToXml()
        {
            var rootElement = new XElement("Text");
            foreach (var entry in this.Definition)
            {
                rootElement.Add(entry.ToXml());
            }

            return rootElement;
        }

        public bool FromXml(XElement rootElement)
        {
            if (rootElement.Name == "Text")
            {
                foreach (XElement xElement in rootElement.Elements())
                {
                    var textField = new TextField();
                    if(textField.FromXml(xElement))
                    {
                        this.Definition.Add(textField);
                    }
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public string ToText()
        {
            return this.Holder.ToText();
        }

        public bool FromText()
        {
            throw new System.NotImplementedException();
        }
    }
}