﻿using System.Xml.Linq;
using Unreal.Core.IO;

namespace Unreal.Engine.LineageData.Definition
{
    public class InitialBinaryField : IBinaryField
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public string ValueToText(LineageDataConverter converter)
        {
            return Value.ToString();
        }

        public DataFieldType Type { get; set; }
        public string DataSource { get; set; }

        public virtual XElement ToXml()
        {

            var rootElement = new XElement("Field");

            rootElement.Add(new XAttribute(nameof(Name), Name));
            rootElement.Add(new XAttribute(nameof(Type), Type.GetTag()));
            return rootElement;
        }

        public virtual bool FromXml(XElement rootElement)
        {
            {
                if (rootElement.Name == "Field")
                {

                    this.Name = (string)rootElement.Attribute(nameof(this.Name));
                    this.Type = DataFieldTypeManager.GetDataFieldType(rootElement.Attribute(nameof(this.Type)).Value[0]);

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void Read(FileReader reader)
        {
            throw new System.NotImplementedException();
        }

        public void Write(FileWriter writer)
        {
            throw new System.NotImplementedException();
        }

        public IBinaryField ConvertToTypedField(XElement xElement)
        {
            return Type.CreateBinaryFieldFrom(xElement);
        }

        public object Clone()
        {
            return new InitialBinaryField(){Name = Name, Type = Type, Value = Value};
        }
    }
}