﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using Unreal.Core.IO;
using Unreal.Core.Objects;
using Unreal.Engine.LineageData.Definition.BinaryField;
using Unreal.Engine.LineageData.Parsers;

namespace Unreal.Engine.LineageData.Definition
{
    public class BinaryData : IUnrealSerializable, IXmlStorage
    {
        //TODO: Declare IBinaryField with next
        //  Name
        //  Serialization Order
        //  Mapping for TextData field, probably to textdata field part

        //  Make a descision - how to bind TextData Field with BinaryDate Field
        // I think the easiest way - in using strict string naming
        // e.g. -> TextField.Name="map_loc", BinaryField.Name="map_loc.x"
        // In xml schema we need to define text format like TextField.Format="{$map_loc.x:F;$map_loc.y:F}"

        public BinaryDataDefinition Definition;
        public BinaryDataHolder Holder;
        public BinaryData()
        {
            this.Definition = new BinaryDataDefinition();
            this.Holder = new BinaryDataHolder(this.Definition);
        }
        public void Read(FileReader reader)
        {
            this.Holder.Read(reader);
        }

        public void Write(FileWriter writer)
        {
            this.Holder.Write(writer);
        }
        
        public bool FromXml(XElement rootElement)
        {
            if (rootElement?.Name == "Binary")
            {
                foreach (XElement xElement in rootElement.Elements())
                {
                    var textField = new InitialBinaryField();
                    if (textField.FromXml(xElement))
                    {
                        this.Definition.Add(textField.ConvertToTypedField(xElement));
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public XElement ToXml()
        {
            var rootElement = new XElement("Binary");
            foreach (var entry in this.Definition)
            {
                rootElement.Add(entry.ToXml());
            }

            return rootElement;
        }
    }
}