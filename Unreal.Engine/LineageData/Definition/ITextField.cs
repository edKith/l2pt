﻿using System;
using Unreal.Core.Objects;

namespace Unreal.Engine.LineageData.Definition
{
    public interface ITextField : IDataField, IXmlStorage, ILineageTextSerializable, ICloneable
    {
        string Value { get; set; }
        bool Index { get; set; }
    }
}