﻿using System.Collections.Generic;
using System.Xml.Linq;
using Unreal.Core.Objects;

namespace Unreal.Engine.LineageData.Native
{
    public class LineageNative : Dictionary<string, LineageNativeData>, IXmlStorage
    {
        public void AddDataFile(LineageNativeData lineageNativeData)
        {
            if (this.ContainsKey(lineageNativeData.Name))
            {
                this[lineageNativeData.Name] = lineageNativeData;
            }
            else
            {
                this.Add(lineageNativeData.Name, lineageNativeData);
            }
        }
        public XElement ToXml()
        {
            var rootElement = new XElement(nameof(LineageNative));
            foreach (KeyValuePair<string, LineageNativeData> nativeData in this)
            {
                var element = nativeData.Value.ToXml();
                rootElement.Add(element);
            }

            return rootElement;
        }

        public bool FromXml(XElement rootElement)
        {
            if (rootElement.Name == nameof(LineageNative))
            {
                var nativeDataElements = rootElement.Elements("Native");
                foreach (XElement nativeDataElement in nativeDataElements)
                {
                    var nativeData = new LineageNativeData();
                    if (nativeData.FromXml(nativeDataElement))
                    {
                        this.Add(nativeData.Name, nativeData);
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}