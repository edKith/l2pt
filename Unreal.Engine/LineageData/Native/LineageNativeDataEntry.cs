﻿using System;
using System.Xml.Linq;
using Unreal.Core.Objects;

namespace Unreal.Engine.LineageData.Native
{
    public class LineageNativeDataEntry : IXmlStorage, ICloneable
    {

        public long ID;
        public string Value;
        public XElement ToXml()
        {
            var rootElement = new XElement("Data");

            rootElement.Add(new XAttribute(nameof(this.ID), this.ID));
            rootElement.Add(new XAttribute(nameof(this.Value), this.Value));

            return rootElement;
        }

        public bool FromXml(XElement rootElement)
        {
            if (rootElement.Name == "Data")
            {
                this.ID = (int)rootElement.Attribute(nameof(this.ID));
                this.Value = (string)rootElement.Attribute(nameof(this.Value));
                return true;
            }
            else
            {
                return false;
            }
        }
        public object Clone()
        {
            var clone =  new LineageNativeDataEntry{ID = this.ID, Value = this.Value};
            return clone;
        }

    }
}