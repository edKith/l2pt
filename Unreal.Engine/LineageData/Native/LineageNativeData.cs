﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Unreal.Core.Objects;

namespace Unreal.Engine.LineageData.Native
{
    public class LineageNativeData : IXmlStorage, ICloneable
    {
        public string Name { get; set; }
        public List<LineageNativeDataEntry> Definitions { get; set; } = new List<LineageNativeDataEntry>();

        public string this[int id]
        {
            get {
                foreach (LineageNativeDataEntry lineageNativeDataEntry in Definitions)
                {
                    if (lineageNativeDataEntry.ID == id)
                    {
                        return lineageNativeDataEntry.Value;
                    }
                }

                return string.Empty;
            }
        }
        public XElement ToXml()
        {
            var rootElement = new XElement("Native");
            rootElement.Add(new XAttribute(nameof(this.Name), this.Name));
            foreach (LineageNativeDataEntry definition in Definitions)
            {
                rootElement.Add(definition.ToXml());
            }
            return rootElement;
        }

        public bool FromXml(XElement rootElement)
        {
            if (rootElement.Name == "Native")
            {
                this.Name = (string)rootElement.Attribute(nameof(this.Name));
                foreach (XElement xElement in rootElement.Elements())
                {
                    var entry = new LineageNativeDataEntry();
                    entry.FromXml(xElement);
                    Definitions.Add(entry);
                }

                return true;
            }
            else
            {
                return false;
            }
        }
        
        public object Clone()
        {
            var clone = new LineageNativeData();
            clone.Name = this.Name;
            clone.Definitions = new List<LineageNativeDataEntry>();
            foreach (LineageNativeDataEntry lineageNativeDataEntry in Definitions)
            {
                clone.Definitions.Add(lineageNativeDataEntry.Clone() as LineageNativeDataEntry);
            }
            return clone;
        }
    }
}