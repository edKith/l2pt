﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml.Linq;
using Unreal.Core.Extensions;
using Unreal.Core.Objects;
using Unreal.Core.Version;
using Unreal.Engine.LineageData.Definition;
using Unreal.Engine.LineageData.Native;

namespace Unreal.Engine.LineageData
{
    public class LineageDataInstance : IXmlStorage
    {
        public L2Version TargetVersion;
        public LineageNative NativeData;
        public Dictionary<string, LineageDataFile> DataFiles;
        public LineageDataConverter DataConverter;

        public LineageDataFile this[string dataName]
        {
            get => this.DataFiles[dataName];
        }

        public void AddDataFile(LineageDataFile dataFile)
        {
            dataFile.Converter = this.DataConverter;
            if (this.DataFiles.ContainsKey(dataFile.Name))
            {
                this.DataFiles[dataFile.Name] = dataFile;
            }
            else
            {
                this.DataFiles.Add(dataFile.Name, dataFile);
            }
        }

        public LineageDataInstance()
        {
            this.NativeData = new LineageNative();
            this.DataFiles = new Dictionary<string, LineageDataFile>();
            this.DataConverter = new LineageDataConverter
            {
                Natives = this.NativeData,
                Datas = this.DataFiles
            };
        }


        public XElement ToXml()
        {
            var rootElement = new XElement("LineageDataDefinition");
            rootElement.Add(new XComment($"Last saved {DateTime.Now.ToString(CultureInfo.InvariantCulture)}"));
            rootElement.Add(new XComment($"Version for: {EnumExtensions.GetEnumDescription(TargetVersion)}"));
            rootElement.Add(new XComment($"LineageDataTool by edKith aka Persy"));
            rootElement.Add(new XAttribute(nameof(this.TargetVersion), this.TargetVersion));

            rootElement.Add(this.NativeData.ToXml());
            foreach (KeyValuePair<string, LineageDataFile> lineageDataFile in this.DataFiles)
            {
                var element = lineageDataFile.Value.ToXml();
                rootElement.Add(element);
            }

            return rootElement;
        }

        public bool FromXml(XElement rootElement)
        {
            if (rootElement.Name == "LineageDataDefinition")
            {
                this.TargetVersion = (L2Version) Enum.Parse(typeof(L2Version),
                    (string) rootElement.Attribute(nameof(this.TargetVersion)));

                var nativeDataElement = rootElement.Element("LineageNative");
                this.NativeData.FromXml(nativeDataElement);

                var lineageDataElements = rootElement.Elements("LineageData");
                foreach (XElement lineageDataElement in lineageDataElements)
                {
                    var lineageDataFile = new LineageDataFileDefinition();
                    if(lineageDataFile.FromXml(lineageDataElement))
                    {
                        lineageDataFile.Converter = this.DataConverter;
                        this.DataFiles.Add(lineageDataFile.Name, lineageDataFile);
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}