﻿using System.IO;
using Unreal.Core.IO;

namespace Unreal.Engine.LineageData.Factories
{
    public class DataIO
    {
        public static FileReader CreateFileReader(string fileName)
        {
            return new FileReader(new FileInfo(Path.Combine(Directory.GetCurrentDirectory(), fileName)));
        }
    }
}