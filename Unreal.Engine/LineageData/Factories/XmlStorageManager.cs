﻿using System.IO;
using System.Xml.Linq;
using Unreal.Core.Objects;

namespace Unreal.Engine.LineageData.Factories
{
    public class XmlStorageManager
    {

        public static void SaveXml(IXmlStorage storage, DirectoryInfo targetDirectory, string targetFileName)
        {
            if (!targetDirectory.Exists) targetDirectory.Create();
            FileInfo targetXmlFile = new FileInfo(Path.Combine(targetDirectory.FullName, $"{targetFileName}.xml"));
            using (var fileStream = targetXmlFile.CreateText())
            {
                fileStream.Write(storage.ToXml().ToString());
            }
        }

        public static bool LoadXml(IXmlStorage storage, DirectoryInfo targetDirectory, string targetFileName)
        {
            if (!targetDirectory.Exists) throw new FileNotFoundException();
            FileInfo targetXmlFile = new FileInfo(Path.Combine(targetDirectory.FullName, $"{targetFileName}.xml"));
            if (!targetXmlFile.Exists) throw new FileNotFoundException();
            XDocument doc = XDocument.Load(targetXmlFile.FullName);
            return storage.FromXml(doc.Root);
        }
    }
}