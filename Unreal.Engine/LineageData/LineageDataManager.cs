﻿using System.IO;
using System.Xml.Linq;
using Unreal.Core.Objects;
using Unreal.Core.Templates;
using Unreal.Core.Version;

namespace Unreal.Engine.LineageData
{
    public class LineageDataManager : Singleton<LineageDataManager>
    {
        public LineageDataInstance[] Data;

        public LineageDataManager()
        {
            Data = new LineageDataInstance[(int) L2Version.Max];
            Data[(int) L2Version.Throne263] = new LineageDataInstance{TargetVersion = L2Version.Throne263};
        }

        public LineageDataInstance this[L2Version version]
        {
            get => this.Data[(int)version];
            set => this.Data[(int)version] = value;
        }

    }
}