﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Unreal.Core.Codecs;
using Unreal.Core.IO;
using Unreal.Engine.ImageFormats;

namespace Unreal.Engine.TextureConverter
{
    public static class PngToTga
    {
        public static void Convert(string fileName)
        {
            var pngFileInfo = new FileInfo(fileName);
            if (!pngFileInfo.Exists)
            {
                Console.WriteLine($"Error - file {fileName} not found!");
                return;
            }
            Bitmap bmp = (Bitmap)Image.FromFile(fileName);
            bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
            System.Drawing.Imaging.BitmapData data =
                bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
                    System.Drawing.Imaging.ImageLockMode.ReadOnly, bmp.PixelFormat);
            var colors = new List<Color>();
            unsafe
            {
                // important to use the BitmapData object's Width and Height
                // properties instead of the Bitmap's.
                for (int x = 0; x < data.Width; x++)
                {
                    int columnOffset = x * 4;
                    for (int y = 0; y < data.Height; y++)
                    {
                        byte* row = (byte*)data.Scan0 + y * data.Stride;
                        byte b = row[columnOffset];
                        byte g = row[columnOffset + 1];
                        byte r = row[columnOffset + 2];
                        byte alpha = row[columnOffset + 3];
                        colors.Add(Color.FromArgb(alpha, r, g, b));
                    }
                }
            }
            bmp.UnlockBits(data);
            var tgaFileInfo = new FileInfo(Path.GetFileNameWithoutExtension(fileName)+".tga");
            var tga = Tga.Create32Bit((short)bmp.Height, (short)bmp.Width);
            tga.ColorData = colors.ToArray();
            using (var fileWriter = new FileWriter(new Lineage2NullCodec(), tgaFileInfo))
            {
                fileWriter.Write(tga);
            }
        }
    }
}
