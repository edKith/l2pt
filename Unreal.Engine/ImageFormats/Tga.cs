﻿using System.Drawing;
using Unreal.Core.IO;
using Unreal.Core.Objects;

namespace Unreal.Engine.ImageFormats
{
    public class Tga : IUnrealSerializable
    {
        public static Tga Create32Bit(short width, short height)
        {
            var tga = new Tga
            {
                idLength = 0,
                colorMapType = 0,
                imageType = 2,
                colorMapInfo = new TgaColorMap
                {
                    Offset = 0,
                    Count = 0,
                    BitsPerPixel = 0
                },
                imageInfo = new TgaImageInfo
                {
                    XOrigin = 0,
                    YOrigin = 0,
                    Width = width,
                    Height = height,
                    BitsPerPixel = 32,
                    ImageDescriptor = 8
                }
            };

            return tga;
        }

        private byte idLength;
        private byte colorMapType;
        private byte imageType;
        private TgaColorMap colorMapInfo;
        private TgaImageInfo imageInfo;
        public Color[] ColorData; 

        public void Read(FileReader reader)
        {
            this.idLength = reader.ReadByte();
            this.colorMapType = reader.ReadByte();
            this.imageType = reader.ReadByte();
            this.colorMapInfo = reader.Read<TgaColorMap>();
            this.imageInfo = reader.Read<TgaImageInfo>();
            ColorData = new Color[this.imageInfo.Height * this.imageInfo.Width];
            for (int i = 0; i < ColorData.Length; i++)
            {
                ColorData[i] = Color.FromArgb(reader.ReadInt32());
            }
        }

        public void Write(FileWriter writer)
        {
            writer.Write(this.idLength);
            writer.Write(this.colorMapType);
            writer.Write(this.imageType);
            writer.Write(this.colorMapInfo);
            writer.Write(this.imageInfo);
            for (int i = 0; i < this.imageInfo.Height * this.imageInfo.Width; i++)
            {
                writer.Write(ColorData[i].B);
                writer.Write(ColorData[i].G);
                writer.Write(ColorData[i].R);
                writer.Write(ColorData[i].A);
            }
        }
    }
}