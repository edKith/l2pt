﻿using Unreal.Core.IO;
using Unreal.Core.Objects;

namespace Unreal.Engine.ImageFormats
{
    public class TgaColorMap : IUnrealSerializable
    {
        public short Offset;
        public short Count;
        public byte BitsPerPixel;

        public void Read(FileReader reader)
        {
            this.Offset = reader.ReadInt16();
            this.Count = reader.ReadInt16();
            this.BitsPerPixel = reader.ReadByte();
        }

        public void Write(FileWriter writer)
        {
            writer.Write(this.Offset);
            writer.Write(this.Count);
            writer.Write(this.BitsPerPixel);
        }
    }
}