﻿using Unreal.Core.IO;
using Unreal.Core.Objects;

namespace Unreal.Engine.ImageFormats
{
    public class TgaImageInfo : IUnrealSerializable
    {
        public short XOrigin;
        public short YOrigin;
        public short Width;
        public short Height;
        public byte BitsPerPixel;
        public byte ImageDescriptor;

        public void Read(FileReader reader)
        {
            this.XOrigin = reader.ReadInt16();
            this.YOrigin = reader.ReadInt16();
            this.Width = reader.ReadInt16();
            this.Height = reader.ReadInt16();
            this.BitsPerPixel = reader.ReadByte();
            this.ImageDescriptor = reader.ReadByte();
        }

        public void Write(FileWriter writer)
        {
            writer.Write(this.XOrigin);
            writer.Write(this.YOrigin);
            writer.Write(this.Width);
            writer.Write(this.Height);
            writer.Write(this.BitsPerPixel);
            writer.Write(this.ImageDescriptor);
        }
    }
}