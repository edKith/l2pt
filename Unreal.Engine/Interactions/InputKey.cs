﻿namespace Unreal.Engine.Interactions
{
    public enum InputKey
    {
        /*00*/
        None, LeftMouse, RightMouse, Cancel,
        /*04*/
        MiddleMouse, Unknown05, Unknown06, Unknown07,
        /*08*/
        Backspace, Tab, Unknown0A, Unknown0B,
        /*0C*/
        Unknown0C, Enter, Unknown0E, Unknown0F,
        /*10*/
        Shift, Ctrl, Alt, Pause,
        /*14*/
        CapsLock, Unknown15, Unknown16, Unknown17,
        /*18*/
        Unknown18, Unknown19, Unknown1A, Escape,
        /*1C*/
        Unknown1C, Unknown1D, Unknown1E, Unknown1F,
        /*20*/
        Space, PageUp, PageDown, End,
        /*24*/
        Home, Left, Up, Right,
        /*28*/
        Down, Select, Print, Execute,
        /*2C*/
        PrintScrn, Insert, Delete, Help,
        /*30*/
        N0, N1, N2, N3,
        /*34*/
        N4, N5, N6, N7,
        /*38*/
        N8, N9, Unknown3A, Unknown3B,
        /*3C*/
        Unknown3C, Unknown3D, Unknown3E, Unknown3F,
        /*40*/
        Unknown40, A, B, C,
        /*44*/
        D, E, F, G,
        /*48*/
        H, I, J, K,
        /*4C*/
        L, M, N, O,
        /*50*/
        P, Q, R, S,
        /*54*/
        T, U, V, W,
        /*58*/
        X, Y, Z, Unknown5B,
        /*5C*/
        Unknown5C, Unknown5D, Unknown5E, Unknown5F,
        /*60*/
        NumPad0, NumPad1, NumPad2, NumPad3,
        /*64*/
        NumPad4, NumPad5, NumPad6, NumPad7,
        /*68*/
        NumPad8, NumPad9, GreyStar, GreyPlus,
        /*6C*/
        Separator, GreyMinus, NumPadPeriod, GreySlash,
        /*70*/
        F1, F2, F3, F4,
        /*74*/
        F5, F6, F7, F8,
        /*78*/
        F9, F10, F11, F12,
        /*7C*/
        F13, F14, F15, F16,
        /*80*/
        F17, F18, F19, F20,
        /*84*/
        F21, F22, F23, F24,
        /*88*/
        Unknown88, Unknown89, Unknown8A, Unknown8B,
        /*8C*/
        Unknown8C, Unknown8D, Unknown8E, Unknown8F,
        /*90*/
        NumLock, ScrollLock, Unknown92, Unknown93,
        /*94*/
        Unknown94, Unknown95, Unknown96, Unknown97,
        /*98*/
        Unknown98, Unknown99, Unknown9A, Unknown9B,
        /*9C*/
        Unknown9C, Unknown9D, Unknown9E, Unknown9F,
        /*A0*/
        LShift, RShift, LControl, RControl,
        /*A4*/
        UnknownA4, UnknownA5, UnknownA6, UnknownA7,
        /*A8*/
        UnknownA8, UnknownA9, UnknownAA, UnknownAB,
        /*AC*/
        UnknownAC, UnknownAD, UnknownAE, UnknownAF,
        /*B0*/
        UnknownB0, UnknownB1, UnknownB2, UnknownB3,
        /*B4*/
        UnknownB4, UnknownB5, UnknownB6, UnknownB7,
        /*B8*/
        UnknownB8, Unicode, Semicolon, Equals,
        /*BC*/
        Comma, Minus, Period, Slash,
        /*C0*/
        Tilde, UnknownC1, UnknownC2, UnknownC3,
        /*C4*/
        UnknownC4, UnknownC5, UnknownC6, UnknownC7,
        /*C8*/
        Joy1, Joy2, Joy3, Joy4,
        /*CC*/
        Joy5, Joy6, Joy7, Joy8,
        /*D0*/
        Joy9, Joy10, Joy11, Joy12,
        /*D4*/
        Joy13, Joy14, Joy15, Joy16,
        /*D8*/
        UnknownD8, UnknownD9, UnknownDA, LeftBracket,
        /*DC*/
        Backslash, RightBracket, SingleQuote, UnknownDF,
        /*E0*/
        UnknownE0, UnknownE1, UnknownE2, UnknownE3,
        /*E4*/
        MouseX, MouseY, MouseZ, MouseW,
        /*E8*/
        JoyU, JoyV, JoySlider1, JoySlider2,
        /*EC*/
        MouseWheelUp, MouseWheelDown, Unknown10E, Unknown10F,
        /*F0*/
        JoyX, JoyY, JoyZ, JoyR,
        /*F4*/
        UnknownF4, UnknownF5, Attn, CrSel,
        /*F8*/
        ExSel, ErEof, Play, Zoom,
        /*FC*/
        NoName, PA1, OEMClear
    };
}