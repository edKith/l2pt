﻿namespace Unreal.NWindow.XmlUI.Data
{
    public enum InputAction
    {
        None,    // Not performing special input processing.
        Press,   // Handling a keypress or button press.
        Hold,    // Handling holding a key or button.
        Release, // Handling a key or button release.
        Axis,    // Handling analog axis movement.
    };
}