﻿using System;
using System.Globalization;
using System.IO;
using System.Windows;
using Microsoft.Win32;

namespace ToolSetConfiger
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();
        }

        private void InstallL2EncDec(object sender, RoutedEventArgs e)
        {
            DirectoryInfo sourceFolder = new DirectoryInfo("L2EncDec.NET");
            if (!sourceFolder.Exists)
            {
                MessageBox.Show($"{sourceFolder.FullName} not found!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            FileInfo exe = new FileInfo(Path.Combine(sourceFolder.FullName, "L2EncDec.NET.exe"));
            FileInfo dll = new FileInfo(Path.Combine(sourceFolder.FullName, "Unreal.Core.dll"));
            FileInfo dllEngine = new FileInfo(Path.Combine(sourceFolder.FullName, "Unreal.Engine.dll"));
            if (!exe.Exists || !dll.Exists|| !dllEngine.Exists)
            {
                MessageBox.Show($"Files not found!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            string currentUserAppData = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            DirectoryInfo targetFolder = new DirectoryInfo(Path.Combine(currentUserAppData, ".l2encdec"));
            if (!targetFolder.Exists)
            {
                targetFolder.Create();
            }

            string newExePath = Path.Combine(targetFolder.FullName, exe.Name);
            string newDllPath = Path.Combine(targetFolder.FullName, dll.Name);
            string newDllEnginePath = Path.Combine(targetFolder.FullName, dllEngine.Name);
            exe.CopyTo(newExePath, true);
            dll.CopyTo(newDllPath, true);
            dllEngine.CopyTo(newDllEnginePath, true);
            RegistryKey currentUserKey = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64);
            RegistryKey contextRegistryKey = currentUserKey.CreateSubKey(@"SOFTWARE\Classes\*\shell\L2EncDec");
            RegistryKey contextRegistryKeyDiretory = currentUserKey.CreateSubKey(@"SOFTWARE\Classes\Directory\shell\L2EncDec");
            RegistryKey contextRegistryKeyPng = currentUserKey.CreateSubKey(@"SOFTWARE\Classes\*\shell\L2EncDecPng");
            CultureInfo currentUICulture = CultureInfo.CurrentUICulture;
            string contextMenuString;
            string contextMenuStringForFolder;
            string contextMenuStringForPng;
            if (currentUICulture.Equals(new CultureInfo("ru-RU")))
            {
                contextMenuString = @"Обработать с помощью L2EncDec.NET";
                contextMenuStringForPng = @"Преобразовать в TGA";
                contextMenuStringForFolder = @"Обработать с помощью Patcher.NET";
            }
            else
            {
                contextMenuString = @"Process using L2EncDec.NET";
                contextMenuStringForPng = @"Convert to TGA";
                contextMenuStringForFolder = @"Process using Patcher.NET";
            }

            contextRegistryKey.SetValue(string.Empty, contextMenuString);
            RegistryKey commandSubKey = contextRegistryKey.CreateSubKey("command");
            commandSubKey.SetValue(string.Empty, $"\"{newExePath}\" \"%1\"");

            contextRegistryKeyDiretory.SetValue(string.Empty, contextMenuStringForFolder);
            RegistryKey commandSubKeyDir = contextRegistryKeyDiretory.CreateSubKey("command");
            commandSubKeyDir.SetValue(string.Empty, $"\"{newExePath}\" folder \"%1\"");

            contextRegistryKeyPng.SetValue(string.Empty, contextMenuStringForPng);
            RegistryKey commandSubKeyPng = contextRegistryKeyPng.CreateSubKey("command");
            commandSubKeyPng.SetValue(string.Empty, $"\"{newExePath}\" pngconvert \"%1\"");

            MessageBox.Show($"L2EncDec.NET copied to {targetFolder.FullName}\nRegistryEntry Configured.");
        }

        private void UninstallL2EncDec(object sender, RoutedEventArgs e)
        {
            string currentUserAppData = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            DirectoryInfo appDataFolder = new DirectoryInfo(Path.Combine(currentUserAppData, ".l2encdec"));
            if (appDataFolder.Exists)
            {
                appDataFolder.Delete(true);
            }

            DeleteCurrentUserRegistryKey(@"SOFTWARE\Classes\*\shell\L2EncDec");
            DeleteCurrentUserRegistryKey(@"SOFTWARE\Classes\Directory\shell\L2EncDec");
            DeleteCurrentUserRegistryKey(@"SOFTWARE\Classes\*\shell\L2EncDecPng");

            MessageBox.Show($"L2EncDec.NET was deleted from your system");
        }

        private void DeleteCurrentUserRegistryKey(string keyPath)
        {
            RegistryKey currentUserKey = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64);
            RegistryKey contextRegistryKey = currentUserKey.OpenSubKey(keyPath);
            if (contextRegistryKey != null)
            {
                currentUserKey.DeleteSubKeyTree(keyPath);
            }
        }
    }
}